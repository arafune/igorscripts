# Igor Macro Pack by RA

1. What's this?
    Igor Procedure files that would be useful for analyzing (or loading data) experimental data.

  　実験データの解析に（またはデータの読み込みに）役立つんじゃないかなあと思うIgorProcedureファイル。

2.　Installation 

    * LoadRA_procs.ipf (またはそのショートカットを) Igor Procedures フォルダーに置く。ことで使える。
    * Macの場合はメイリオフォントが入っていないとフォント置き換えの指示が出てくるかもしれない（そのときは適当な日本語を表示できるフォントを選んでおけば良い：全てのフォルダー名がASCIIのみであれば、どんなフォントでも問題ない。）

    * Put LoadRA_procs.ipf (or its shortcut/alias) in the Igor Procedures folder.
    * (You may be asked to replace the font if it does not contain the Meirio font.)
