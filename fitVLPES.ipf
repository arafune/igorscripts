#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVertsion = 8.0
#pragma ModuleName = fitVLPES
// Version $Id: fitVLPES.ipf 393 2006-04-25 15:03:07Z arafune $

#include "LaserPEAnalysis"
#include "eachWave2"
#include "RA_common"

Menu "Analysis"
	SubMenu "ARPES"
		"Fermi-Dirc fitting" , DispFitVLPESPanel()
	End
End


Function DispFitVLPESPanel()
	DoWindow /F fitVLPESPanel
	if (V_Flag != 0 )
		return 0
	endif
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	setDataFolder root:Packages:LaserUPS
	//  Fitting Parameters
	Variable resol = NumVarOrDefault(":g_resol", 20)
	Variable /G g_resol = resol
	Variable temperature = NumVarOrDefault(":g_temperature", 300)
	Variable /G g_temperature = temperature
	Variable EVac = NumVarOrDefault(":g_EVac", 4)
	Variable /G g_EVac = EVac
	Variable EEf = NumVarOrDefault(":g_EEf",5)
	Variable /G g_EEf = EEf
	Variable Intensity = NumVarOrDefault(":g_Int", 100)
	Variable /G g_Int = Intensity
	Variable Coeffa = NumVarOrDefault(":g_Coeffa", 1)
	Variable /G g_Coeffa = Coeffa
	SetDataFolder dfSave
	RA_fitVLPESPanel()
end

//Window fitVLPESPanel()	:Panel
static Function RA_fitVLPESPanel() :Panel
	PauseUpdate; Silent 1

	NewPanel /K=1 /W=(62,69,572,721) as "Fermi-Dirac fit"
	ModifyPanel frameStyle=2
	DoWindow /F/C fitVLPESPanel
	//
	SetWindow fitVLPESPanel, hook=fitVLPESPanelHook
	GetWindow fitVLPESPanel, wsize
	//
	ModifyPanel /W = fitVLPESPanel cbRGB=(65280,59904,48896)
	SetDrawLayer /W=fitVLPESPanel UserBack
	//
	SetDrawEnv /W=fitVLPESPanel /W=fitVLPESPanel linefgc=(65280,48896,48896), fillfgc=(65280,48896,48896)
	DrawRRect /W=fitVLPESPanel 285,514,11,645
	SetDrawEnv /W=fitVLPESPanel /W=fitVLPESPanel linefgc=(65280,48896,48896), fillfgc=(65280,48896,48896)
	DrawRRect /W=fitVLPESPanel 11,437,285,508
	SetDrawEnv /W=fitVLPESPanel linefgc=(48896,65280,48896), fillfgc=(48896,65280,48896)
	DrawRRect /W=fitVLPESPanel 6,61,301,352
	SetDrawEnv /W=fitVLPESPanel linefgc=(49152,65280,32768), fillfgc=(49152,65280,32768)
	DrawRRect /W=fitVLPESPanel 11,80,288,127
	SetDrawEnv /W=fitVLPESPanel linefgc=(57344,65280,48896), fillfgc=(57344,65280,48896)
	DrawRRect /W=fitVLPESPanel 11,327,288,238
	SetDrawEnv /W=fitVLPESPanel linefgc=(32768,65280,32768), fillfgc=(32768,65280,32768)
	DrawRRect /W=fitVLPESPanel 12,127,289,236
	SetDrawEnv /W=fitVLPESPanel /W=fitVLPESPanel fname= "Arial Black",fsize= 16
	DrawText /W=fitVLPESPanel 339,95,"Select a Wave"
	SetDrawEnv /W=fitVLPESPanel linefgc= (65280,54528,48896),fillfgc= (65280,54528,48896)
	DrawRect /W=fitVLPESPanel 3,2,506,53
	SetDrawEnv /W=fitVLPESPanel fsize= 18,textrgb= (13056,0,26112)
	DrawText /W=fitVLPESPanel 11,36,"Fermi Dirac Function with Linear function"
	SetDrawEnv /W=fitVLPESPanel fsize= 14,fstyle= 1
	//
	DrawText /W=fitVLPESPanel 100,78,"Parameters"
	DrawText /W=fitVLPESPanel 66,99,"Instrument (Gaussian)"
	DrawText /W=fitVLPESPanel 65,144,"Fermi Dirac distribution"
	DrawText /W=fitVLPESPanel 30,276,"Linear function (a(Energy)+1-a EF)"
	SetDrawEnv /W=fitVLPESPanel linefgc= (65280,48896,48896),fillfgc= (65280,48896,48896)
	DrawRRect /W=fitVLPESPanel 10,354,288,433
	SetDrawEnv /W=fitVLPESPanel fsize= 48,fstyle= 1,textrgb= (65280,32768,32768)
	DrawText /W=fitVLPESPanel 19,412,"Diff"
	SetDrawEnv /W=fitVLPESPanel fsize= 48,fstyle= 1,textrgb= (65280,32768,32768)
	DrawText /W=fitVLPESPanel 18,501,"Ratio"
	SetDrawEnv /W=fitVLPESPanel fsize= 48,fstyle= 1,textrgb= (65280,32768,32768)
	DrawText /W=fitVLPESPanel 20,566,"All "
	SetDrawEnv /W=fitVLPESPanel fsize= 14,fstyle= 1,textrgb= (65280,32768,32768)
	DrawText /W=fitVLPESPanel 100,555,"(Spectrum, Diff, Ratio)"
	//
	SetVariable setResolution Win=fitVLPESPanel, pos ={28,100}, size={228,21}
	SetVariable setResolution title="Resolution  ( meV )   "
	SetVariable setResolution help={"meV scale"},labelBack=(49152,65280,32768)
	SetVariable setResolution font="Arial",fSize=16
	SetVariable setResolution limits={0,inf,0.1},value= root:Packages:LaserUPS:g_resol,bodyWidth= 80
	//
	SetVariable setTemperature Win=fitVLPESPanel, pos ={30,147}, size={226,21}
	SetVariable setTemperature title="Temperature  ( K )    "
	SetVariable setTemperature help={"K scale"},labelBack=(32768,65280,32768)
	SetVariable setTemperature font="Arial", fSize=16, limits={0,inf,1}
	SetVariable setTemperature value=root:Packages:LaserUPS:g_temperature,bodyWidth= 80
	//
	SetVariable setEVac,Win=fitVLPESPanel, pos ={20,167},size={236,21},title="Vacuum Energy ( eV )"
	SetVariable setEVac,help={"eV scale, set the work function if the final state energy scale is used."}
	SetVariable setEVac,labelBack=(32768,65280,32768),font="Arial",fSize=16
	SetVariable setEVac,limits={-inf,inf,0.001},value= root:Packages:LaserUPS:g_EVac,bodyWidth= 80
	//
	SetVariable setEF,Win=fitVLPESPanel, pos ={30,187},size={226,21},title="EF energy  ( eV )      "
	SetVariable setEF,help={"eV scale, set the photon energy if the final state energy scale is used."}
	SetVariable setEF,labelBack=(32768,65280,32768),font="Arial",fSize=16
	SetVariable setEF,limits={-inf,inf,0.0005},value= root:Packages:LaserUPS:g_EEf,bodyWidth= 80
	//
	SetVariable setInt,Win=fitVLPESPanel, pos ={45,239},size={211,21},title="Intensity                  "
	SetVariable setInt,help={"eV scale, set the photon energy if the final state energy scale is used."}
	SetVariable setInt,labelBack=(57346,65535,49151),font="Arial",fSize=16
	SetVariable setInt,limits={0,inf,1},value= root:Packages:LaserUPS:g_Int,bodyWidth= 80
	//
	ListBox WaveSelectorList, win=fitVLPESPanel, pos ={318,99},size={174,243},frame=2
	MakeListIntoWaveSelector("fitVLPESPanel", "WaveSelectorList", selectionMode=WMWS_SelectionSingle, content=WMWS_Waves)
	//
	CheckBox fromTarget,Win=fitVLPESPanel
	CheckBox fromTarget,pos ={358,364},size={104,18}
	CheckBox fromTarget,title="From Target",font="Arial"
	CheckBox fromTarget,fSize=16,value= 1
	CheckBox fromTarget,proc=CheckProc_SingleCheck
	//
	Button makeWaveButton Win=fitVLPESPanel, pos ={334,397}, size={160,40}
	Button makeWaveButton proc=fitVLPES#fitVLPES,title="Analyze spectrum"
	Button makeWaveButton help={"Build convoluted Data for this spectrum"}
	Button makeWaveButton font="Arial",fSize=14
	//
	Button FromThisWave Win=fitVLPESPanel, pos ={17,329}, size={270,20}
	Button FromThisWave title="Get parameters from this Spectrum"
	Button FromThisWave proc=fitVLPES#GetFDFitParameters
	Button FromThisWave font="Arial",fSize=14
	//
	Button appendfittingCurveButton Win=fitVLPESPanel, pos={334,468}, size={160,40}
	Button appendfittingCurveButton proc=fitVLPES#AppendConvCurve,title="Append Curve "
	Button appendfittingCurveButton help={"Show fitting curve on the graph page"}
	Button appendfittingCurveButton font="Arial"
	//
	Button storefitDataButton Win=fitVLPESPanel, pos ={334,513}, size={160,40}
	Button storefitDataButton title="Store fit result"
	Button storefitDataButton help={"Copy the wave by FDfit_<wave name> and add the fitting parameters in wave note."}
	Button storefitDataButton font="Arial"
	Button storefitDataButton proc=fitVLPES#StoreFittingResultButton
	//
	Button Difference Win=fitVLPESPanel, pos ={118,362},size={75,33}
	Button Difference proc=fitVLPES#MakeDiffFromFD,title="\\K(65535,65535,65535)Make"
	Button Difference font="Arial",fSize=14,fColor=(65280,0,0)
	//
	Button StoreDiff Win=fitVLPESPanel, pos ={206,397},size={64,30}
	Button StoreDiff proc=fitVLPES#StoreDiffFromFDResultButton,title="\\K(65535,65535,65535)Store"
	Button StoreDiff font="Arial",fSize=14,fColor=(65280,0,0)
	Button StoreDiff help={"Copy the wave by Diff_<wave name> and add the fitting parameters in wave note."}
	//
	Button AppendDiffSpect,Win=fitVLPESPanel, pos ={118,397},size={75,31},proc=fitVLPES#AppendDiffCurve,title="\\K(65535,65535,65535)Append"
	Button AppendDiffSpect,font="Arial",fSize=14,fColor=(65280,0,0)
	//
	Button Ratio,Win=fitVLPESPanel, pos ={118,440},size={75,33},proc=fitVLPES#MakeRatioToFD,title="Make"
	Button Ratio,font="Arial",fSize=14,fColor=(65280,65280,48896)
	//
	Button StoreRatio,Win=fitVLPESPanel, pos ={206,475},size={64,30},proc=fitVLPES#StoreRatioToFDResultButton,title="Store"
	Button StoreRatio,font="Arial",fSize=14,fColor=(65280,65280,48896)
	Button StoreRatio,help={"Copy the wave by Ratio_<wave name> and add the fitting parameters in wave note."}
	//
	Button AppendRatioSpect Win=fitVLPESPanel, pos ={118,475},size={75,30}
	Button AppendRatioSpect proc=fitVLPES#AppendRatioCurve,title="Append"
	Button AppendRatioSpect font="Arial",fSize=14,fColor=(65280,65280,48896)
	//
	Button StoreAllFDFit,Win=fitVLPESPanel, pos ={22,568},size={123,30},title="Store",font="Arial"
	Button StoreAllFDFit,fSize=14,fColor=(65280,43520,0)
	Button StoreAllFDFit,proc=fitVLPES#StoreAllFDFitResultButton
	//
	Button MakeAFDFitResTable Win=fitVLPESPanel,pos ={168,604},size={110,35}
	Button MakeAFDFitResTable proc=fitVLPES#MakeAFDFitResTable,title="Make Fitting \rResult Table"
	Button MakeAFDFitResTable help={"Record the All fitting results to root:Packages:LaserUPS:FDFitReults wave"}
	Button MakeAFDFitResTable font="Arial",fColor=(48896,65280,48896)
	//
	Button donebutton,Win=fitVLPESPanel,pos ={354,610},size={140,30},proc=DonePanelButton,title="Done"
	Button donebutton,help={"Close this panel"},font="Arial",fSize=14
	//
	SetVariable LinearCoeff_a,Win=fitVLPESPanel, pos ={84,277},size={172,21},title="Coefiicient a"
	SetVariable LinearCoeff_a,help={"eV scale, set the photon energy if the final state energy scale is used."}
	SetVariable LinearCoeff_a,labelBack=(57344,65280,48896),font="Arial",fSize=16
	SetVariable LinearCoeff_a,value= root:Packages:LaserUPS:g_Coeffa,bodyWidth= 80
	SetVariable LinearCoeff_a,limits={-inf,inf,0.01}
	//
	Button KillDiff Win=fitVLPESPanel, pos ={206,362},size={64,33}
	Button KillDiff proc=fitVLPES#killDiffCurve,title="\\K(65535,65535,65535)Remove \rand Kill"
	Button KillDiff font="Arial",fSize=11,fColor=(65280,0,0)
	Button KillDiff help={"Remove wave (Diff) and then kill this wave"}
	//
	Button KillRatio Win=fitVLPESPanel, pos ={206,440},size={64,33},proc=fitVLPES#KillRatioCurve,title="Remove \rand Kill"
	Button KillRatio font="Arial",fSize=11,fColor=(65280,65280,48896)
	Button KillRatio help={"Remove wave (Ratio) and then kill this wave."}
	//
	Button RemoveAllFDFit Win=fitVLPESPanel, pos ={157,568},size={123,30}
	Button RemoveAllFDFit proc=fitVLPES#KillAllCurves,title="Remove and Kill"
	Button RemoveAllFDFit font="Arial",fSize=14,fColor=(65280,43520,0)
	Button RemoveAllFDFit help={"Remove waves(ConvCurve, Ratio, Diff) and then kill Waves (ConvCurve, Ratio, Diff, FDfit, and gausscurve)"}
	//
	Button UseCursor1 Win=fitVLPESPanel, pos={18,211},size={164,23},title="Cursors for EF & EVac"
	Button UseCursor1 proc=fitVLPES#VLPESFitCursors
	//
	Button UseCursor2 Win=fitVLPESPanel, pos={20,300},size={200,23},title="Cursors for Intensity and slope"
	Button UseCursor2 proc=fitVLPES#VLPESFitCursors
End


static Function fitVLPESPanelHook(infostr)
	String infoStr

	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	Variable statusCode = 0 // 0 if nothing done, else 1

	strswitch (event)
		case "resize":
			GetWindow $win, wsize
			Variable res_ratio
			res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
			MoveWindow /W= $win V_Left, V_Top, V_left+509/ res_ratio, V_top+652/res_ratio
			break;
	endswitch
end


static Function VLPESFitCursors(ctrlName):ButtonControl
	String ctrlName
	String wavenameatCursorA = CsrWave(A)
	String wavenameatCursorB = CsrWave(B)
	If (strlen(wavenameatCursorA)*strlen(wavenameatCursorB)==0)
		Abort "Put Cursors on Graph"
	endif
	Variable xA=xcsr(A)
	Variable xB=xcsr(B)
	Variable yA=vcsr(A)
	Variable yB=vcsr(B)
	If (xA == xB)
		Abort "Cursor A and B indicate the same X"
	endif
	NVar g_EVac = root:Packages:LaserUPS:g_EVac
	NVar g_EEf = root:Packages:LaserUPS:g_EEf
	NVar g_Int = root:Packages:LaserUPS:g_Int
	NVar g_Coeffa = root:Packages:LaserUPS:g_Coeffa
	strswitch(ctrlName)
		case "UseCursor1":
			g_EEf = max(xA, xB)
			g_EVac = min (xA, xB)
			break
		case "UseCursor2":
			g_Int = round((yA+yB)/2)
			g_Coeffa = round((((yA-yB)/(xA-xB))/g_Int)*100)/100
			break
	endswitch
End

static Function GetFDFitParameters(ctrlName):ButtonControl
	String ctrlName
	String objs = WS_SelectedObjectsList("fitVLPESPanel", "WaveSelectorList")
	String waveNameBase = StringFromList(0, objs)
	String FitWaveName = AddPrefix2wavename(waveNameBase, "FDfit_")
	if (waveExists($FitWavename))
		wave FitWave = $FitWavename
		String tmpNote = note(FitWave)
		NVar g_Int = root:Packages:LaserUPS:g_Int
		g_Int = NumberByKey("fit_Intensity", tmpNote)
		NVar g_EVac = root:Packages:LaserUPS:g_EVac
		g_EVac = NumberByKey("fit_EVac", tmpNote)
		NVar g_EEf = root:Packages:LaserUPS:g_EEf
		g_EEf = NumberByKey("fit_Ef", tmpNote)
		NVar g_temperature = root:Packages:LaserUPS:g_temperature
		g_temperature = NumberByKey ("fit_Temperature", tmpNote)
		NVar g_resol = root:Packages:LaserUPS:g_resol
		g_resol = NumberByKey("fit_resolution", tmpNote)
		NVar g_Coeffa = root:Packages:LaserUPS:g_Coeffa
		g_Coeffa = NumberByKey("Coeff_a", tmpNote)
	else
		String smtp = "Can't the fitting parameters.\n "+ "The wave : FDfit_"
		smtp += waveNamebase+" does not exist"
		Abort smtp
	endif
End

static Function fitVLPES(ctrlName): ButtonControl
	String ctrlName
	//
	String objs = WS_SelectedObjectsList("fitVLPESPanel", "WaveSelectorList")
	String waveNameBase = StringFromList(0, objs)
	//	Analyze experimental wave.
	Variable delx
	delx = deltax($waveNameBase)
	Variable npnts
	npnts = numpnts($waveNameBase)
	Variable left_x
	left_x = leftx($waveNameBase)
	//	gauss curve
	String gaussWaveforFitFullPath
	gaussWaveforFitFullPath = "root:Packages:LaserUPS:gausscurve"
	Make /O/N=600 $gaussWaveforFitFullPath
	Wave gausswave = $gaussWaveforFitFullPath
	SetScale/P x -delx * 300 , delx ,"", gausswave
	SetFormula root:Packages:LaserUPS:gausscurve, " instrument(x, g_resol)"
	// Fermi-Dirac Curve
	String FDCurveFullPath
	FDCurveFullPath = "root:Packages:LaserUPS:FDCurve"
	Make /O/N=(npnts) $FDCurveFullPath
	Wave FDCurve = $FDCurveFullPath
	SetScale /P x left_x, delx, "eV", FDCurve
	SetFormula root:Packages:LaserUPS:FDCurve , "(g_Coeffa*x+1-g_Coeffa*g_EEF)*g_Int*(FDVac(x, g_EVac,g_EEf,g_temperature))"
	//  Convoluted Curve
	String ConvCurveFullPath = "root:Packages:LaserUPS:ConvCurve"
	Make /O/N=(npnts) $ConvCurveFullPath
	Wave ConvCurve = $ConvCurveFullPath
	SetScale /P x left_x, delx, "eV", ConvCurve
	SetFormula root:Packages:LaserUPS:ConvCurve, "conv2(p, gausscurve, FDCurve)"
End

static Function MakeDiffFromFD(ctrlName):ButtonControl
	String ctrlName
	//
	String objs = WS_SelectedObjectsList("fitVLPESPanel", "WaveSelectorList")
	String waveNameBase = StringFromList(0, objs)
	//	Analyze experimental wave.
	Variable delx
	delx = deltax($waveNameBase)
	Variable npnts
	npnts = numpnts($waveNameBase)
	Variable left_x
	left_x = leftx($waveNameBase)
	String DiffCurveFullPath = "root:Packages:LaserUPS:Diff"
	Make /O/N=(npnts) $DiffCurveFullPath
	Wave DiffCurve = $DiffCurveFullPath
	SetScale /P x left_x, delx, "eV", DiffCurve
	SetScale d 0,0,"cps eV/mW", DiffCurve
	String cmd
	SetFormula root:Packages:LaserUPS:Diff, GetWavesDataFolder($waveNameBase,2)+"- ConvCurve"
End


static Function MakeRatioToFD(ctrlName): ButtonControl
	String ctrlName
	String objs = WS_SelectedObjectsList("fitVLPESPanel", "WaveSelectorList")
	String waveNameBase = StringFromList(0, objs)
	//	Analyze experimental wave.
	Variable delx
	delx = deltax($waveNameBase)
	Variable npnts
	npnts = numpnts($waveNameBase)
	Variable left_x
	left_x = leftx($waveNameBase)
	String RatioCurveFullPath = "root:Packages:LaserUPS:Ratio"
	Make /O/N=(npnts) $RatioCurveFullPath
	Wave RatioCurve = $RatioCurveFullPath
	SetScale /P x left_x, delx, "eV", RatioCurve
	SetFormula root:Packages:LaserUPS:Ratio, GetWavesDataFolder($waveNameBase,2)+"/ root:Packages:LaserUPS:ConvCurve"
End

static Function AppendConvCurve(ctrlName): ButtonControl
	String ctrlName
	String curWinName
	curWinName = WinName (0,1)
	GetWindow $curWinName, wavelist
	Variable i
	Wave/T W_WaveList = W_WaveList
	Variable isExistOntheCurrentWindow = 0
	for (i =0; i<DimSize(W_WaveList,0);i+=1)
		if (stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:ConvCurve") == 1)
			isExistOnTheCurrentWindow = 1
		endif
	endfor
	//
	if (isExistOnTheCurrentWindow != 1)
		AppendToGraph root:Packages:LaserUPS:ConvCurve
		Print "Append root:Packages:LaserUPS:ConvCurve on the Graph"
	endif
	Killwaves W_WaveList
End

static Function AppendDiffCurve(ctrlName): ButtonControl
	String ctrlName
	String curWinName
	curWinName = WinName (0,1)
	GetWindow $curWinName, wavelist
	Variable i
	Wave/T W_WaveList = W_WaveList
	Variable isExistOnTheCurrentWindow = 0
	for (i =0; i<DimSize(W_WaveList,0);i+=1)
		if (stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:Diff") == 1)
			isExistOnTheCurrentWindow = 1
		endif
	endfor
	//
	If (isExistOnTheCurrentWindow != 1 )
		AppendToGraph /L=Diff root:Packages:LaserUPS:Diff
		ModifyGraph tick(Diff)=2,mirror(Diff)=1,axThick=2,freepos(Diff)=0,lblPos(Diff)=50
		ModifyGraph zero(Diff)=1,zeroThick(Diff)=1
		Label Diff "Diff (\\u)"
		Print "Append root:Packages:LaserUPS:Diff on the Graph"
	endif
	Killwaves W_WaveList
End

static Function AppendRatioCurve(ctrlName): ButtonControl
	String ctrlName
	String curWinName
	curWinName = WinName (0,1)
	GetWindow $curWinName, wavelist
	Variable i
	Wave/T W_WaveList = W_WaveList
	Variable isExistOnTheCurrentWindow = 0
	for (i =0; i<DimSize(W_WaveList,0);i+=1)
		if (stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:Ratio") == 1)
			isExistOnTheCurrentWindow = 1
		endif
	endfor
	//
	If (isExistOnTheCurrentWindow != 1 )
		AppendToGraph /L=Ratio root:Packages:LaserUPS:Ratio
		ModifyGraph tick(Ratio)=2,mirror(Ratio)=1,axThick=2,freepos(Ratio)=0,lblPos(Ratio)=50
		Label Ratio "Ratio"
		SetAxis Ratio -1,10
		Print "Append root:Packages:LaserUPS:Ratio on the Graph"
	endif
	Killwaves W_WaveList
End

static Function StoreFittingResultButton (ctrlName): ButtonControl
	String ctrlName
	StoreFittingResult()
End

static Function StoreFittingResult()
	String objs = WS_SelectedObjectsList("fitVLPESPanel", "WaveSelectorList")
	String waveNameBase = StringFromList(0, objs)
	if (waveExists(root:Packages:LaserUPS:ConvCurve))
		String fit_result
		fit_result = Addprefix2wavename (waveNameBase, "FDfit_")
		Duplicate /O root:Packages:LaserUPS:ConvCurve, $fit_result
		SetFormula $fit_result, ""
		String fittingparameters
		NVar g_Int = root:Packages:LaserUPS:g_Int
		NVar g_EEf = root:Packages:LaserUPS:g_EEf
		NVar g_EVac =root:Packages:LaserUPS:g_EVac
		NVar g_temperature = root:Packages:LaserUPS:g_temperature
		NVar g_resol = root:Packages:LaserUPS:g_resol
		NVar g_Coeffa = root:Packages:LaserUPS:g_Coeffa
		fittingparameters = "fit_Intensity:"+num2str(g_Int)
		fittingparameters += ";fit_Ef:"+num2str(g_EEf)
		fittingparameters += ";fit_EVac:"+num2str(g_EVac)
		fittingparameters += ";fit_Temperature:"+num2str(g_temperature)
		fittingparameters += ";fit_resolution:"+num2str(g_resol)+";"+num2Char(13)
		fittingparameters += ";Coeff_a:"+num2str(g_Coeffa)+";"
		note $fit_result, fittingparameters
		Print "Store the fitting result for the wave : "+ waveNamebase
	else
		String stmp = "Use \"Analyze this spectrum\", first"
		Abort stmp
	Endif
End

static Function StoreDiffFromFDResultButton (ctrlName): ButtonControl
	String ctrlName
	StoreDiffFromFDResult()
End

static Function StoreDiffFromFDResult()
	String objs = WS_SelectedObjectsList("fitVLPESPanel", "WaveSelectorList")
	String waveNameBase = StringFromList(0, objs)
	if (waveExists(root:Packages:LaserUPS:Diff))
		String fit_result
		fit_result = Addprefix2wavename (waveNameBase, "Diff_")
		Duplicate /O root:Packages:LaserUPS:Diff, $fit_result
		Wave fitWave = $fit_result
		Wave diffWave = root:Packages:LaserUPS:Diff
		fitWave = diffWave
		String fittingparameters
		NVar g_Int = root:Packages:LaserUPS:g_Int
		NVar g_EEf = root:Packages:LaserUPS:g_EEf
		NVar g_EVac =root:Packages:LaserUPS:g_EVac
		NVar g_temperature = root:Packages:LaserUPS:g_temperature
		NVar g_resol = root:Packages:LaserUPS:g_resol
		NVar g_Coeffa = root:Packages:LaserUPS:g_Coeffa
		fittingparameters = "fit_Intensity:"+num2str(g_Int) + ";fit_Ef:"+num2str(g_EEf)
		fittingparameters += ";fit_EVac:"+num2str(g_EVac)
		fittingparameters += ";fit_Temperature:"+num2str(g_temperature)
		fittingparameters += ";fit_resolution:"+num2str(g_resol)+";"+num2Char(13)
		fittingparameters += ";Coeff_a:"+num2str(g_Coeffa)+";"
		note fitWave, fittingparameters
		Print "Store the difference spectra for the wave : "+ waveNamebase
	else
		String stmp = "Use \"Make a difference\", first"
		Abort stmp
	Endif
End

static Function StoreRatioToFDResultButton (ctrlName): ButtonControl
	String ctrlName
	StoreRatioToFDResult ()
End

static Function StoreRatioToFDResult()
	String objs = WS_SelectedObjectsList("fitVLPESPanel", "WaveSelectorList")
	String waveNameBase = StringFromList(0, objs)
	if (waveExists(root:Packages:LaserUPS:Ratio))
		String fit_result
		fit_result = Addprefix2wavename(waveNameBase, "Ratio_")
		//
		Duplicate /O root:Packages:LaserUPS:Ratio, $fit_result
		Wave fitWave = $fit_result
		Wave ratioWave = root:Packages:LaserUPS:Ratio
		fitWave = ratioWave
		//
		String fittingparameters
		NVar g_Int = root:Packages:LaserUPS:g_Int
		NVar g_EEf = root:Packages:LaserUPS:g_EEf
		NVar g_EVac =root:Packages:LaserUPS:g_EVac
		NVar g_temperature = root:Packages:LaserUPS:g_temperature
		NVar g_resol = root:Packages:LaserUPS:g_resol
		NVar g_Coeffa = root:Packages:LaserUPS:g_Coeffa
		fittingparameters = "fit_Intensity:"+num2str(g_Int)+";fit_Ef:"+num2str(g_EEf)
		fittingparameters += ";fit_EVac:"+num2str(g_EVac)
		fittingparameters += ";fit_Temperature:"+num2str(g_temperature)
		fittingparameters += ";fit_resolution:"+num2str(g_resol)+";"+num2Char(13)
		fittingparameters += ";Coeff_a:"+num2str(g_Coeffa)+";"
		note fitWave, fittingparameters
		Print "Store the ratio spectra for the wave: "+ waveNamebase
	else
		String stmp = "Use \"Make a Ratio \", first"
		Abort stmp
	Endif
End

static Function StoreAllFDFitResultButton (ctrlName): ButtonControl
	String ctrlName
	StoreFittingResult()
	StoreDiffFromFDResult()
	StoreRatioToFDResult ()
End

static Function MakeAFDFitResTable(ctrlName): ButtonControl //要検討
	String ctrlName
	String wList = WaveList("FDfit_*", ";", "")
	wList = SortList(wList)
	Variable numWaves = ItemsInList(wList)
	Print "See the wave root:Packages:LaserUPS:FDFitResults"
	String FDFitTableFullPath = "root:Packages:LaserUPS:FDFitResults"
	Make /O/T/N=(numWaves, 6) $FDFitTableFullPath
	Wave/T FDFitTable = $FDFitTableFullPath
	SetDimLabel 1, 0, Intensity, FDFitTable
	SetDimLabel 1, 1, Ef, FDFitTable
	SetDimLabel 1, 2, EVac, FDFitTable
	SetDimLabel 1, 3, Temperature, FDFitTable
	SetDimLabel 1, 4, resolution, FDFitTable
	SetDimLabel 1, 5, Coeff_a, FDFitTable
	Variable i
	for (i=0; i<numWaves; i+=1)
		String curFitWaveName = StringFromList(i, wList)
		String baseWaveName = curFitWaveName [6, Inf]
		wave theWave = $curFitWaveName
		String tmpNote = note(theWave)
		Variable Intensity = NumberByKey("fit_Intensity", tmpNote)
		Variable Ef = NumberByKey("fit_Ef", tmpNote)
		Variable EVac = NumberByKey("fit_EVac", tmpNote)
		Variable Temperature = NumberByKey("fit_Temperature", tmpNote)
		Variable resolution = NumberByKey("fit_resolution", tmpNote)
		Variable a = NumberByKey("Coeff_a", tmpNote)
		SetDimLabel 0, i, $baseWaveName, FDFitTable
		FDFitTable[i][%Intensity] = num2str(Intensity)
		FDFitTable[i][%Ef] = num2str(Ef)
		FDFitTable[i][%EVac] = num2str(EVac)
		FDFitTable[i][%Temperature] = num2str(Temperature)
		FDFitTable[i][%resolution] = num2str(resolution)
		FDFitTable[i][%Coeff_a] = num2str(a)
	endfor
End

static Function killDiffCurve(ctrlName) : ButtonControl
	String ctrlName
	String DiffCurveFullPath = "root:Packages:LaserUPS:Diff"
	if (	WaveExists($DiffCurveFullPath))
		RemoveFromGraph/Z Diff
		KillWaves $DiffCurveFullPath				// execute if condition is TRUE
	else
		Abort "You have not made a wave : root:Packages:LaserUPS:Diff	."
	endif
End


static Function KillRatioCurve(ctrlName) : ButtonControl
	String ctrlName
	String RatioCurveFullPath = "root:Packages:LaserUPS:Ratio"
	if (	WaveExists($RatioCurveFullPath))

		RemoveFromGraph/Z Ratio
		KillWaves $RatioCurveFullPath				// execute if condition is TRUE
	else
		Abort "You have not made a wave : root:Packages:LaserUPS:Ratio	."
	endif
End

static Function KillAllCurves(ctrlName) : ButtonControl
	String ctrlName
	if (waveExists(root:Packages:LaserUPS:Ratio))
		KillRatioCurve("Button")
	Endif
	if (waveExists(root:Packages:LaserUPS:Diff))
		killDiffCurve("Button")
	EndIf
	RemoveFromGraph/Z ConvCurve
	KillWaves/Z root:Packages:LaserUPS:ConvCurve
	KillWaves /Z root:Packages:LaserUPS:FDCurve
	KillWaves /Z root:Packages:LaserUPS:gausscurve
End
