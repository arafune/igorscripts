#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVertsion = 8.0
#pragma version=1.00
// Version $Id: SelectFiles.ipf 258 2005-03-15 11:26:42Z arafune $

Function/S SelectFilesIndir (fileType, creator)
	String fileType, creator
	String message = "Set path to Read"
	Variable refNum
	NewPath /Q/O/M=message dirName
	if (V_Flag !=0)
		Abort "The user pressed canncel."
	endif
	PathInfo dirName
	Variable i
	String fileName
	String fileNames=""
	do
		fileName = IndexedFile (dirName, i, fileType, creator)
		if (strlen(fileName)==0 )
			break
		endif
		fileNames=AddListItem(fileName, fileNames)
		i += 1
	while (1)
	fileNames=SortList (fileNames, ";", 4)
	Make /T/O/N=(i) filesListWave1
	Wave /T wn1=filesListWave1
	Make /O/N=(i) filesListWave2
	Wave wn2 = filesListWave2
	for (i=0; i<ItemsInList(fileNames); i+=1)
		wn1[i]=StringFromList(i, fileNames)
	endfor
	//
	String filesList
	String titleInPrompt
	titleInPrompt = "Files in " + S_path
	//
	FileListPanel(wn1,wn2)
	PauseForUser SelectedFiles, SelectedFiles  	// ここで待つような処理を書けないかね？
	String dir_filesList = ""
	WaveStats /Q wn2
	//
	for (i=0; i<V_npnts; i+=1)
		if (wn2[i]!=0)
			dir_filesList = AddListItem(wn1[i], dir_filesList, ";", Inf)
		endif
	endfor
	dir_filesList = AddListItem(S_path, dir_filesList)
	KillWaves wn1
	KillWaves wn2
	if (ItemsInList(dir_filesList)==1)
		Print "No files selected...\nAborted"
		Abort "No files selected...\nAborted"
	endif
	// print dir_filesList // for debug
	return dir_filesList
End

Static Function FileListPanel(wn1, wn2) : Panel
	Wave /T wn1
	Wave wn2
	NewPanel /K=1 /W=(60, 60, 360, 360) as "Select File(s)"
	DoWindow /F/C SelectedFiles
	SetWindow SelectedFiles, hook=SelectedFilesHook
	GetWindow SelectedFiles, wsize
	Variable res_ratio
	res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
	String ctrlBox
	ListBox ctrlBox, listWave=wn1, selWave=wn2, mode=4, fsize=16
	ListBox ctrlBox, size={(V_right-V_left) * res_ratio, \
		((V_bottom-V_top)-50) * res_ratio}
	Button doneButton, pos={((V_right - V_left) / 2 - 20) * \
		res_ratio, (V_Bottom - V_Top -35)*res_ratio}
	Button doneButton,Proc=doneButton
	Button doneButton,title="Done", size={40 * res_ratio, 20 * res_ratio}
End

Function doneButton (ctrlName) : ButtonControl   /// Don't set static!!
	String ctrlName
	DoWindow/K SelectedFiles
End

Function SelectedFilesHook(infoStr)  /// Don't set static
	String infoStr
	String event= StringByKey("EVENT",infoStr)
	String win= StringByKey("WINDOW",infoStr)
	Variable statusCode=0		// 0 if nothing done, else 1
	strswitch(event)	// string switch
		case "resize":		// execute if case matches expression
			GetWindow $win, wsize
			Variable res_ratio
			res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
			Variable winWidth = V_right - V_left
			Variable winHeight = V_Bottom - V_Top
			Variable wintop = V_Top
			Variable winLeft = V_left
			if (winWidth<150 )
				MoveWindow /W=$win winLeft, winTop, winLeft + 150, winTop + winHeight
				winWidth=150
			endif
			if (winHeight < 150 )
				MoveWindow /W=$win winLeft, winTop, winLeft+winWidth, winTop+150
				winHeight = 150
			endif
			ListBox ctrlBox,win = $win
			ListBox ctrlBox,size = {winWidth * res_ratio, (winHeight-50)*res_ratio }
			Button doneButton, win=$win
			Button doneButton, pos = {(winWidth/2 - 20)*res_ratio, (winHeight-35)*res_ratio}
			break;					// exit from switch
	endswitch
End
