#pragma rtGlobals=3    // Use modern global access method
#pragma version=0.99
#pragma IgorVersion=6.1

// Loading SPECS data files into Igor
// (c) SPECS GmbH, Berlin, Germany, 2007-2009.
// For assistance email support@specs.de

//  Revised in part by R. Arafune, Japan, 2011

// *** For use with SP2z format, make sure bunzip2_path_win() points to a 'bunzip2' program. ***

menu  "Load Waves"
	"Load SP2 [SP2z] File...", SPECS_Load_SP2()
end

static function/S bunzip2_path_win32()
	return "c:\\Program Files\\SPECS\\SpecsLab2\\bin\\bunzip2.exe"  // <-- adjust here
  	// This is in fact %SPECS% + "\\SpecsLab2\\bin\\bunzip2.exe",
  	// but Igor (<= 6.1) doesn't give us the environment variable %SPECS%.
end

Function SPECS_Load_SP2() // ask user for options
	variable file, displayRawImage, displayCorrectedImage, usePhysicalAxis
	open/R/F="SPECS P2 Files (*.sp2, *.sp2z):.sp2,.sp2z;All Files:.*;" file
	if (file == 0)
		print "No file given, aborting"
		return -1  // User pressed "Cancel"
	endif
	if(getUserInput(displayRawImage, displayCorrectedImage, usePhysicalAxis) != 0)
		return -1
	endif
	FStatus file
	string path = S_path + S_fileName
	close file
	SPECS_Load_SP2_WithOptions(path, displayRawImage, displayCorrectedImage, usePhysicalAxis)
End

static Function getUserInput(displayRawImage, displayCorrectedImage, usePhysicalAxis)
	variable &displayRawImage,&displayCorrectedImage,&usePhysicalAxis
	string physicalAxis = "Yes;No"
	string images  = "Corrected Raw;Corrected;Raw"
	string physicalAxisDlg, imageSelectionDlg
	Prompt physicalAxisDlg, "Use physical axes for corrected image", popup physicalAxis
	Prompt imageSelectionDlg, "Select the images to display", popup images
	DoPrompt "Options for SP2 files", physicalAxisDlg, imageSelectionDlg

	if (V_Flag)
		return -1
	endif
	if (cmpstr(imageSelectionDlg,"Raw") == 0)
		displayRawImage = 1
		displayCorrectedImage = 0
	elseif (cmpstr(imageSelectionDlg,"Corrected") == 0)
		displayRawImage=0
		displayCorrectedImage = 1
	elseif (cmpstr(imageSelectionDlg,"Corrected Raw") == 0)
		displayRawImage=1
		displayCorrectedImage = 1
	else // senseful default
		displayRawImage=1
		displayCorrectedImage = 1
	endif
	if (cmpstr(physicalAxisDlg,"Yes") == 0)
		usePhysicalAxis = 1
	elseif (cmpstr(physicalAxisDlg,"No") == 0)
		usePhysicalAxis = 0
	else // senseful default
		usePhysicalAxis = 1
	endif
	return 0
End

Function SPECS_Load_SP2_WithOptions(path, displayRawImage, displayCorrectedImage, usePhysicalAxis)
	string path
	variable displayRawImage, displayCorrectedImage, usePhysicalAxis
	variable file
	string pathToOpen = path
	if (cmpstr(LowerStr(Extension(path)), ".sp2z") == 0)
		pathToOpen = bunzip2(path)
	endif
	open/R file as pathToOpen
  	string suffix=".new" // string to append to an already existing folder/wave
	string  fname, newfname,str,group,key,value,imageOrder, wvName, fnameWithPath, contents="", AzimuthLabel
	string ImageType=""
	variable numImages = 1, imageIndex=0, fileSize
	variable line=0, lastRound=0,valRead, numTotalRows, numTotalCols, numKeyValuePairs=0
	variable rowIndex, columnIndex,skipCurrentImage
	variable col1, col2,col3
	variable posEnd, version = 100 // default version
	variable cnt, i
	variable EkinLow, EkinHigh, AzimuthLow, AzimuthHigh
	SetDataFolder root:
	FStatus file
	fnameWithPath = S_path + S_fileName
	fname = S_fileName
	fileSize = V_logEOF
	newfname = S_fileName
	if (strlen(newfname) > 26) // Igor limits the maximum length of names to 31 characters
		newfname = "data"
		fname = "data"
	endif
	if (DataFolderExists(newfname))
		fname += suffix
		do
			newfname = fname + num2str(cnt)
			cnt+=1
		while (DataFolderExists(newfname))
	endif
	fname = S_fileName // reset fname to real fileName
	NewDataFolder/s $newfname
	Make/T/N=100 keys,values
	contents = PadString(contents, fileSize, 0)
	FBinRead file, contents
	close file
	variable numItems = ItemsInList(contents,"\n")
	for (line=0; line < numItems; line+=1)
		str = StringFromList(line,contents,"\n") // only doing this here would be incredible slow,
		                                         // therefore we use LoadWave from
                                                 // and do some index magic
		if(strlen(str) == 0) // hit EOF
			lastRound=1
			str=""
		endif
		str = stripwhitespace(str)
		if (line == 0 && cmpstr(str,"P2") != 0)
			print "File " + fname + " is not a SP2 file"
			return -1
		endif
		if (isCommentLine(str) )
			str = removeCommentChar(str)// just remove the first character in the string.
			str = stripwhitespace(str)
			if (isGroup(str,group))
				if (stringmatch(group,"SPECS P2 Imagefile Version *"))
					posEnd = strsearch(group," ",Inf,1)
					version = str2num(group[posEnd+1,strlen(group)])
					DebugPrintf("file version is %s\r", num2str(version))
				endif
			endif
			if (isKeyValuePair(str,key,value))
				if (cmpstr(key,"Images") == 0)
					numImages = ItemsInList(value," ")
					imageOrder = value
				elseif (cmpstr(key,"ERange") == 0)
					EkinLow = str2num(StringFromList(0,value," "))
					EkinHigh = str2num(StringFromList(1,value," "))
				elseif (cmpstr(key,"aRange") == 0)
					AzimuthLow = str2num(StringFromList(0,value," "))
					AzimuthHigh = str2num(StringFromList(1,value," "))
				elseif (cmpstr(key,"aUnit") == 0)
					AzimuthLabel = "Azimuth [" + value + "]"
				endif
				if (numKeyValuePairs > DimSize(keys,0))
					Redimension/N=(10*DimSize(keys,0)) keys, values
				endif
				keys[numKeyValuePairs] = key
				values[numKeyValuePairs] = value
				numKeyValuePairs +=1
			endif
		else
			valRead = getNumberOfDataColumns(str,col1,col2,col3)
			if (valRead == 3)
				numTotalRows = col1
				numTotalCols = col2
				skipCurrentImage = 0
				ImageType = StringFromList(imageIndex, imageOrder, " ")
//wvName = fname + " [" + StringFromList(imageIndex,imageOrder," ") + "]"
				wvName = fname + " [" + ImageType + "]"
				imageIndex += 1
				if (col1 == 1 && col2 == 1 && col3 == 1) // no data in wave
					print "Skipping empty image of type " + wvName
					skipCurrentImage = 1
				endif
//if (cmpstr(wvName,"Corrected") == 0) // current Image is Corrected
				if (cmpstr(ImageType,"Corrected") == 0) // current Image is Corrected
					if (displayCorrectedImage == 0) // should not be displayed
						skipCurrentImage = 1
					endif
				elseif (cmpstr(ImageType,"Raw") == 0)
//elseif  cmpstr(wvName,"Raw") == 0)
					if (displayRawImage == 0)
						skipCurrentImage = 1
					endif
				endif
				if (skipCurrentImage != 1)
// current line is "640 480 123", data starts in next line
// /L={nameLine, firstLine, numLines, firstColumn, numColumns }
					LoadWave/O/Q/K=0/A/G/N=tmpWave/L={0,line+1, numTotalRows*numTotalCols,0,1} fnameWithPath
					Wave tmpWave = $StringFromList(0,S_waveNames)
					string tmpWvName, promptstr
					tmpWvName = wvName
					if (strlen (wvName) > 31)
						Do
							promptstr = "Re-enter Object name: (The current lenggh is " + num2str(strlen(tmpWvName)) + ".)"
							Prompt tmpWvName, promptstr
							DoPrompt "The length of object name < 31",tmpWvName
						while (strlen(tmpWvName)>31)
					endif
					wvName = tmpWvName
//
					Make/I/U/N=(col1,col2) $wvName
					Wave wv = $wvName
					for (i=0; i < numKeyValuePairs; i+=1)
						Note wv, keys[i] + ":" + values[i]
					endfor
					for (i=0; i < numTotalCols; i+=1)
						wv[][i] = tmpWave[p+i*numTotalRows]
					endfor
					if (usePhysicalAxis && cmpstr(ImageType,"Corrected") == 0)
//if (usePhysicalAxis && cmpstr(wvName,"Corrected") == 0)
						variable deltaE = (EkinHigh - EkinLow) / numTotalRows
						variable deltaA = (AzimuthHigh - AzimuthLow) / numTotalCols
//SetScale/P x, EkinLow + 0.5*deltaE, deltaE,"Kinetic Energy [eV]" wv
//SetScale/P y, AzimuthLow + 0.5*deltaA, deltaA, AzimuthLabel wv
						SetScale/P x, EkinLow + 0.5*deltaE, deltaE,"eV" wv
						SetScale/P y, AzimuthLow + 0.5*deltaA, deltaA, "Degree" wv
					endif
					NewImage wv
				endif
				line= line + numTotalRows *numTotalCols
				KillWaves/Z tmpWave
			endif
		endif
	endfor
	KillWaves/Z keys, values
	string platform = IgorInfo(2)
	if ((cmpstr(LowerStr(Extension(path)), ".sp2z") == 0) &&(Cmpstr(platform,"Macintosh") == 0))
		Deletefile /Z pathToOpen
	endif
	return 0
end

static Function getNumberOfDataColumns(str,col1,col2,col3)
	string str
	variable &col1, &col2, &col3
	variable tmp1, tmp2, tmp3, valRead
	col1 = 0
	col2 = 0
	col3 = 0
	sscanf str, "%d %d %d", tmp1, tmp2, tmp3
	valRead = V_flag
	if(valRead == 1)
		col1 = tmp1
	elseif(valRead == 2)
		col1 = tmp1
		col2 = tmp2
	elseif(valRead == 3)
		col1 = tmp1
		col2 = tmp2
		col3 = tmp3
	else
		valRead = 0
	endif
	return valRead
end

static Function isCommentLine(str)
	string str
	variable ret = cmpstr(str[0],"#")
	if (ret == 0)
		return 1 // is comment line
	else
		return 0 // is no comment line
	endif
end

static Function isGroup(str,group)
	string str, &group
// stripwhitespace を行って先頭と末尾
// のwhitespaceを取り除いた後の文字列が
//　[　で始まって　かつ　］がきちんとある場合
// [ ]を取り除いた文字列をGroupとして、さらに１を返す。
// 例　[Acquisition Parameters] -> Acquisition Parameters
	variable posStart, posEnd
	posStart = strsearch(str,"[",0)
	posEnd = strsearch(str,"]",Inf,1)
	if (posStart == 0 && posEnd != -1)
		group = str[posStart+1,posEnd-1]
		DebugPrintf ("group is %s\r", group)
		return 1
	else
		group = ""
		return 0
	endif
end

static Function/S removeCommentChar(str)
	string str
	if(isCommentLine(str))
		return str[1,strlen(str)]
	else
		return str
	endif
end

static Function isKeyValuePair(str,key,value)
	string str, &key, &value
	variable posStart
	posStart = strsearch(str,"=",0)
	if (posStart != -1)
		key = str[0,posStart-1]
		value = str[posStart+1,strlen(str)]
		key = stripwhitespace(key)
		value = stripwhitespace(value)
		value = stripStringLimiters(value)
		DebugPrintf("key is %s\r", key)
		DebugPrintf("value is %s\r",value)
		return 1
	else
		return 0
	endif
end

static Function/S stripStringLimiters(str)
	string str
//" " で囲まれた文字を取り出す
	variable posStart, posEnd
	posStart = strSearch(str,"\"",0)
	posEnd= strSearch(str,"\"",Inf,1)
	if (posStart != -1 && posEnd != 1 & posEnd != posStart)
		return str[posStart+1,posEnd-1]
	else
		return str
	endif
end

static Function/S stripwhitespace(str)
	string str
 // This function removes the white spaces at the head or the tail of the string.
	variable i=0,j=0, length=strlen(str)
	Debugprintf("before is __%s__\r",str)
	for (i=0 ; i< length; i+=1)
		if (cmpstr(str[i]," ") != 0 && cmpstr(str[i],"\t") != 0 && cmpstr(str[i],"\r") != 0 && cmpstr(str[i],"\n") != 0)
			break
		endif
	endfor
	for (j=length-1 ; j != 0; j-=1)
		if (cmpstr(str[j]," ") != 0 && cmpstr(str[j],"\t") != 0 && cmpstr(str[j],"\r") != 0 && cmpstr(str[j],"\n") != 0)
			break
		endif
	endfor
	str=str[i,j]
	Debugprintf("after is __%s__\r",str)
	return str
end

static Function DebugPrint(str)
	string str
	//print str
end

static Function Debugprintf(format, out)
	String format, out
	//printf format, out
end

static function/S bunzip2(inpath) // returns path to sp2-file or "" (failed)
	string inpath
	string platform = IgorInfo(2)
	if (Cmpstr(platform,"Windows") == 0)
		return bunzip2_win(inpath)
	elseif (Cmpstr(platform,"Macintosh") == 0)
		return bunzip2_mac(inpath)
	else
		printf "ERROR: Unrecognized platform \"%s\"\r",platform
	endif
end

static function/S bunzip2_win(inpath)
	string inpath
// printf "bunzip2_win(\"%s\")\r",inpath
	string tempdir = Specialdirpath("Temporary",0,1,1)
	string outpath = tempdir + "igor_input.sp2"
	string tempfile = outpath + ".bz2"
	CopyFile/O inpath tempfile
	if (V_flag != 0)
		printf "ERROR: CopyFile failed from \"%s\" to \"%s\"\r",inpath,tempfile
		return ""
	endif
	string bunzip2_exe = "\"" + bunzip2_path_win32() + "\""
	string bunzip2_cmd =bunzip2_exe + " --force " // overwrites possibly existing output
	string cmdline = bunzip2_cmd + "\"" + NativePath(tempfile) + "\""
	ExecuteScriptText/B/W=100/Z cmdline
	if (V_flag != 0)
		printf "ERROR: ExecuteScriptText failed for command line:\r%s\r",cmdline
	return ""
endif
	return outpath
end

static function/S bunzip2_mac(inpath)
	string inpath
// printf "bunzip2_mac(\"%s\")\r",inpath
//printf "ERROR: MacOS not yet supported"
	string tempdir=Specialdirpath("Temporary", 0, 0, 1)
	string outpath = tempdir + ParseFilePath(3, inpath, ":", 0, 0)+".sp2"
	string tempfile = outpath + ".bz2"
	CopyFile /O inpath tempfile
	if (V_flag!=0)
		printf "ERROR: CopyFile failed from \"%s\" to \"%s\"\r",inpath,tempfile
		return ""
	endif
	string bunzip2_cmd = "do shell script \"/usr/bin/bunzip2 --force "
	string cmdline = bunzip2_cmd +ParseFilepath(5, tempfile, "/", 0,0) +"\""
	ExecuteScriptText /B/Z cmdline
	if (V_flag != 0)
		printf "ERROR: ExecuteScriptText failed for command line:\r%s\r",cmdline
		return ""
	endif
	return outpath
end

static function/S NativePath(path) // convert to native path separators
	string path
	return ParseFilePath(5,path,"*",0,0)
end

static function/S Basename(path) // filename without dir or extension
	string path
	return ParseFilePath(3,path,":",0,0)
end

static function/S Extension(path) // ".<ext>" or ""
	string path
	string ext = ParseFilePath(4,path,":",0,0)
	if (cmpstr(ext,"") == 0)
		return ""
	else
		return "." + ext
	endif
end
