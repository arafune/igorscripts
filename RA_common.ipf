/// @file RA_common.ipf
/// @brief Convenient functions for my work.
#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 8.0
#pragma version = 2.0
#pragma ModuleName = RACommon

// Version $Id: RA_common.ipf 409 2006-08-14 04:08:10Z arafune $

Function DonePanelButton(B_struct): ButtonControl
	STRUCT WMButtonAction &B_Struct
	if (B_Struct.eventCode == 2)
		String curWinName = B_Struct.win
		DoWindow /K $curWinName
	endif
End

Menu "Graph"
	"Change Symbol colors as Rainbow/2", ChangeSymbolColors("Rainbow")
	"Add default Legend/3", Legend/F=0/A=RT
	"Offset plots", ShiftPlots(0,0)
End

/// @param ColorTable        Coloar Table
Function ChangeSymbolColors(String ColorTable)
	ColorTab2Wave $ColorTable
	Wave M_colors
	Variable colorTabSize = DimSize(M_colors, 0)
	String TraceList = TraceNameList("", ";", 1)
	Variable NumTrace = ItemsInList(TraceList)
	Variable i = 0
	Variable ColorRow = 0
	Variable ColorRowDiff
	Variable ColorRowInt
	ColorRowDiff =  (colorTabSize-1) / (NumTrace-1)
	Wave cWave = M_colors
	String curTrace
	PauseUpdate
	do
		curTrace = StringFromList(i, TraceList)
		ModifyGraph rgb($curTrace) = (cWave[ColorRowInt][0], \
			cWave[ColorRowInt][1], \
			cWave[ColorRowInt][2])
		i += 1
		ColorRow += ColorRowDiff
		ColorRowInt = round(ColorRow)
	while (i < NumTrace)
	KillWaves cWave
	ResumeUpdate
End

/// @param xshift          Value for the plot shift amount along X-direction
/// @param yshift          Value for the plot shift amount along Y-direction
Function ShiftPlots(Variable xshift, Variable yshift)
	if (xshift==0 && yshift==0)
		ShiftPlotsPanel()
		PauseForUser ShiftPlotsWindow
		NVAR V_ShiftPlotsPanel = root:Packages:LaserUPS:V_ShiftPlotsPanel
		if (V_ShiftPlotsPanel==0)
			Abort
		else
			NVAR ShiftX = root:Packages:LaserUPS:ShiftX
			NVAR ShiftY = root:Packages:LaserUPS:ShiftY
			xshift = ShiftX
			yshift = ShiftY
		endif
	endif
	//
	String wList = TraceNameList("", ";", 1)
	Variable numTraces = ItemsInList(wList)
	Variable i = 0
	Variable del_x = 0
	Variable del_y = 0
	String curTrace
	if (V_ShiftPlotsPanel==1)
		for (i=0; i<numTraces; i+=1)
			curTrace = StringFromList(i, wLIst)
			del_x = del_x + xshift
			del_y = del_y + yshift
			ModifyGraph offset($curTrace)={del_x, del_y}
		endfor
	elseif(V_ShiftPlotsPanel==2)
		for (i=numTraces; i>=0; i-=1)
			curTrace = StringFromList(i, wLIst)
			del_x = del_x + xshift
			del_y = del_y + yshift
			ModifyGraph offset($curTrace)={del_x, del_y}
		endfor
	endif
End


Function ShiftPlotsPanel() : Panel
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	Variable /G root:Packages:LaserUPS:ShiftX
	Variable /G root:Packages:LaserUPS:ShiftY
	Variable /G root:Packages:LaserUPS:V_ShiftPlotsPanel
	Variable w_width = 246
	Variable w_height = 144
	SetDataFolder dfSave
	NewPanel /K=1 /W=(83, 60, 83 +w_width, 60+w_height) as "Shift plots"
	DoWindow /F/C ShiftPlotsWindow
	SetWindow ShiftPlotsWindow, sizeLimit={w_width, w_height, w_width, w_height}
	GetWindow ShiftPlotsWindow, wsize
	SetDrawLayer UserBack
	CheckBox ReverseShift, pos={158, 22}, size={71, 16}, title="Reverse", font="Arial"
	CheckBox ReverseShift, fSize=14,value= 0
	//
	SetVariable ShiftX, pos={18, 47}, size={204, 19}, title="X Shift", font="Arial"
	SetVariable ShiftX, fSize=14, value=V_Flag, bodyWidth= 160
	SetVariable ShiftX, value=root:Packages:LaserUPS:ShiftX
	//
	SetVariable ShiftY, pos={18, 78}, size={205, 19},title="Y Shift",font="Arial"
	SetVariable ShiftY, fSize=14, value= V_Flag, bodyWidth= 160
	SetVariable ShiftY, value=root:Packages:LaserUPS:ShiftY
	//
	Button Cancel, pos={18, 114}, size={80, 20}, title="Cancel", font="Arial",fSize=14
	Button Cancel, Proc=RACommon#ShiftPlotsButtonsProc
	Button continue,pos={105, 114},size={80, 20},title="Continue",font="Arial"
	Button continue,Proc=RACommon#ShiftPlotsButtonsProc
	Button continue,fSize=14
	Button Help,pos={192, 114},size={43, 20},title="Help",font="Arial",fSize=14
	Button Help, Proc=RACommon#ShiftPlotsButtonsProc
EndMacro


Static Function ShiftPlotsButtonsProc(B_Struct)
	STRUCT WMButtonAction &B_Struct
	//
	NVAR V_ShiftPlotsPanel = root:Packages:LaserUPS:V_ShiftPlotsPanel
	//
	switch(B_Struct.eventCode)
		case 2: // mouse up
			strswitch(B_Struct.ctrlName)
				case "Cancel":
					V_ShiftPlotsPanel = 0
					DoWindow/K ShiftPlotsWindow
					break
				case "Continue":
					ControlInfo /W=ShiftPlotsWindow ReverseShift
					V_ShiftPlotsPanel = 1 + V_Value
					DoWindow/K ShiftPlotsWindow
					break
				case "Help":
					Print "Not inpremented..."
					break
			endswitch
	endswitch
End


Function ChangeNormalizedGraph()
	Wave /Z wavea = CsrWaveRef(A)
	Wave /Z waveb = CsrWaveRef(B)
	Variable xA = NaN
	Variable xB = NaN
	if (WaveExists(wavea))
		xA = xcsr(A)
	endif
	if (WaveExists(waveb))
		xB = xcsr(B)
	endif
	Variable maxx = max(xA, xB)
	Variable minx = min(xA, xB)
	String m
	String topWinName =WinName(0, 1)
	m = WinRecreation(topWinName, 4)
	Execute(m)
	topWinName = WinName(0, 1)
	//
	Variable i = 0
	String fName
	String dfSave = GetDataFolder(1)
	Wave theWave = WaveRefIndexed(topWinName, i, 1)
	String toWaveName
	Variable normalizedFactor = 1
	do
		fName = GetWavesDataFolder(theWave, 2)
		fName = ParseFilePath(1, fName, ":", 1, 0)
		SetDataFolder fName
		NewDataFolder /O HNormalized
		ToWaveName = fName+"HNormalized:"+PossiblyQuoteName(NameOfWave(theWave))
		String fromWaveName = NameOfWave(theWave)
		Duplicate /O theWave $(ToWaveName)
		ReplaceWave trace=$fromWaveName, $(ToWaveName)
		Wave NormalizedWave = $(ToWaveName)
		if (numtype(maxx)==2 || numtype(minx)==2)
			normalizedFactor = WaveMax(NormalizedWave)
		else
			normalizedFactor = WaveMax(NormalizedWave, minx, maxx)
		endif
		NormalizedWave /= normalizedFactor
		String notetext = ";NormalizedBY:" + num2str(normalizedFactor) + ";"
		note NormalizedWave, notetext
		i += 1
		Wave theWave = WaveRefIndexed(topWinName, i, 1)
	while(WaveExists(theWave))
	SetDataFolder dfSave
End

/// @param n     Integer number
Function Fraction(Variable n)
	if (n!=ceil(n))
		return NaN
	endif
	if (n<0)
		return NaN
	endif
	if (n==0)
		return 1
	endif
	Variable f = 1
	Variable i
	for (i=1; i<=n; i+=1)
		f = f * i
	endfor
	return f
End


///
Function /S AddPrefix2wavename(String src, String prefix)
	String folderpath = ParseFilePath(1, src, ":", 1, 0)
	String wn = ParseFilePath(0, src, ":", 1, 0)
	wn = prefix + wn
	wn = folderpath + wn
	return wn
End


Function swapwavename(String first, String second)
	Rename $first, tmpwave
	Rename $second $first
	Rename tmpwave $second
End


/// @param colro_tab_wave
/// @param alpha
Function add_alpha_to_color_table(Wave color_tab_wave, Variable alpha)
	String ColorTable_folder = GetWavesDataFolder(color_tab_wave, 1)
	String color_tab_alpha = NameOfWave(color_tab_wave) + "_" + num2str(alpha)
	Duplicate/O color_tab_wave $color_tab_alpha
	InsertPoints /M=1 3, 1, $color_tab_alpha
	wave the_Wave = $color_tab_alpha
	the_wave[][3] = round(alpha * 65536)
	MoveWave the_wave, $ColorTable_folder
End

/// @param str     String that may contain the ascii sequence.
Function/S multiline2list(String str)
//	String tmp_str ReplaceString("\n", str, "")
//	return TrimString(ReplaceString("\r", tmp_str, ";"))
	return TrimString(ReplaceString("\r", ReplaceString("\n", str, ""), ";"))
End


/// provide a zero-padding
/// @param input_str    input string
/// @param num          number of digits
Function /S zfill(String input_str, Variable num)
//  zfill("3", 5) -> 00003
	Variable len = strlen(input_str)
	String output_str
	Variable i
	String zeros = "0"
	if (len >= num)
		output_str = input_str
	else
		for (i=1; i<(num-len);i++)
			zeros += "0"
		endfor
		output_str = zeros + input_str
	endif
	return output_str
End

/// @param source     source wave
/// @param key        parameter key name such as "beta" and "position"
Function fetchValuefromWaveNote(Wave src, String key)
	String note_str = note(src)
	String note_list = multiline2list(note_str)
	String tmpstr
	do
		tmpstr = ReplaceString("; ", note_list, ";")
		if (cmpstr(note_list, tmpstr) == 0)
			break
		else
			note_list = tmpstr
		endif
	while (1)
	return NumberByKey(key, note_list)
End

/// @param source    source wave
/// @param key       parameter key name such as "Beta angle"
Function/S fetchStringfromWaveNote(wave src, String key)
	String note_str = note(src)
	String note_list = multiline2list(note_str)
	String tmpstr
	do
		tmpstr = ReplaceString("; ", note_list, ";")
		if (cmpstr(note_list, tmpstr) == 0)
			break
		else
			note_list = tmpstr
		endif
	while (1)
	return StringByKey(key, note_list)
End

/// @param src      source wave
/// @param q0       first Column number
/// @param q1       last Column number
/// @param p0       [Optional, default = 0] first Row number
/// @param p1       [Optional, default = DimSize(src, 0)-1] last Row number
Function SelectedAreaSum(Wave src, Variable q0, Variable q1, [Variable p0, Variable p1])
	if (ParamIsDefault(p0))
		p0 = 0
	endif
	if (ParamIsDefault(p1))
		p1 = DimSize(src, 0)-1
	endif
	WaveStats /Q/RMD=[p0, p1][q0, q1] src
	return V_sum
End
