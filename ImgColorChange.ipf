﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later
#pragma modulename=IMGCOLOR
#include <WMImageInfo>

Menu "Image"
	"Image Color Control", ImgColorModify()
End

Function ImgColorModify()
	//
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	//
	Variable /G root:Packages:LaserUPS:ZCOLOR_L_VALUE
	Variable /G root:Packages:LaserUPS:ZCOLOR_H_VALUE
	Variable /G root:Packages:LaserUPS:ZCOLOR_L_REL
	Variable /G root:Packages:LaserUPS:ZCOLOR_H_REL
	String targetwindow = WinName(0, 1)
	String /G root:Packages:LaserUPS:ImgTargetWin = targetwindow
	GetWindow $targetwindow, wsize
	Variable w_width = 250
	Variable w_height = 440
	if (WinType("ImageCOlorMod")==7)
		DoWindow /F ImageColorMod
	else
		DrawPanel(w_width, w_height, targetwindow)
	endif
End


/// @param w_width
/// @param w_height
/// @param targetwindow
static Function DrawPanel(Variable w_width, Variable w_height, String targetwindow)
	Variable /G root:Packages:LaserUPS:ZCOLOR_L_VALUE
	Variable /G root:Packages:LaserUPS:ZCOLOR_H_VALUE
	Variable /G root:Packages:LaserUPS:ZCOLOR_L_REL
	Variable /G root:Packages:LaserUPS:ZCOLOR_H_REL
	NVAR ZCOLOR_L_REL = root:Packages:LaserUPS:ZCOLOR_L_REL
	NVAR ZCOLOR_H_REL = root:Packages:LaserUPS:ZCOLOR_H_REL
	NVAR ZCOLOR_L_VALUE = root:Packages:LaserUPS:ZCOLOR_L_VALUE
	NVAR ZCOLOR_H_VALUE = root:Packages:LaserUPS:ZCOLOR_H_VALUE
	//
	GetWindow $targetwindow, wsize
	SetWindow $targetwindow, hook=IMGCOLOR#followpanelhook
	NewPanel /N=ImageColorMod /W=(V_right, V_top, V_right+w_width, V_top+w_height)
	DoWindow /F/C ImageColorMod
	SetWindow ImageColorMod sizeLimit = {w_width, w_height, w_width, w_height}
	//
	String images = ImageNameList(targetwindow, ";")
	String first_image = StringFromList(0, images)
	//
	Variable zcolor_l_auto = NumVarOrDefault("root:Packages:LaserUPS:ZCOLOR_L_AUTO", 1)
	Variable /G root:Packages:LaserUPS:ZCOLOR_L_AUTO = zcolor_l_auto
	Variable zcolor_h_auto = NumVarOrDefault("root:Packages:LaserUPS:ZCOLOR_H_AUTO", 1)
	Variable /G root:Packages:LaserUPS:ZCOLOR_H_AUTO = zcolor_H_auto
	//
	TitleBox Title pos={46.67, 4.00},size={159.33, 25.33}
	TitleBox Title title="Image Color Controller",fSize=16
	//
	PopupMenu WAVESEL pos={16.00, 38.00},size={94.00, 19.33}
	PopupMenu WAVESEL title="2DWave: ", fSize=13
	PopupMenu WAVESEL mode=1, popvalue=first_image
	popupMenu WAVESEL mode=1, value=#("\"" + images +"\"")
	popupMenu WAVESEL proc=IMGCOLOR#wavesel_proc
	//
	Wave image_wave = ImageNameToWaveRef(targetwindow, first_image)
	Variable /G root:Packages:LaserUPS:IMAGEZMAX = WaveMax(image_wave)
	Variable /G root:Packages:LaserUPS:IMAGEZMin = WaveMin(image_wave)
	NVAR IMAGEZMAX = root:Packages:LaserUPS:IMAGEZMAX
	NVAR IMAGEZMIN = root:Packages:LaserUPS:IMAGEZMIN
	//
	PopupMenu ColorTab pos={20, 63.33},size={192.67, 19.33}, fSize=13
	String current_color_table
	current_color_table = StringFromlist(2, \
		WM_ImageColorTabInfo(targetwindow, first_image), ",")
	PopupMenu ColorTab mode=WHichLIstItem(current_color_table, cTabList())+1
	PopupMenu ColorTab value=#"\"*COLORTABLEPOP*\""
	PopupMenu ColorTab bodywidth=200

	PopupMenu ColorTab proc=IMGCOLOR#ColorTabProc
	//
	CheckBox  ColorRev pos={225, 64},size={43.33, 16.00}
	CheckBox  ColorRev title="",fSize=13
	CheckBOx  ColorRev proc=IMGCOLOR#ColorRevProc
	//
	CheckBox AUTOH pos={12, 140.00},size={43.33, 16.00}
	CheckBox AUTOH title="Auto",fSize=13
	CheckBox AUTOH value=zcolor_h_auto,mode=1
	CheckBox AUTOH proc=IMGCOLOR#zColor_auto_check
	CheckBox AUTOL pos={12, 280},size={43.33, 16.00}
	CheckBox AUTOL title="Auto",fSize=13
	CheckBox AUTOL value=zcolor_l_auto, mode=1
	CheckBox AUTOL proc=IMGCOLOR#zColor_auto_check
	//
	CheckBox MANH pos={12, 160},size={14.67, 16.00},MODE=1
	CHeckBox MANH title="",fSize=13,value=!zcolor_h_auto
	CheckBOx MANH proc=IMGCOLOR#zColor_man_check
	CheckBox MANL pos={12, 300.00},size={14.67, 16.00}, MODE=1
	CheckBox MANL title="",fSize=13,value=!zcolor_l_auto
	CheckBox MANL proc=IMGCOLOR#zColor_man_check
	//
	SetVariable SET_H_REL pos={32, 160},size={60, 17.33}, title=" "
	SetVariable SET_H_REL fSize=12, value=ZCOLOR_H_REL
	SetVariable SET_H_REL proc=IMGCOLOR#set_rel_proc
	SetVariable SET_H_REL format="%3.1f %"
	SetVariable SET_L_REL pos={32, 300.00},size={60.00, 17.33}, title=" "
	SetVariable SET_L_REL fSize=12, value=ZCOLOR_L_REL
	SetVariable SET_L_REL proc=IMGCOLOR#set_rel_proc
	SetVariable SET_L_REL format="%3.1f %"
	//
	Slider HSlider pos={11, 190.00},size={220, 230},fSize=14
	Slider HSlider limits={0, 100, 1},value=ZCOLOR_L_REL, vert=0
	Slider HSlider live=0, proc=IMGCOLOR#slider_proc
	Slider LSlider pos={11.00, 330.00},size={220, 230},fSize=14
	Slider LSlider limits={0, 100, 1},value=ZCOLOR_L_REL
	Slider LSlider live=0, proc=IMGCOLOR#slider_proc, vert=0
	//
	SetVariable SET_H_ABS pos={120, 160}, title="Z\\Bmax"
	SetVariable SET_H_ABS size={120.00, 17.33}, fSize=12
	ZCOLOR_H_VALUE = absolute_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_H_REL)
	SetVariable SET_H_ABS value=ZCOLOR_H_VALUE
	SetVariable SET_H_ABS proc=IMGCOLOR#set_abs_proc
	SetVariable SET_L_ABS pos={120, 300}, title="Z\\Bmin"
	SetVariable SET_L_ABS size={120.00, 17.33}, fSize=12
	ZCOLOR_L_VALUE = absolute_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_L_REL)
	SetVariable SET_L_ABS value=ZCOLOR_L_VALUE
	SetVariable SET_L_ABS proc=IMGCOLOR#set_abs_proc
	//
	GroupBox groupH pos={5, 120},size={240, 140.0}
	GroupBox groupL pos={5, 265},size={240.0, 140.0}
	Button close pos={101.33, 410},size={50.0, 20.0}
	Button close title="Close", fSize=16
	Button close proc=IMGCOLOR#closebutton
End


/// @param infostr
static Function followpanelhook(infostr)
	String infostr
	string event = StringByKey("EVENT", infostr)
	string win_name = StringByKey("WINDOW", infostr)
	Variable w_width = 250
	Variable w_height = 440
	strswitch(event)
		case "moved":
			GetWindow $win_name, wsize
			MoveWindow /W=ImageColorMod V_right, V_top, V_right+w_width, V_top+w_height
		break;
	endswitch
End


static Function ImageColorUpdate()
	SVAR targetwindow = root:Packages:LaserUPS:ImgTargetWin
	String images = ImageNameList(targetwindow, ";")
	Variable num_images = itemsinList(images)
	Variable i
	for (i=0; i<itemsinList(images); i++)
		String image = StringFromList(i, images)
		String color_tabinfo =WM_ImageColorTabInfo(targetwindow, image)
		String cmd = "ModifyImage /W="+targetwindow+" "
		cmd += image+ " ctab={"+color_tabinfo+"}"
		Execute/P /Z cmd
	endfor
End

static Function wavesel_proc(pup): popupMenuControl
	STRUCT WMPopupAction &pup
	//
	SVar targetwindow = root:Packages:LaserUPS:ImgTargetWin
	NVAR IMAGEZMAX = root:Packages:LaserUPS:IMAGEZMAX
	NVAR IMAGEZMIN = root:Packages:LaserUPS:IMAGEZMIN
	NVAR ZCOLOR_L_VALUE = root:Packages:LaserUPS:ZCOLOR_L_VALUE
	NVAR ZCOLOR_H_VALUE = root:Packages:LaserUPS:ZCOLOR_H_VALUE
	NVAR ZCOLOR_L_REL = root:Packages:LaserUPS:ZCOLOR_L_REL
	NVAR ZCOLOR_H_REL = root:Packages:LaserUPS:ZCOLOR_H_REL
	//
	switch (pup.eventCode)
		case 2: // mouse up
			String image_name = pup.popStr
			//
			Wave image_wave = ImageNameToWaveRef(targetwindow, image_name)
			IMAGEZMAX = WaveMax(image_wave)
			IMAGEZMIN = WaveMin(image_wave)
			String cname
			cname = StringFromlist(2, \
				WM_ImageColorTabInfo(targetwindow, image_name), ",")
			PopupMenu ColorTab,mode=WHichLIstItem(cname, cTabList())+1
			//
			String zmin
			zmin = StringFromlist(0, WM_ImageColorTabInfo(targetwindow, image_name), ",")
			if (cmpstr(zmin, "*")==0)
				CheckBox AUTOL value=1
				Checkbox MANL value=0
				ModifyImage /W=$targetwindow $image_name, ctab={*, , }
			else
				CheckBox AUTOL value=0
				Checkbox MANL value=1
				ZCOLOR_L_VALUE = str2num(zmin)
				ZCOLOR_L_REL = relative_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_L_VALUE)
				ModifyImage /W=$targetwindow $image_name, ctab={str2num(zmin), , }
			endif
			//
			String zmax
			zmax = StringFromlist(1, \
				WM_ImageColorTabInfo(targetwindow, image_name), ",")
			if (cmpstr(zmax, "*")==0)
				CheckBox AUTOH value=1
				Checkbox MANH value=0
				ModifyImage /W=$targetwindow $image_name, ctab={*, , }
			else
				CheckBox AUTOH value=0
				Checkbox MANH value=1
				ZCOLOR_H_VALUE = str2num(zmax)
				ZCOLOR_H_REL = relative_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_H_VALUE)
				ModifyImage /W=$targetwindow $image_name, ctab={, str2num(zmax), }
			endif
			//
			String reverse_flag
			reverse_flag = StringFromlist(3, \
				WM_ImageColorTabInfo(targetwindow, image_name), ",")
			CheckBox ColorRev, value=str2num(reverse_flag)
			ModifyImage /W=$targetwindow $image_name, ctab={,,, str2num(reverse_flag)}
			//ZCOLOR_L_VALUE = absolute_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_L_REL)
			//SetVariable SET_L_ABS,value=ZCOLOR_L_VALUE
			//ZCOLOR_H_VALUE = absolute_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_H_REL)
			//SetVariable SET_H_ABS,value=ZCOLOR_H_VALUE
		break
	endswitch
End

static Function ColorTabProc(pup): popupMenuControl
	STRUCT WMPopupAction &pup
	String color_tab = pup.popStr
	//
	ControlInfo /W=imageColorMod WAVESEL
	String image_name = S_Value
	SVAR targetwindow = root:Packages:LaserUPS:ImgTargetWin
	//
	switch (pup.eventCode)
		case 2: // mouse up
			ModifyImage /W=$targetwindow $image_name, ctab={*, , $color_tab}
		break
	endswitch
End

static Function ColorRevProc(cba): CheckBoxControl
	STRUCT WMCheckboxAction &cba
//
	ControlInfo /W=imageColorMod WAVESEL
	String image_name = S_Value
	SVAR targetwindow = root:Packages:LaserUPS:ImgTargetWin
	switch (cba.eventCode)
		case 2:
			Variable checked = cba.checked
			ModifyImage /W=$targetwindow $image_name, ctab={*, , , checked}
		break
	endswitch
End

static Function set_abs_proc(sv): SetVariableControl
	STRUCT	WMSetVariableAction &sv
	//
	NVar ZCOLOR_H_REL = root:Packages:LaserUPS:ZCOLOR_H_REL
	NVAR ZCOLOR_L_REL = root:Packages:LaserUPS:ZCOLOR_L_REL
	//
	NVar ZCOLOR_H_VALUE = root:Packages:LaserUPS:ZCOLOR_H_VALUE
	NVAR ZCOLOR_L_VALUE = root:Packages:LaserUPS:ZCOLOR_L_VALUE
	//
	NVAR IMAGEZMAX = root:Packages:LaserUPS:IMAGEZMAX
	NVAR IMAGEZMIN = root:Packages:LaserUPS:IMAGEZMIN
	switch (sv.eventCode)
		case -3: // Control received keyboard focus (Igor8 or later)
		case -2: // Control received keyboard focus
		case -1: // Control being killed
		// case 1: //Mouse up
		// case 2: //Enter key
		// case 3: //Liveupdate
			break
		default:
			if (sv.eventCode & 6) // 6 Value changed by dependency update
				if (cmpstr(sv.ctrlName, "SET_L_ABS")==0)
					ZCOLOR_L_VALUE = sv.dval
					ZCOLOR_L_REL = relative_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_L_VALUE)
				else
					ZCOLOR_H_VALUE = sv.dval
					ZCOLOR_H_REL = relative_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_H_VALUE)
				endif
			endif
		break
	endswitch
End


static Function set_rel_proc(sv): SetVariableControl
	STRUCT	WMSetVariableAction &sv
	//
	NVar ZCOLOR_H_REL = root:Packages:LaserUPS:ZCOLOR_H_REL
	NVAR ZCOLOR_L_REL = root:Packages:LaserUPS:ZCOLOR_L_REL
	//
	NVar ZCOLOR_H_VALUE = root:Packages:LaserUPS:ZCOLOR_H_VALUE
	NVAR ZCOLOR_L_VALUE = root:Packages:LaserUPS:ZCOLOR_L_VALUE
	//
	NVAR IMAGEZMAX = root:Packages:LaserUPS:IMAGEZMAX
	NVAR IMAGEZMIN = root:Packages:LaserUPS:IMAGEZMIN
	//
	SVAR targetwindow = root:Packages:LaserUPS:ImgTargetWin
	String image_name
	switch (sv.eventCode)
		case -3:
		case -2:
		case -1:
			break
		default:
			if (sv.eventCode & 6)
				if (cmpstr(sv.ctrlName, "SET_L_REL")==0)
					ZCOLOR_L_REL = sv.dval
					Slider LSlider, value = ZCOLOR_L_REL
					ZCOLOR_L_VALUE = absolute_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_L_REL)
					SetVariable SET_L_ABS, value=ZCOLOR_L_VALUE
					ControlInfo 	/W=imageColorMod AUTOL
					if (V_Value==0)
						ControlInfo /W=imageColorMod WAVESEL
						image_name = S_Value
						ModifyImage /W=$targetwindow $image_name, ctab={ZColor_L_VALUE, ,}
					endif
				else
					ZCOLOR_H_REL = sv.dval
					Slider HSlider, value = ZCOLOR_H_REL
					ZCOLOR_H_VALUE = absolute_pos(IMAGEZMIN, IMAGEZMAX, ZCOLOR_H_REL)
					SetVariable SET_H_ABS, value=ZCOLOR_H_VALUE
					ControlInfo /W=imageColorMod AUTOH
					if (V_Value==0)
						ControlInfo /W=imageColorMod WAVESEL
						image_name = S_Value
						ModifyImage /W=$targetwindow $image_name, ctab={, ZColor_H_VALUE,}
					endif
				endif
			endif
		break
	endswitch
End

static Function slider_proc(s): SliderControl
	STRUCT WMSliderAction &s
	String slider_name = s.ctrlName
	Variable curval
	//
	NVar ZCOLOR_H_REL = root:Packages:LaserUPS:ZCOLOR_H_REL
	NVAR ZCOLOR_L_REL = root:Packages:LaserUPS:ZCOLOR_L_REL
	//
	NVar ZCOLOR_H_VALUE = root:Packages:LaserUPS:ZCOLOR_H_VALUE
	NVAR ZCOLOR_L_VALUE = root:Packages:LaserUPS:ZCOLOR_L_VALUE
	//
	NVAR IMAGEZMAX = root:Packages:LaserUPS:IMAGEZMAX
	NVAR IMAGEZMIN = root:Packages:LaserUPS:IMAGEZMIN
	//
	switch (s.eventCode)
		case -3:
		case -2:
		case -1:
			break
		default:
			if (s.eventCode & 1) //Value set
				if (cmpstr(s.ctrlName, "LSlider")==0)
					ZCOLOR_L_REL = s.curval
					SetVariable SET_L_REL, value = ZCOLOR_L_REL
				else
					ZCOLOR_H_REL = s.curval
					SetVariable SET_H_REL, value = ZCOLOR_H_REL
				endif
			endif
			if (s.eventCode & 8) // Mouse moved or arrow key moved the slider
				if (cmpstr(s.ctrlName, "LSlider")==0)
					ZCOLOR_L_REL = s.curval
					SetVariable SET_L_REL, value = ZCOLOR_L_REL
				else
					ZCOLOR_H_REL = s.curval
					SetVariable SET_H_REL, value = ZCOLOR_H_REL
				endif
			endif
		break
	endswitch
End


static Function zColor_auto_check(cba): CheckBoxControl
	STRUCT WMCheckboxAction &cba
	ControlInfo /W=imageColorMod WAVESEL
	String image_name = S_Value
	SVAR targetwindow = root:Packages:LaserUPS:ImgTargetWin
	if (cmpstr(cba.ctrlName , "AUTOL")==0)
		NVAR ZCOLOR_L_AUTO = root:Packages:LaserUPS:ZCOLOR_L_AUTO
        ZCOLOR_L_AUTO = 1
		CheckBox AUTOL, value=1
		Checkbox MANL, value=0
		ModifyImage /W=$targetwindow $image_name, ctab={*, , }
	else
		NVAR ZCOLORH_AUTO = root:Packages:LaserUPS:ZCOLOR_H_AUTO
        ZCOLOR_L_AUTO = 1
		CheckBox AUTOH, value=1
		Checkbox MANH, value=0
		ModifyImage /W=$targetwindow $image_name, ctab={, *, }
	endif
END

static Function zColor_man_check(cba): CheckBoxControl
	STRUCT WMCheckboxAction &cba
	//
	ControlInfo /W=imageColorMod WAVESEL
	String image_name = S_Value
	SVAR targetwindow = root:Packages:LaserUPS:ImgTargetWin
	NVar ZCOLOR_H_VALUE = root:Packages:LaserUPS:ZCOLOR_H_VALUE
	NVAR ZCOLOR_L_VALUE = root:Packages:LaserUPS:ZCOLOR_L_VALUE
	//
	if (cmpstr(cba.ctrlName, "MANL")==0)
		NVAR ZCOLOR_L_AUTO = root:Packages:LaserUPS:ZCOLOR_L_AUTO
        ZCOLOR_L_AUTO = 0
		CheckBox AUTOL, value=0
		Checkbox MANL, value=1
		ModifyImage /W=$targetwindow $image_name, ctab={ZColor_L_VALUE, ,}
	else
		NVAR ZCOLORH_AUTO = root:Packages:LaserUPS:ZCOLOR_H_AUTO
        ZCOLOR_L_AUTO = 0
		CheckBox AUTOH, value=0
		Checkbox MANH, value=1
		ModifyImage /W=$targetwindow $image_name, ctab={, ZColor_H_VALUE, }
	endif
END

/// @param min_value
/// @param max_value
/// @param position
static Function absolute_pos(Variable min_value, Variable max_value, Variable percentage)
	return (max_value - min_value) * percentage / 100 + min_value
End

/// @param min_value
/// @param max_value
/// @param position
static Function relative_pos(Variable min_value, Variable max_value, Variable position)
	return (position - min_value) * 100 / (max_value - min_value)
End

static Function closebutton(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String win_name = ba.win
	SVar targetwindow =  root:Packages:LaserUPS:ImgTargetWin
	switch (ba.eventcode)
		case 2:
			Setwindow $targetwindow, hook=$""
			ImageColorUpdate()
			DoWIndow /K $win_name
		break
	endswitch
End
