#rtGlobals Pragma=3		// Use modern global access method.
//#pragma  version = $Id: AnalysisLUPS.ipf 843 2007-10-23 12:09:22Z arafune $
#pragma Version = 2.0beta
#pragma IgorVersion  = 6.3
#pragma TextEncoding = "UTF-8"


#include "LaserPEAnalysis"
#include "RA_common"

Menu "LaserUPS"
	"Analysis of Inelastic photoemission spectrum", DispIEPEAnalysisPanel(1)
	"Analysis of Inelastic photoemission spectrum -- Lorentzian", DispIEPEAnalysisPanel(2)
End

Function DispIEPEAnalysisPanel(ver)
	Variable ver
	Silent 1
	String msg=""
	Switch (ver)
		case 1:
			If (wintype("IEPEAnalysisPanel2")==7)
				msg = "Please close another panel for analysis "
				msg += "of inelastic photoemission spectrum before using this procedure. \n\n"
				msg += "このプロシージャーを使う前の競合する他のパネルを閉じて下さい。"
				Abort msg
			else
				DoWindow /F IEPEAnalysisPanel
			endif
			break
		case 2:
			If (wintype("IEPEAnalysisPanel")==7)
				msg = "Please close another panel for analysis "
				msg += "of inelastic photoemission spectrum before using this procedure. \n\n"
				msg += "このプロシージャーを使う前の競合する他のパネルを閉じて下さい。"
				Abort msg
			else
				DoWindow /F IEPEAnalysisPanel2
			endif
			break
		default:
			Abort "Incorrect Procedure Version..."
			break
	endswitch
	if (V_Flag != 0)
		return 0
	endif
	//
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	setDataFolder root:Packages:LaserUPS
	//
	// Set Variables
	//
	Variable NumExcitation = NumVarOrDefault(":g_NumExcitation", 0)
	Variable /G g_NumExcitation= NumExcitation
	Variable resol = NumVarOrDefault(":g_resol", 4)
	Variable /G g_resol = resol
	Variable EVac = NumVarOrDefault(":g_EVac",-5)
	Variable /G g_EVac = EVac
	Variable /G TfromW = 0
	Variable /G PosDiff = 0
	//
	Variable RPos = NumVarOrDefault(":RPosExcitation", 0)
	Variable /G RPosExcitation=RPos
	Variable IEPEPos = NumVarOrDefault(":IEPEPos0", 0)
	Variable /G IEPEPos0 = IEPEPos
	Variable IEPEW = NumVarOrDefault(":IEPEW0", 0)
	Variable /G IEPEW0 = IEPEW
	Variable IEPEInt = NumVarOrDefault(":IEPEInt0", 0)
	Variable /G IEPEInt0 = IEPEInt
	Variable IEPECoA = NumVarOrDefault(":IEPECoA0", 0)
	Variable /G IEPECoA0 = IEPECoA
	//
	String VibBr = StrVarOrDefault(":VibBroad", "Lorentzian")
	String /G VibBroad = VibBr
	//
	switch (ver)
		case 1:
			break
		case 2:   // ver.2 では IEP**1 が必要。(少なくとも0 にしておく必要。)
			IEPEPos = NumVarOrDefault(":IEPEPos1", 0)
			Variable /G IEPEPos1 = IEPEPos
			IEPEW = NumVarOrDefault(":IEPEW1", 0)
			Variable /G IEPEW1 = IEPEW
			IEPEInt = NumVarOrDefault(":IEPEInt1", 0)
			Variable /G IEPEInt1 = IEPEInt
			IEPECoA = NumVarOrDefault(":IEPECoA1", 0)
			Variable /G IEPECoA1 = IEPECoA
			// Even though ICPECoA1 is not used in "case 2", set this for consistency.
			If (NumExcitation == 0)
				NumExcitation = 1
			endif
			g_NumExcitation= NumExcitation
			break
		default:
			break
	endswitch
	//
	SetDataFolder dfSave
	Switch (ver)
		case 1:
			DoIEPEAnalysisPanel(1)
			break
		case 2:
			DoIEPEAnalysisPanel(2)
			ModifyControlList "IEPEPos1;IEPEW1;IEPEInt1" disable=0
			break
		default:
			Abort "This version is unavailable."
			break
	endswitch
End



//////////////////////////////////////////////////////////////////////////
// DoIEPEAnalysisPanel
//  --- Building panel ---
//////////////////////////////////////////////////////////////////////////
Function DoIEPEAnalysisPanel(ver) : Panel
	Variable ver
	NVAR g_NumExcitation = root:Packages:LaserUPS:g_NumExcitation
	SVar VibBroad = root:Packages:LaserUPS:VibBroad
	Variable j
	Switch(ver)
			// ------- Case 1 --------------------
		Case 1:
			Variable i
			String TabLabelString=""
			//
			PauseUpdate; Silent 1		// building window...
			//
			NewPanel /K=1 /W=(180, 121, 930, 551) /N=IEPEAnalysisPanel as "Inelastic photoemission analysis"
			ModifyPanel frameStyle =1, fixedSize=1
			//
			SetDrawEnv  /W=IEPEAnalysisPanel fsize= 14, fstyle= 0
			DrawText /W=IEPEAnalysisPanel  360, 195, "( eV )"
			SetDrawEnv  /W=IEPEAnalysisPanel fsize= 14, fstyle= 0
			DrawText /W=IEPEAnalysisPanel  360, 215, "( eV )"
			SetDrawEnv  /W=IEPEAnalysisPanel fsize= 14, fstyle= 0
			DrawText 569, 178, "( meV )"
			SetDrawEnv  /W=IEPEAnalysisPanel fsize= 14, fstyle= 0
			DrawText 564, 215, " ( K )"
			SetDrawEnv linefgc= (48896, 49152, 65280), fillfgc= (48896, 49152, 65280)
			DrawRRect 190, 265, 605, 314
			SetDrawEnv linefgc= (48896, 65280, 65280), fillfgc= (48896, 65280, 65280)
			DrawRRect 190, 316, 606, 365

			SetDrawLayer /W= IEPEAnalysisPanel  UserBack
			SetDrawEnv  /W=IEPEAnalysisPanel fsize= 18, fstyle= 1
			//
			DrawText 30, 97, "Select Wave"
			ListBox WaveSelectorList, pos={10, 110}, size={160, 270}, font="Arial", fSize=12, frame=3
			MakeListIntoWaveSelector("IEPEAnalysisPanel", "WaveSelectorList", selectionMode=WMWS_SelectionSingle, content=WMWS_Waves)
			//
			CheckBox FrmTarget, pos={40, 383}, size={81, 14}, title="From Target"
			CheckBox FrmTarget, font="Arial", fSize=12, proc = CheckProc_SingleCheck
			//
			Button AnalyzeThis, pos={196, 270}, size={160, 40}
			BUtton AnalyzeThis, proc=AnalysisIEPEButton, title="Analyze this Spectrum"
			//
			Button AppendTheCurve, pos={363, 270}, size={160, 40}
			Button AppendTheCurve, proc=AnalysisIEPEButton, title="Append the Curve"
			//
			Button StoreTheCurve, title="Store the Curve", pos={528, 270}, size={72, 40}
			Button StoreTheCurve, proc=AnalysisIEPEButton, title="Store Curve"
			//
			Button DiffMake, pos={196, 319}, size={160, 40}
			Button DiffMake, fColor=(48896, 52992, 65280)
			Button DiffMake, proc=AnalysisIEPEButton, title="Diff: Make"
			//
			Button DiffAppend, pos={366, 319}, size={160, 40}
			Button DiffAppend, fColor=(48896, 52992, 65280)
			Button DiffAppend, proc=AnalysisIEPEButton, title="Diff: Append"
			//
			Button StoreDiff,  pos={530, 319}, size={72, 41}
			Button StoreDiff,  fColor=(48896, 52992, 65280)
			Button StoreDiff,  proc=AnalysisIEPEButton, title="Diff: Store"
			//
			Button KillDepends, pos={380, 373}, size={160, 40}, title="Kill Dependencies"
			Button KillDepends, fColor=(13056, 13056, 13056)
			Button KillDepends, proc=AnalysisIEPEButton
			//
			Button Done, pos={580, 370}, size={160, 40}, proc=DonePanelButton, title="Done"
			//
			SetVariable NumExcitation, pos={188, 72}, size={230, 20}
			SetVariable NumExcitation, bodyWidth=80
			SetVariable NumExcitation, proc=changeExcitationN, title="Number of Excitations"
			SetVariable NumExcitation, font="Arial", fSize=16, format="%u", fStyle=1
			SetVariable NumExcitation, limits={0, 14, 1}
			SetVariable NumExcitation, value= root:Packages:LaserUPS:g_NumExcitation
			//
			SetVariable Resolution, pos = {188, 100}, size={230, 20}
			SetVariable Resolution, bodywidth = 80
			SetVariable Resolution, title = "Resolution ( meV )"
			SetVariable Resolution, font="Arial", fSize = 16, fStyle = 1
			SetVariable Resolution, limits = {0, inf, 0.1}
			SetVariable Resolution, value = root:Packages:LaserUPS:g_resol
			//
			SetVariable Vlevelpos, pos= {290, 130}, size={230, 20}
			SetVariable Vlevelpos, bodywidth = 80
			SetVariable Vlevelpos, title = "Vacuum level"
			SetVariable Vlevelpos, font="Arial", fSize = 16, fStyle = 1
			SetVariable Vlevelpos, limits = {-inf, inf, 0.001}, disable = 1
			SetVariable Vlevelpos, value = root:Packages:LaserUPS:g_EVac
			//
			TabControl TabIEPEAnalysis, pos={195, 130}, size={537, 115}, proc=AnalysisIEPETabProc
			TabControl TabIEPEAnalysis, font="Symbol", fSize=10, tabLabel(0)="n0"
			TabControl TabIEPEAnalysis, value= 0 // Force table 0
			For(i=0; i<=g_NumExcitation; i+=1)
				TabLabelString= "n"+num2iStr(i)
				TabControl TabIEPEAnalysis tabLabel(i)=TabLabelString
			EndFor
			//
			//		*********** Important ******************
			//		Control name must be started with "IEPE", if <only> it depends on Tab control.
			//		****************************************
			//
			SetVariable IEPEPos0, pos={240, 180}, size={115, 17}, title="Position"
			SetVariable IEPEPos0, font="Arial", fSize=14, bodyWidth= 90, limits={-inf, inf, 0.0001}
			SetVariable IEPEPos0, value= root:Packages:LaserUPS:IEPEPos0
			//
			SetVariable IEPEW0, pos={253, 200}, size={102, 20}, title="Width", fSize=14
			SetVariable IEPEW0, bodyWidth= 90, limits={0, inf, 0.0001}, font="Arial"
			SetVariable IEPEW0, value= root:Packages:LaserUPS:IEPEW0
			//
			SetVariable IEPEInt0, pos={237, 160}, size={118, 17}, title="Intensity"
			SetVariable IEPEInt0, font="Arial", fSize=14, bodyWidth= 90
			SetVariable IEPEInt0, limits={-inf, inf, 1000}
			SetVariable IEPEInt0, value= root:Packages:LaserUPS:IEPEInt0
			//
			SetVariable IEPECoA0, pos = {259, 218}, size={96, 17}, title = "Coefficient A"
			SetVariable IEPECoA0, font = "Arial", fSize= 14, bodyWidth = 70, limits={-inf, inf, 0.01}
			SetVariable IEPECoA0, value = root:Packages:LaserUPS:IEPECoA0
			//
			//		NumPPow0tation
			//
			If (g_NumExcitation >= 1)
				for (j = 1; j <= g_NumExcitation; j+=1)
					SetVariable $("IEPEPos"+num2istr(j)), pos={240, 180}, size={115, 17}, title="Position"
					SetVariable $("IEPEPos"+num2istr(j)), font="Arial", fSize=14, bodyWidth= 90
					SetVariable $("IEPEPos"+num2istr(j)), disable=1, limits ={-inf, inf, 0.0001}
					SetVariable $("IEPEPos"+num2istr(j)), value= $("root:Packages:LaserUPS:IEPEPos"+num2istr(j))
					//
					SetVariable $("IEPEW"+num2istr(j)), pos={253, 200}, size={102, 20}, title="Width", fSize=14
					SetVariable $("IEPEW"+num2istr(j)), font="Arial", bodyWidth= 90, limits={0, inf, 0.0001}
					SetVariable $("IEPEW"+num2istr(j)), disable=1
					SetVariable $("IEPEW"+num2istr(j)), value= $("root:Packages:LaserUPS:IEPEW"+num2istr(j))
					//
					SetVariable $("IEPEInt"+num2istr(j)), pos={237, 160}, size={118, 17}, title="Intensity"
					SetVariable $("IEPEInt"+num2istr(j)), font="Arial", fSize=14, bodyWidth= 90
					SetVariable $("IEPEInt"+num2istr(j)), disable=1, limits={-inf, inf, 10}
					SetVariable $("IEPEInt"+num2istr(j)), value= $("root:Packages:LaserUPS:IEPEInt"+num2istr(j))
					//
					SetVariable $("IEPECoA"+num2istr(j)), pos={259, 218}, size={96, 17}, title = "Coefficient A"
					SetVariable $("IEPECoA"+num2istr(j)), font="Arial", fSize=14, bodyWidth= 70
					SetVariable $("IEPECoA"+num2istr(j)), disable=1, limits={-inf, inf, 0.01}
					SetVariable $("IEPECoA"+num2istr(j)), value= $("root:Packages:LaserUPS:IEPECoA"+num2istr(j))
				endfor
			endif
			//
			NVar TfromW = root:Packages:LaserUPS:TfromW
			SetFormula root:Packages:LaserUPS:TfromW , "root:Packages:LaserUPS:IEPEW0/(8.61708*10^(-5)"
			ValDisplay CorrespondingT, pos={420, 200}, size={143, 16}, title="Temperature"
			ValDisplay CorrespondingT, font="Arial", fSize=14
			ValDisplay CorrespondingT, limits={0, 0, 0}, barmisc={0, 1000}, bodyWidth= 60
			ValDisplay CorrespondingT, value= #"root:Packages:LaserUPS:TfromW"
			//
			NVar PosDiff = root:Packages:LaserUPS:PosDiff
			NVar RPosExcitation = root:Packages:LaserUPS:RPosExcitation
			String posdiffFormula = ""
			posdiffFormula = "root:Packages:LaserUPS:IEPEPos"
			posdiffFormula += num2istr(RPosExcitation)
			posdiffFormula +="- root:Packages:LaserUPS:IEPEPos0"
			SetFormula PosDiff , posdiffFormula
			ValDisplay RelativePosition, pos={406, 160}, size={154, 18}, title="RelativePosition"
			ValDisplay RelativePosition, font="Arial", fSize=14
			ValDisplay RelativePosition, limits={0, 0, 0}, barmisc={0, 1000}, bodyWidth= 90
			ValDisplay RelativePosition, value= #"root:Packages:LaserUPS:PosDiff * 1000"
			//
			SetVariable RelativePositionFrom, pos={570, 180}, size={148, 20}, title="from #:"
			SetVariable RelativePositionFrom, fSize=14, limits={0, g_NumExcitation, 1}
			SetVariable RelativePositionFrom, format="%u", font="Arial"
			SetVariable RelativePositionFrom, proc=changeRelativePositionProc
			SetVariable RelativePositionFrom, bodyWidth= 60
			SetVariable RelativePositionFrom, value=root:Packages:LaserUPS:RPosExcitation
			//
			TitleBox title0, pos={189, 13}, size={293, 38}, title="Inelastic photoemission Analysis..."
			TitleBox title0, labelBack=(65535, 65533, 32768), fSize=24, fStyle=3, font="Arial"
			TitleBox title0, fColor=(65535, 16385, 16385), anchor= MT
			break
			// ---------- Case 2 -------------
		Case 2:
			PauseUpdate; Silent 1	// building window...
			NewPanel /K=1 /W=(80, 125, 744, 644)  /N=IEPEAnalysisPanel2 as "Inelastic photoemission analysis by convolution with Lorentzian"
			ModifyPanel cbRGB=(56576, 56576, 56576), frameStyle=3, frameInset=8, fixedsize=1
			SetDrawLayer /W=IEPEAnalysisPanel2 UserBack
			SetDrawEnv /W=IEPEAnalysisPanel2 linethick= 0, rounding= 20, fillpat= -1, fillbgc= (52224, 52224, 52224)
			DrawRRect 202, 133, 647, 236
			DrawRRect 157, 14, 576, 74
			SetDrawEnv fsize= 18, fstyle= 2, fname="Arial", textrgb= (65280, 0, 0)
			DrawText 182, 45, "Inelastic photoemission Analysis"
			SetDrawEnv fsize= 18, fstyle= 2, fname="Arial", textrgb= (65280, 0, 0)
			DrawText 167, 65, "(Convolution with Lorentzian/Gaussia function)"
			SetDrawEnv fsize= 14, fstyle= 1, fname="Arial"
			DrawText 605, 197, " ( K )"
			SetDrawEnv fsize= 18, fstyle= 5, fname="Arial", textrgb= (0, 15872, 65280)
			DrawText 219, 160, "Elastic component"
			SetDrawEnv fsize= 18, fstyle= 5, fname="Arial", textrgb= (0, 15872, 65280)
			DrawText 431, 349, "Inelastic components"
			SetDrawEnv fsize= 18, fstyle= 1
			DrawText 51, 128, "Select Wave"
			//
			ListBox WaveSelectorList, pos={10, 110}, size={160, 270}, font="Arial", fSize=12, frame=3
			MakeListIntoWaveSelector("IEPEAnalysisPanel2", "WaveSelectorList", selectionMode=WMWS_SelectionSingle, content=WMWS_Waves)
			//
			CheckBox FrmTarget, pos={55, 410}, size={87, 15}, proc=CheckProc_SingleCheck, title="From Target"
			CheckBox FrmTarget, font="Arial", fSize=12, fStyle=1
			//
			//
			Button Done, pos={545, 453}, size={95, 50}, proc=DonePanelButton, title="Done"
			Button Done, font="Arial", fSize=14, fStyle=1
			//
			Button KillDepends, pos={399, 453}, size={140, 50}, title="Kill \rDependencies"
			Button KillDepends, font="Arial", fSize=14, fStyle=1, fColor=(13056, 13056, 13056)
			Button KillDepends, proc=AnalysisIEPEButton
			//
			Button AnalyzeThis, pos={212, 360}, size={120, 40}, title="Analyze\r this spectrum"
			Button AnalyzeThis, font="Arial", fSize=14, fStyle=1
			Button AnalyzeThis, proc=AnalysisIEPEButton
			//
			Button AppendTheCurve, pos={348, 360}, size={120, 40}, title="Append Curve", font="Arial"
			Button AppendTheCurve, fSize=14, fStyle=1
			Button AppendTheCurve, proc=AnalysisIEPEButton
			//
			Button StoreTheCurve, pos={484, 360}, size={120, 40}, title="Store Curve", font="Arial"
			Button StoreTheCurve, fSize=14, fStyle=1
			Button StoreTheCurve, proc=AnalysisIEPEButton
			//
			Button DiffMake, pos={212, 404}, size={120, 40}, title="Diff\r Make", font="Arial"
			Button DiffMake, fSize=14, fStyle=1, fColor=(48896, 52992, 65280)
			Button DiffMake, proc=AnalysisIEPEButton
			//
			Button DiffAppend, pos={348, 404}, size={120, 40}, title="Diff\r Append", font="Arial"
			Button DiffAppend, fSize=14, fStyle=1, fColor=(48896, 52992, 65280)
			Button DiffAppend, proc=AnalysisIEPEButton
			//
			Button StoreDiff, pos={484, 404}, size={120, 40}, title="Diff\r Store", font="Arial"
			Button StoreDiff, fSize=14, fStyle=1, fColor=(48896, 52992, 65280)
			Button StoreDiff, proc=AnalysisIEPEButton
			//
			SetVariable NumExcitation, pos={232, 82}, size={253, 22}, proc=changeExcitationN, title="Number of Excitations"
			SetVariable NumExcitation, font="Arial", fSize=16, format="%u", fStyle=1
			SetVariable NumExcitation, limits={1, 14, 1}, value= root:Packages:LaserUPS:g_NumExcitation, bodyWidth= 80
			//
			SetVariable Resolution, pos={262, 110}, size={223, 22}, title="Resolution ( meV )"
			SetVariable Resolution, font="Arial", fSize=16, fStyle=1
			SetVariable Resolution, limits={0, inf, 0.1}, value= root:Packages:LaserUPS:g_resol, bodyWidth= 80
			//
			SetVariable IEPEInt0, pos={224, 162}, size={154, 20}, title="Intensity", font="Arial"
			SetVariable IEPEInt0, fSize=14, fStyle=1
			SetVariable IEPEInt0, limits={-inf, inf, 1000}, value= root:Packages:LaserUPS:IEPEInt0, bodyWidth= 90
			SetVariable IEPEPos0, pos={227, 214}, size={151, 20}, title="Position", font="Arial"
			SetVariable IEPEPos0, fSize=14, fStyle=1
			SetVariable IEPEPos0, limits={-inf, inf, 0.0001}, value= root:Packages:LaserUPS:IEPEPos0, bodyWidth= 90
			SetVariable IEPEW0, pos={429, 156}, size={134, 20}, title="Width", font="Arial"
			SetVariable IEPEW0, fSize=14, fStyle=1
			SetVariable IEPEW0, limits={0, inf, 0.0001}, value= root:Packages:LaserUPS:IEPEW0, bodyWidth= 90
			SetVariable IEPECoA0, pos={399, 204}, size={164, 20}, title="Coefficient A"
			SetVariable IEPECoA0, font="Arial", fSize=14, fStyle=1
			SetVariable IEPECoA0, limits={-inf, inf, 0.01}, value= root:Packages:LaserUPS:IEPECoA0, bodyWidth= 70
			//
			PopupMenu VibBroad title="Vibrational broadening:",pos={280,470},bodyWidth=120
			PopupMenu VibBroad value="Lorentzian;Gaussian",font="Arial",fSize=14,fstyle=1
			PopupMenu VibBroad proc=VibBroadFunctionSelectrion
			Strswitch (VibBroad)
				case "Lorentzian":
					PopupMenu VibBroad mode=1
					break
				case "Gaussian":
					PopupMenu VibBroad mode=2
					break
			endswitch
			//
			ValDisplay CorrespondingT, pos={450, 182}, size={154, 18}, title="Temperature"
			ValDisplay CorrespondingT, font="Arial", fSize=14, fStyle=1
			ValDisplay CorrespondingT, limits={0, 0, 0}, barmisc={0, 1000}, bodyWidth= 60
			ValDisplay CorrespondingT, value= #"root:Packages:LaserUPS:TfromW"
			//
			String Tempformula
			TempFormula = "root:Packages:LaserUPS:IEPEW0/(8.61708*10^(-5)"
			SetFormula root:Packages:LaserUPS:TfromW, TempFormula
			//
			TabControl TabIEPEAnalysis, pos={206, 255}, size={442, 101}, font="Symbol", fSize=14, fStyle=1
			TabControl TabIEPEAnalysis, TabLabel(0)="n1", value= 0, proc=AnalysisIEPETabProc
			If (g_NumExcitation >= 1)
				for (j = 1;  j<=g_NumExcitation; j+=1)
					SetVariable $("IEPEInt"+num2istr(j)), pos={273, 286}, size={154, 20}, title="Intensity", font="Arial"
					SetVariable $("IEPEInt"+num2istr(j)), fSize=14, fStyle=1
					SetVariable $("IEPEInt"+num2istr(j)), limits={-inf, inf, 0.001}, bodyWidth= 90
					SetVariable $("IEPEInt"+num2istr(j)), value= $("root:Packages:LaserUPS:IEPEInt"+num2istr(j))
					//
					SetVariable $("IEPEPos"+num2istr(j)), pos={219, 308}, size={209, 20}, title="Vib. Energy  (eV)"
					SetVariable $("IEPEPos"+num2istr(j)), font="Arial", fSize=14, fStyle=1
					SetVariable $("IEPEPos"+num2istr(j)), limits={-inf, inf, 0.0001}, bodyWidth= 90
					SetVariable $("IEPEPos"+num2istr(j)), value= $("root:Packages:LaserUPS:IEPEPos"+num2istr(j))
					//
					SetVariable $("IEPEW"+num2istr(j)), pos={456, 287}, size={166, 20}, title="Width (eV)", font="Arial"
					SetVariable $("IEPEW"+num2istr(j)), fSize=14, fStyle=1
					SetVariable $("IEPEW"+num2istr(j)), limits={0, inf, 0.0001}, bodyWidth= 90
					SetVariable $("IEPEW"+num2istr(j)), value= $("root:Packages:LaserUPS:IEPEW"+num2istr(j))
				endfor
			endif
			//
			SetVariable IEPEInt1, disable = 0
			SetVariable IEPEW1, disable = 0
			SetVariable IEPEPos1, disable = 0

			//
			For(i=1; i<=g_NumExcitation; i+=1)
				TabLabelString= "n"+num2iStr(i)
				TabControl TabIEPEAnalysis tabLabel(i-1)=TabLabelString
			EndFor
			DoUpdate
			//
			break
		default:
			break
	endswitch
	return 1
End


//////////////////////////////////////////////////////////////////////////
//  Button
//////////////////////////////////////////////////////////////////////////
Function AnalysisIEPEButton(B_Struct)
	STRUCT WMButtonAction &B_Struct
	//
	// Declare the common variable used in this procedure...
	//
	String waveNamebase
	String curWinName
	Variable i
	Variable isExistOnCurrentWindow =0
	String DiffCurveFullPath = "root:Packages:LaserUPS:Diff_mFD"
	String fittingparameters =""
	String WStr, PSTr, AStr, IStr
	NVar/Z NumExcitation = root:Packages:LaserUPS:g_NumExcitation
	SVar/Z VibBroad = root:Packages:LaserUPS:VibBroad
	//
	switch(B_Struct.eventCode)
		case 2: // mouse up
			strswitch (B_Struct.ctrlName)
				case "AnalyzeThis":
					waveNameBase = WS_SelectedObjectsList(B_Struct.win, "WaveSelectorList")
					waveNamebase = StringFromList(0, waveNameBase)
					//	Analyze experimental wave.
					Variable delx
					delx = deltax($waveNameBase)
					Variable npnts
					npnts = numpnts($waveNameBase)
					Variable left_x
					left_x = leftx($waveNameBase)
					//	gauss curve
					String gaussWaveforFitFullPath
					gaussWaveforFitFullPath = "root:Packages:LaserUPS:gausscurve"
					Make /O/N=600   $gaussWaveforFitFullPath
					Wave gausswave = $gaussWaveforFitFullPath
					SetScale/P x -delx * 300 , delx , "", gausswave
					SetFormula root:Packages:LaserUPS:gausscurve, "instrument(x, g_resol)"
					//
					NVar g_NumExcitation = root:Packages:LaserUPS:g_NumExcitation
					StrSwitch(B_Struct.win)
						case "IEPEAnalysisPanel":
							Make_mFD(B_Struct.ctrlName, g_NumExcitation, "IEPEW", "IEPEPos", "IEPECoA", "IEPEInt", npnts, delx, left_x)
							break
						case "IEPEAnalysisPanel2":
							Make_mFD2(B_Struct.ctrlName, g_NumExcitation, "IEPEW", "IEPEPos", "IEPECoA", "IEPEInt", npnts, delx, left_x)
							break
						default:
							break
					endswitch
					break
				case "AppendTheCurve":
					if (exists("root:Packages:LaserUPS:conv_mFD") != 1)
						Abort "Use \"Analyze this spectrum\" Button before this button"
					endif
					curWinName = WinName (0, 1)
					GetWindow $curWinName, wavelist
					Wave /T W_WaveList = W_WaveList
					isExistOnCurrentWindow = 0
					for  (i=0; i<DimSize(W_WaveList, 0); i+=1)
						if (stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:conv_mFD")==1)
							isExistOnCurrentWindow = 1
						endif
					endfor
					if (isExistOnCurrentWindow !=1)
						AppendToGraph root:Packages:LaserUPS:conv_mFD
					endif
					KillWaves W_WaveList
					break
				case "StoreTheCurve":
					waveNameBase = WS_SelectedObjectsList(B_Struct.win, "WaveSelectorList")
					waveNamebase = StringFromList(0, waveNameBase)
					//
					if (WaveExists(root:Packages:LaserUPS:conv_mFD))
						String StoreWStr
						Strswitch (B_Struct.win)
							case "IEPEAnalysisPanel":
								StoreWStr = Addprefix2wavename(waveNameBase, "mFD_")
								break
							case "IEPEAnalysisPanel2":
								Strswitch (VibBroad)
									case "Lorentzian":
										StoreWStr = Addprefix2wavename(waveNameBase, "lcFit_")
										break
									case "Gaussian":
										StoreWStr = Addprefix2wavename(waveNameBase, "gcFit_")
										break
								endswitch
								break
						endswitch
						Duplicate /O root:Packages:LaserUPS:conv_mFD, $StoreWStr
						SetFormula $StoreWStr, ""
						//
						fittingparameters =""
						NVar g_NumExcitation = root:Packages:LaserUPS:g_NumExcitation
						NVar res = root:Packages:LaserUPS:g_resol
						Strswitch (B_Struct.win)
							case "IEPEAnalysisPanel":
								fittingparameters = "VibrationalBroadening:AsTemperature;"+num2char(13)
								break
							case "IEPEAnalysisPanel2":
								Strswitch (VibBroad)
									case "Lorentzian":
										fittingparameters= "VibrationalBroadening:Lorentzian;"+num2char(13)
										break
									case "Gaussian":
										fittingparameters= "VibrationalBroadening:Gaussian;"+num2char(13)
										break
								endswitch
								break
						endswitch
						fittingparameters += ";resolution:"+num2str(res)+";"+num2char(13)
						fittingparameters += ";NumExcitations:"+num2str(g_NumExcitation)+";"+num2char(13)
						for (i=0; i<=g_NumExcitation; i+=1)
							NVar Int = $("root:Packages:LaserUPS:IEPEInt"+num2istr(i))
							fittingparameters += ";IEPEInt"+num2istr(i)+":"+num2str(Int)
							NVar P = $("root:Packages:LaserUPS:IEPEPos"+num2istr(i))
							fittingparameters += ";IEPEPos"+num2istr(i)+":"+num2str(P)
							NVar W = $("root:Packages:LaserUPS:IEPEW"+num2istr(i))
							fittingparameters += ";IEPEW"+num2istr(i)+":"+num2str(W)+";"+num2char(13)
						endfor
						Strswitch (B_Struct.win)
							case "IEPEAnalysisPanel":
								for (i=0; i<=g_NumExcitation; i+=1)
									NVar/Z A = $("root:Packages:LaserUPS:IEPECoA"+num2istr(i))
									fittingparameters += ";IEPECoA"+num2istr(i)+":"+num2str(A)+";"+num2char(13)
								endfor
								break
							case "IEPEAnalysisPanel2":
								NVar/Z A = $("root:Packages:LaserUPS:IEPECoA"+num2istr(0))
								fittingparameters += ";IEPECoA"+num2istr(0)+":"+num2str(A)+";"+num2char(13)
								break
						endswitch
						note $StoreWStr, fittingparameters
					else
						String stmp = "Use \"Analyze this specrum\", before use this button."
						Abort stmp
					endif
					break
				case "DiffMake":
					if (exists("root:Packages:LaserUPS:conv_mFD") != 1)
						Abort "Use \"Analyze this spectrum\" Button before using this function"
					endif
					waveNameBase = WS_SelectedObjectsList(B_Struct.win, "WaveSelectorList")
					waveNamebase = StringFromList(0, waveNameBase)
					Duplicate /O $waveNamebase $DiffCurveFullPath
					String Diff_mFDFormula
					Diff_mFDFormula = GetWavesDataFolder($waveNameBase, 2) + "- Conv_mFD"
					SetFormula $DiffCurveFullPath, Diff_mFDFormula
					break
				case "DiffAppend":
					if (exists("root:Packages:LaserUPS:Diff_mFD") != 1)
						Abort "Use \"Diff Make\" Button before using this function"
					endif
					curWinName = WinName (0, 1)
					GetWindow $curWinName, wavelist
					Wave /T W_WaveList =W_WaveList
					for (i=0; i<DimSize(W_WaveList, 0); i+=1)
						if(stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:Diff_mFD")==1)
							isExistOncurrentWindow =1
						endif
					endfor
					if (isExistOnCurrentWindow !=1)
						AppendToGraph /L=Diff $DiffCurveFullPath
						ModifyGraph tick(Diff)=1, mirror(Diff)=2, axThick=2, freepos(Diff)=0, lblPos(Diff)=50
						Label Diff "Diff (\\u)"
					endif
					Killwaves W_WaveList
					break
				case "StoreDiff":
					waveNameBase = WS_SelectedObjectsList(B_Struct.win, "WaveSelectorList")
					waveNamebase = StringFromList(0, waveNameBase)
					if(WaveExists(root:Packages:LaserUPS:Diff_mFD))
						Strswitch (B_Struct.win)
							case "IEPEAnalysisPanel":
								StoreWStr = Addprefix2wavename(waveNameBase, "DiffmFD_")
								break
							case "IEPEAnalysisPanel2":
								Strswitch (VibBroad)
									case "Lorentzian":
										StoreWStr = Addprefix2wavename(waveNameBase, "Difflc_")
										break
									case "Gaussian":
										StoreWStr = Addprefix2wavename(waveNameBase, "Diffgc_")
										break
								endswitch
								break
						endswitch
						Duplicate /O root:Packages:LaserUPS:Diff_mFD, $StoreWStr
						SetFormula $StoreWStr, ""
						//
						fittingparameters =""
						NVar g_NumExcitation = root:Packages:LaserUPS:g_NumExcitation
						NVar res = root:Packages:LaserUPS:g_resol
						Strswitch (B_Struct.win)
							case "IEPEAnalysisPanel":
								fittingparameters = "VibrationalBroadening:AsTemperature;"+num2char(13)
								break
							case "IEPEAnalysisPanel2":
								Strswitch (VibBroad)
									case "Lorentzian":
										fittingparameters= "VibrationalBroadening:Lorentzian;"+num2char(13)
										break
									case "Gaussian":
										fittingparameters= "VibrationalBroadening:Gaussian;"+num2char(13)
										break
								endswitch
								break
						endswitch
						fittingparameters += ";resolution:"+num2str(res)+";"+num2char(13)
						fittingparameters += ";NumExcitations:"+num2str(g_NumExcitation)+";"+num2char(13)
						for (i=0; i<=g_NumExcitation; i+=1)
							NVar Int = $("root:Packages:LaserUPS:IEPEInt"+num2istr(i))
							fittingparameters += ";IEPEInt"+num2istr(i)+":"+num2str(Int)
							NVar P = $("root:Packages:LaserUPS:IEPEPos"+num2istr(i))
							fittingparameters += ";IEPEPos"+num2istr(i)+":"+num2str(P)
							NVar W = $("root:Packages:LaserUPS:IEPEW"+num2istr(i))
							fittingparameters += ";IEPEW"+num2istr(i)+":"+num2str(W)+";"+num2char(13)
						endfor
						Strswitch (B_Struct.win)
							case "IEPEAnalysisPanel":
								for (i=0; i<=g_NumExcitation; i+=1)
									NVar/Z A = $("root:Packages:LaserUPS:IEPECoA"+num2istr(i))
									fittingparameters += ";IEPECoA"+num2istr(i)+":"+num2str(A)+";"+num2char(13)
								endfor
								break
							case "IEPEAnalysisPanel2":
								NVar/Z A = $("root:Packages:LaserUPS:IEPECoA"+num2istr(0))
								fittingparameters += ";IEPECoA"+num2istr(0)+":"+num2str(A)+";"+num2char(13)
								break
						endswitch
						note $StoreWStr, fittingparameters
					endif
					break
				case "KillDepends":
					String sFDFullPath = "root:Packages:LaserUPS:sFD"
					String vibFullPath = "root:Packages:LaserUPS:vib"
					RemoveFromGraph /Z Diff_mFD
					KillWaves/Z $DiffCurveFullPath
					RemoveFromGraph /Z conv_mFD
					Killwaves/Z root:Packages:LaserUPS:conv_mFD
					killwaves/Z root:Packages:LaserUPS:mFD
					strswitch (B_Struct.win)
						case "IEPEAnalysisPanel":
							break
						case "IEPEAnalysisPanel2":
							if (NVAR_EXISTS(NumExcitation))
								for (i=1; i<=NumExcitation; i+=1)
									sFDFullPath ="root:Packages:LaserUPS:sFD"+num2istr(i)
									vibFullPath = "root:Packages:LaserUPS:vib"+num2istr(i)
									KillWaves /Z $sFDFullPath
									KillWaves /Z $vibFullPath
								endfor
								KillWaves /Z root:Packages:LaserUPS:sFD0
							endif
							KillWaves /Z root:Packages:LaserUPS:gausscurve
							break
						default:
							break
					endswitch
					break
				default:
					Print B_Struct.ctrlName, ": This is not imprelented yet."
			endswitch
			// click code here
			break
	endswitch
End
////////////////////////////////////////////////////////////////////////
// Make_mFD2
////////////////////////////////////////////////////////////////////////
Function Make_mFD2(ctrlName, N, WStr, PStr, AStr, IStr, npts, delx, left_x)
	String ctrlName
	Variable N
	String WStr
	String PStr
	String ASTr
	String IStr
	Variable npts
	Variable delx
	Variable left_x
	//
	Variable VibWaveSize = 600
	SVar VibBroad = root:Packages:LaserUPS:VibBroad
	//
	Variable i =0
	String sFDformula = ""
	String sFDFullPath = ""
	String mFDformula = "sFD0"
	String mFDFullPath = "root:Packages:LaserUPS:mFD"
	//
	String SFD0formula =""
	sFD0formula = "+("+Astr+num2istr(0)+"*x+1-"+Astr+num2istr(0)+"*"
	sFD0formula += PStr+num2istr(0)+")*"+IStr+num2istr(0)
	sFD0formula += "/(exp((x-"+PStr+num2istr(0)+")/"+WStr+num2istr(0)+")+1)"
	String sFD0FullPath = "root:Packages:LaserUPS:sFD0"
	//
	String VibFormula =""
	String VibFullPath =""
	//
	String Conv_mFDFullPath = "root:Packages:LaserUPS:conv_mFD"
	//
	strswitch (ctrlName)
		case "AnalyzeThis":
			Make /O/N=(npts) $sFD0FullPath
			Wave sFD0 = $sFD0FullPath
			SetScale /P x, left_x, delx, "eV", sFD0
			SetFormula $sFD0FullPath, sFD0formula
			For (i=1; i<=N; i+=1)
				// Make "Vib" waves
				Strswitch (VibBroad)
					case "Lorentzian":
						VibFormula = "+ Lorentzian(x, "
						break
					case "Gaussian":
						VibFormula = "+ gaussian(x, "
						break
				endswitch
				VibFormula += PStr+num2istr(i)
				VibFormula += ","+IStr+num2istr(i)
				VibFormula += ","+Wstr+num2istr(i)+")"
				VibFullPath = "root:Packages:LaserUPS:vib"+num2istr(i)
				Make /O/N=(VibWaveSize) $VibFullPath
				Wave Vib = $VibFullPath
				SetScale /P x,  -(VibWaveSize)*delx/2, delx, "eV", Vib
				SetFormula $VibFullPath, VibFormula
				// Make "sFDi" waves (i=1,2,3,...)
				sFDformula = "conv3(p, vib"+num2istr(i)+", sFD0)"
				sFDFullPath = "root:Packages:LaserUPS:sFD"+num2istr(i)
				Make /O/N=(npts) $sFDFullPath
				Wave sFD = $sFDFullPath
				SetScale /P x, left_x, delx, "eV", sFD
				SetFormula $sFDFullPath, sFDformula
				mFDformula += "+sFD"+num2istr(i)
			EndFor
			// mFD
			Make /O/N=(npts) $mFDFullPath
			Wave mFD = $mFDFullPath
			SetScale /P x, left_x, delx, "eV", mFD
			SetFormula mFD, mFDformula
			// conv_mFD
			Make /O/N=(npts) $Conv_mFDFullPath
			Wave conv_mFD = $Conv_mFDFullPath
			SetScale /P x, left_x, delx, "eV", conv_mFD
			SetFormula $Conv_mFDFullPath, "conv3(p, gausscurve, mFD)"
			break
		case "NumExcitation":
			If(WaveExists(root:Packages:LaserUPS:conv_mFD))
				left_x = leftx(root:Packages:LaserUPS:conv_mFD)
				delx = deltax(root:Packages:LaserUPS:conv_mFD)
				npts = numpnts(root:Packages:LaserUPS:conv_mFD)
			else
				Abort "Select wave, and press \"Analyze This\" button."
			endif
			for (i=1; i<=N; i+=1)
				VibFullPath = "root:Packages:LaserUPS:vib"+num2istr(i)
				// Vib wave
				If(WaveExists($VibFullPath))
				else
					Strswitch (VibBroad)
						case "Lorentzian":
							VibFormula = "+ lorentzian(x, "
							break
						case "Gaussian":
							VibFormula = "+ gaussian(x, "
							break
					endswitch
					VibFormula += PStr+num2istr(i)
					VibFormula += ","+IStr+num2istr(i)
					VibFormula += ","+Wstr+num2istr(i)+")"
					VibFullPath = "root:Packages:LaserUPS:vib"+num2istr(i)
					Make /O/N=(VibWaveSize) $VibFullPath
					Wave Vib = $VibFullPath
					SetScale /P x,  -(VibWaveSize)*delx/2, delx, "eV", Vib
					SetFormula $VibFullPath, VibFormula
					DoUpdate
				endif
				// sFD wave
				sFDFullPath = "root:Packages:LaserUPS:sFD"+num2istr(i)
				If(WaveExists($sFDFullPath))
				else
					sFDformula = "conv3(p, vib"+num2istr(i)+", sFD0)"
					sFDFullPath = "root:Packages:LaserUPS:sFD"+num2istr(i)
					Make /O/N=(npts) $sFDFullPath
					Wave sFD = $sFDFullPath
					SetScale /P x, left_x, delx, "eV", sFD
					SetFormula $sFDFullPath, sFDformula
				endif
			endfor
			mFDformula =""
			for (i=0; i<=N; i+=1)
				mFDformula += "+sFD"+num2istr(i)
			endfor
			Wave mFD = $mFDFullPath
			SetFormula mFD, mFDformula
			break
		default:
	endswitch
End


////////////////////////////////////////////////////////////////////////
// Make_mFD
////////////////////////////////////////////////////////////////////////

Function Make_mFD(ctrlName, N, WStr, PStr, AStr, IStr, npts, delx, left_x)
	String ctrlName
	Variable N
	String WStr
	String PStr
	String ASTr
	String IStr
	Variable npts
	Variable delx
	Variable left_x
	//
	Variable i =0
	String sFDformula = ""
	String sFDFullPath = ""
	String mFDformula = ""
	String mFDFullPath = "root:Packages:LaserUPS:mFD"
	//
	strswitch (ctrlName)
		case "AnalyzeThis":
			For(i=0; i<=N; i+=1)
				sFDformula  = "+("+Astr+num2istr(i)+"*x+1-"+Astr+num2istr(i)+"*"
				sFDformula += PStr+num2istr(i)+")*"+IStr+num2istr(i)
				sFDformula += "/(exp((x-"+PStr+num2istr(i)+")/"+WStr+num2istr(i)+")+1)"
				sFDFullPath = "root:Packages:LaserUPS:sFD"+num2istr(i)
				Make /O/N=(npts) $sFDFullPath
				Wave sFD = $sFDFullPath
				SetScale /P x left_x, delx, "eV", sFD
				SetFormula $sFDFullPath, sFDformula
				mFDformula += "+sFD"+num2istr(i)
			EndFor
			Make /O/N=(npts) $mFDFullPath
			Wave mFD = $mFDFullPath
			SetScale /P x  left_x, delx, "eV", mFD
			SetFormula $mFDFullPath, mFDformula
			//
			String Conv_mFDFullPath = "root:Packages:LaserUPS:conv_mFD"
			Make /O/N=(npts) $Conv_mFDFullPath
			Wave conv_mFD = $Conv_mFDFullPath
			SetScale /P x left_x, delx, "eV", conv_mFD
			SetFormula $Conv_mFDFullPath, "conv2(p, gausscurve, mFD)"
			break
		case "NumExcitation":
			Wave mFD = $mFDFullPath
			if (WaveExists(mFD))
				delx = deltax(mFD)
				left_x = leftx(mFD)
				npts = numpnts(mFD)
				For (i=0; i<=N; i+=1)
					sFDFullPath = "root:Packages:LaserUPS:sFD"+num2istr(i)
					sFDformula = "+("+Astr+num2istr(i)+"*x+1-"+Astr+num2istr(i)
					sFDformula += "*"+PStr+num2istr(i)+")*"+IStr+num2istr(i)
					sFDformula += "/(exp((x-"+PStr+num2istr(i)+")/"
					sFDformula += WStr+num2istr(i)+")+1)"
					Wave sFD = $sFDFullPath
					if (WaveExists(sFD	))
					else
						//						Print "Not exists, so we make a corresponding wave"
						Make /O/N=(npts) $sFDFullPath
						SetScale /P x left_x, delx, "eV", $sFDFullPath
						SetFormula $sFDFullPath, sFDFormula
					endif
					mFDFormula += "+sFD"+num2istr(i)
				endfor
				SetFormula $mFDFullPath, mFDFormula
			else
				break
			endif
			break

		default:
			Print ctrlName
	endswitch
End

////////////////////////////////////////////////////////////////////
//  Tab Control
////////////////////////////////////////////////////////////////////
Function AnalysisIEPETabProc(TC) : TabControl
	STRUCT WMTabControlAction &TC
	//
	NVar /Z RPosExcitation = root:Packages:LaserUPS:RPosExcitation
	//
	String posdiffFormula = ""
	String TempFormula
	String controlsNdependVal
	String ControlsInCur
	String ControlsInOther
	strswitch (TC.win)
		case "IEPEAnalysisPanel":
			if (TC.eventCode == 2)
				controlsNdependVal = ControlNameList("", ";", "IEPE*")
				ControlsInCur = ListMatch(controlsNdependVal, "IEPE*"+num2istr(TC.tab))
				ControlsInOther =  ListMatch(controlsNdependVal, "!"+"IEPE*"+num2istr(TC.tab))
				ModifyControlList ControlsInOther disable =1	 //hide
				ModifyControlList ControlsInCur disable =0   //show
				TempFormula = "root:Packages:LaserUPS:IEPEW"+num2istr(TC.tab)+"/(8.61708*10^(-5)"
				SetFormula root:Packages:LaserUPS:TfromW, TempFormula
				//
				posdiffFormula = "root:Packages:LaserUPS:IEPEPos"
				posdiffFormula += num2istr(TC.tab)
				posdiffFormula += "- root:Packages:LaserUPS:IEPEPos"
				posdiffFormula += num2istr(RPosExcitation)
				SetFormula root:Packages:LaserUPS:PosDiff, posdiffFormula
			endif
			break
		case "IEPEAnalysisPanel2":
			if (TC.eventCode ==2)
				controlsNdependVal = ControlNameList("", ";", "IEPE*")
				ControlsInOther =  ListMatch(controlsNdependVal, "!"+"IEPE*"+num2istr(TC.tab+1))
				ControlsInCur = ListMatch(controlsNdependVal, "IEPE*"+num2istr(TC.tab+1))
				ControlsInCur += ListMatch(controlsNdependVal, "IEPE*0")
				ModifyControlList ControlsInOther disable =1	 //hide
				ModifyControlList ControlsInCur disable =0   //show
				//
			endif
			break
		default:
			break
	endswitch
	return 0
End


//////////////////////////////////////////////////////////////////////////
// changeRelativePositionProc
//////////////////////////////////////////////////////////////////////////
Function changeRelativePositionProc(SV) : SetVariableControl
	STRUCT WMSetVariableAction &SV
	//
	ControlInfo TabIEPEAnalysis
	Variable tabNum = V_Value
	NVar RPosExcitation = root:Packages:LaserUPS:RPosExcitation
	String posdiffFormula = ""
	posdiffFormula = "root:Packages:LaserUPS:IEPEPos"
	posdiffFormula += num2istr(tabNum)
	posdiffFormula += "- root:Packages:LaserUPS:IEPEPos"
	posdiffFormula += num2istr(RPosExcitation)
	SetFormula root:Packages:LaserUPS:PosDiff, posdiffFormula
	return 0
End

//////////////////////////////////////////////////////////////////////////
// changeExcitationN
//////////////////////////////////////////////////////////////////////////
Function changeExcitationN(SV) : SetVariableControl
	//
	//  When "Number of Excitations" changes, following occur:
	//       1) The tab number changes.
	//       2) Make a wave if needed (i.e. Number of excitations increases.)
	//
	STRUCT WMSetVariableAction &SV
	//
	If (SV.eventcode< 1 )
		return 0
	endif
	switch (SV.eventCode)
		case 1: // mouse up
		case 2: // Enter key
		case 3: // Live update
			Variable dval = SV.dval
			String sval = SV.sval
			Variable i =0
			Variable j =1
			Variable currentTabNumber
			String tmpTabName1
			String tmpTabname2
			ControlInfo TabIEPEAnalysis
			currentTabNumber = V_Value  //V_value means the "current tab number".
			String  TablabelString
			//
			//	Increase the number of Tabs
			//
			String IEPPosName="IEPEPos"
			String IEPWName ="IEPEW"
			String IEPIName="IEPEInt"
			String IEPCoAName="IEPECoA"
			String IEPPosPath=""
			String IEPWPath =""
			String IEPIPath=""
			String IEPCoAPath=""
			//
			strswitch (SV.win)
				case "IEPEAnalysisPanel":
					For(i=0; i<=SV.dval; i+=1)
						TabLabelString= "n"+num2iStr(i)
						TabControl TabIEPEAnalysis tabLabel(i)=TabLabelString
					EndFor
					break
				case "IEPEAnalysisPanel2":
					For(i=0; i<SV.dval; i+=1)
						TabLabelString= "n"+num2iStr(i+1)
						TabControl TabIEPEAnalysis tabLabel(i)=TabLabelString
					EndFor
					break
				default:
					break
			endswitch
			IEPPosPath="root:Packages:LaserUPS:"+IEPPosName+num2str(SV.dval)
			IEPWPath="root:Packages:LaserUPS:"+IEPWName+num2str(SV.dval)
			IEPIPath="root:Packages:LaserUPS:"+IEPIName+num2str(SV.dval)
			IEPCoAPath="root:Packages:LaserUPS:"+IEPCoAName+num2str(SV.dval)
			NVar/Z  Pos = $IEPPosPath
			if (NVar_Exists(Pos))
			else
				Variable /G $IEPPosPath
				Variable /G $IEPWPath
				Variable /G $IEPIPath
				Variable /G $IEPCoAPath
			endif
			String ValCtrl = ControlNameList("", ";", "*Int*")
			if (stringmatch(IEPPosName+num2str(SV.dval), valCtrl)==0)
				strswitch (SV.win)
					case "IEPEAnalysisPanel":
						SetVariable $(IEPCoAName+num2str(SV.dval)), pos={259, 218}, size={96, 17}, title="Coefficient A"
						SetVariable $(IEPCoAName+num2str(SV.dval)), font="Arial", fSize=14
						SetVariable $(IEPCoAName+num2str(SV.dval)), bodyWidth= 70, limits={-inf, inf, 0.01}
						SetVariable $(IEPCoAName+num2str(SV.dval)), value= $IEPCoAPath
						SetVariable $(IEPPosName+num2str(SV.dval)), title="Position"
						SetVariable $(IEPWName+num2str(SV.dval)), title="Width", fSize=14
						If(SV.dval!=0)
							SetVariable $(IEPCoAName+num2str(SV.dval)), disable=1
						EndIf
						SetVariable RelativePositionFrom, fSize=14, limits={0, SV.dval, 1}
						SetVariable $(IEPIName+num2str(SV.dval)), pos={237, 160}, size={118, 17}
						SetVariable $(IEPPosName+num2str(SV.dval)), pos={240, 180}, size={115, 17}
						SetVariable $(IEPWName+num2str(SV.dval)), pos={253, 200}, size={102, 20}
						break
					case "IEPEAnalysisPanel2":
						SetVariable $(IEPIName+num2str(SV.dval)), pos={273, 286}, size={154, 20}, fStyle=1
						SetVariable $(IEPPosName+num2str(SV.dval)), pos={219, 308}, size={209, 20}, fStyle=1
						SetVariable $(IEPWName+num2str(SV.dval)), pos={456, 287}, size={166, 20}, fStyle=1
						SetVariable $(IEPPosName+num2str(SV.dval)), title="Vib. Energy  (eV)"
						SetVariable $(IEPWName+num2str(SV.dval)), title="Width (eV)", fSize=14
						break
				endswitch
				SetVariable $(IEPPosName+num2str(SV.dval)), font="Arial", fSize=14, bodyWidth=90
				SetVariable $(IEPPosName+num2str(SV.dval)), limits ={-inf, inf, 0.0001}
				SetVariable $(IEPPosName+num2str(SV.dval)), value= $IEPPosPath
				//
				SetVariable $(IEPWName+num2str(SV.dval)), font="Arial"
				SetVariable $(IEPWName+num2str(SV.dval)), bodyWidth= 90, limits={0, inf, 0.0001}
				SetVariable $(IEPWName+num2str(SV.dval)), value= $IEPWPath
				//
				SetVariable $(IEPIName+num2str(SV.dval)), title="Intensity"
				SetVariable $(IEPIName+num2str(SV.dval)), font="Arial", fSize=14, bodyWidth=90
				SetVariable $(IEPIName+num2str(SV.dval)), limits={-inf, inf, 0.001}
				SetVariable $(IEPIName+num2str(SV.dval)), value= $IEPIPath
				If(SV.dval!=0)
					SetVariable $(IEPIName+num2str(SV.dval)), disable=1
					SetVariable $(IEPPosName+num2str(SV.dval)), disable=1
					SetVariable $(IEPWName+num2str(SV.dval)), disable=1
				Endif
			Endif
			//
			// Reduce Tab
			//
			strswitch (SV.win)
				case "IEPEAnalysisPanel":
					do
						TabControl TabIEPEAnalysis value =SV.dval+j
						ControlInfo TabIEPEAnalysis
						If (strLen(S_Value)>0)  // S_value means "Tab text"
							TabControl TabIEPEAnalysis, tabLabel(SV.dval+j)=""
						endif
						j+=1
						TabControl TabIEPEAnalysis value =SV.dval+j
						ControlInfo TabIEPEAnalysis
						tmpTabName1 = S_Value
					while (strLen(tmpTabName1)>0 )
					break
				case "IEPEAnalysisPanel2":
					do
						TabControl TabIEPEAnalysis value =SV.dval+j-1
						ControlInfo TabIEPEAnalysis
						If (strLen(S_Value)>0)  // S_value means "Tab text"
							TabControl TabIEPEAnalysis, tabLabel(SV.dval+j-1)=""
						endif
						j+=1
						TabControl TabIEPEAnalysis value =SV.dval+j-1
						ControlInfo TabIEPEAnalysis
						tmpTabName1 = S_Value
					while (strLen(tmpTabName1)>0 )
					break
				default:
			endswitch

			// Return to the current tab
			String controlsNdependVal = ControlNameList("", ";", "IEPE*")
			String ControlsInCur, ControlsInOther
			strswitch(SV.win)
				case "IEPEAnalysisPanel":
					// Set current Tab
					if (currentTabNumber > SV.dval)
						currentTabNumber = SV.dval
					endif
					TabControl TabIEPEAnalysis value= currentTabNumber
					ControlsInCur = ListMatch(controlsNdependVal, "IEPE*"+num2istr(currentTabNumber))
					ControlsInOther =  ListMatch(controlsNdependVal, "!"+"IEPE*"+num2istr(currentTabNumber))
					// tune mFD if exists.
					make_mFD(SV.ctrlName, SV.dval, "IEPEW", "IEPEPos", "IEPECoA", "IEPEInt", 0, 0, 0)
					break
				case "IEPEAnalysisPanel2":
					// Set current Tab
					if (currentTabNumber > SV.dval-1)
						currentTabNumber = SV.dval-1
					endif
					TabControl TabIEPEAnalysis value= currentTabNumber
					ControlsInCur = ListMatch(controlsNdependVal, "IEPE*"+num2istr(currentTabNumber+1))
					ControlsInOther =  ListMatch(controlsNdependVal, "!"+"IEPE*"+num2istr(currentTabNumber+1))
					ControlsInCur +=ListMatch(controlsNdependVal, "IEPE*0")
					// tune mFD if exists.
					make_mFD2(SV.ctrlName, SV.dval, "IEPEW", "IEPEPos", "IEPECoA", "IEPEInt", 0, 0, 0)
					break
			endswitch
			ModifyControlList ControlsInOther disable =1	 // hide
			ModifyControlList ControlsInCur disable =0   // show
			String TempFormula = "root:Packages:LaserUPS:IEPEW"+num2istr(currentTabNumber)+"/(8.61708*10^(-5)"
			SetFormula root:Packages:LaserUPS:TfromW, TempFormula
			//
			NVar RPosExcitation = root:Packages:LaserUPS:RPosExcitation
			String posdiffFormula = ""
			posdiffFormula = "root:Packages:LaserUPS:IEPEPos"
			posdiffFormula += num2istr(currentTabNumber)
			posdiffFormula += "- root:Packages:LaserUPS:IEPEPos"
			posdiffFormula += num2istr(RPosExcitation)
			SetFormula root:Packages:LaserUPS:PosDiff, posdiffFormula
			break
	endswitch
End


Function VibBroadFunctionSelectrion(PU)
	STRUCT WMPopupAction &PU
	SVar VibBroad = root:Packages:LaserUPS:VibBroad
	NVar N = root:Packages:LaserUPS:g_NumExcitation
	String VibFormula
	Variable i
	String PStr="IEPEPos"
	String IStr = "IEPEInt"
	String Wstr = "IEPEW"
	String VibFullPath= ""
	If (PU.eventCode==2)
		Wave vib1=root:Packages:LaserUPS:vib1
		Variable VibWaveSize = numpnts(vib1)
		Variable delx = deltax(vib1)
		VibBroad = PU.popStr
		For (i=1; i<=N; i+=1)
			Strswitch (PU.popStr)
				case "Lorentzian":
					VibFormula = "+ Lorentzian(x, "
					break
				case "Gaussian":
					VibFormula = "+ gaussian(x, "
					break
			endswitch
			VibFormula += PStr+num2istr(i)
			VibFormula += ","+IStr+num2istr(i)
			VibFormula += ","+Wstr+num2istr(i)+")"
			VibFullPath = "root:Packages:LaserUPS:vib"+num2istr(i)
			Wave Vib = $VibFullPath
			SetScale /P x,  -(VibWaveSize)*delx/2, delx, "eV", Vib
			SetFormula $VibFullPath, VibFormula
		endfor
	endif
End


///////////////////////////////////////////////////////////////////////
///  Obsolute function.
///////////////////////////////////////////////////////////////////////

Function /S MultiFDString(N, EVac, WidthStr, PosStr, CoAStr, IntStr)
	Variable N
	Variable EVac
	String WidthStr
	String PosStr
	String CoAStr
	String IntStr
	//
	Variable i=0
	//
	String AFD =""
	String MFD = ""
	String W, P, A, Int
	For (i =0; i<=N; i+=1)
		W = WidthStr + num2istr(i)
		P = PosStr + num2istr(i)
		A = CoAstr + num2istr(i)
		Int = IntStr + num2istr(i)
		AFD = "+("+A+"*x+1-"+A+"*"+P+")*"+Int+"/(exp((x-"+P+")/"+W+")+1)"
		MFD += AFD
	EndFor
	Return MFD
End
