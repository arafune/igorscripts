#pragma rtGlobals=3		// Use modern global access method.
#pragma ModuleName=FCfit
#pragma IgorVersion = 4.0
#include "RA_common"

Function FranckCondonFit()

	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	// Variable setting for FC peaks 1
	Variable s_1 = NumVarOrDefault("root:Packages:LaserUPS:s_1", 1)
	Variable /G root:Packages:LaserUPS:s_1 = s_1
	Variable baseHeight_1 = NumVarOrDefault("root:Packages:LaserUPS:baseHeight_1", 1000)
	Variable /G root:Packages:LaserUPS:baseHeight_1 = baseHeight_1
	Variable widthSpectrum_1 = NumVarOrDefault("root:Packages:LaserUPS:widthSpectrum_1", 1)
	Variable /G root:Packages:LaserUPS:widthSpectrum_1 = widthSpectrum_1
	Variable vibEnergy_1 = NumVarOrDefault("root:Packages:LaserUPS:vibEnergy_1", 1)
	Variable /G root:Packages:LaserUPS:vibEnergy_1 = vibEnergy_1
	Variable E0_1 = NumVarOrDefault("root:Packages:LaserUPS:E0_1", 1)
	Variable /G root:Packages:LaserUPS:E0_1 = E0_1
	Variable PeakN_1 =  NumVarOrDefault("root:Packages:LaserUPS:PeakN_1", 1)
	Variable /G root:Packages:LaserUPS:PeakN_1 = PeakN_1
	Variable deltaBaseHeight_1 = NumVarOrDefault("root:Packages:LaserUPS:deltaBaseHeight_1", 10)
	Variable /G root:Packages:LaserUPS:deltaBaseHeight_1 = deltaBaseHeight_1
	// Variable setting for FC peaks 2
	Variable s_2 = NumVarOrDefault("root:Packages:LaserUPS:s_2", 0)
	Variable /G root:Packages:LaserUPS:s_2 = s_2
	Variable baseHeight_2 = NumVarOrDefault("root:Packages:LaserUPS:baseHeight_2", 0)
	Variable /G root:Packages:LaserUPS:baseHeight_2 = baseHeight_2
	Variable widthSpectrum_2 = NumVarOrDefault("root:Packages:LaserUPS:widthSpectrum_2", 0)
	Variable /G root:Packages:LaserUPS:widthSpectrum_2 = widthSpectrum_2
	Variable vibEnergy_2 = NumVarOrDefault("root:Packages:LaserUPS:vibEnergy_2", 0)
	Variable /G root:Packages:LaserUPS:vibEnergy_2 = vibEnergy_2
	Variable E0_2 = NumVarOrDefault("root:Packages:LaserUPS:E0_2", 0)
	Variable /G root:Packages:LaserUPS:E0_2 = E0_2
	Variable PeakN_2 =  NumVarOrDefault("root:Packages:LaserUPS:PeakN_2", 1)
	Variable /G root:Packages:LaserUPS:PeakN_2 = PeakN_2
	Variable deltaBaseHeight_2 = NumVarOrDefault("root:Packages:LaserUPS:deltaBaseHeight_2", 10)
	Variable /G root:Packages:LaserUPS:deltaBaseHeight_2 = deltaBaseHeight_2
	// Build a panel
	String panelName = "FCfit"
	if (WinType(panelName)==7)
		DoWindow/F $panelName
	else
		NewPanel /N=$panelName /W=(73,83,660,501) as "Franck Condon Envelope"
		ModifyPanel cbRGB=(65280,59904,48896)
		SetDrawLayer UserBack
		SetDrawEnv linethick= 0
		DrawRRect 233,42,566,197
		SetDrawEnv linethick= 0
		DrawRRect 233,201,566,356
		DrawText 414,126,"( meV )"
		DrawText 414,157,"( meV )"
		DrawText 416,186,"( eV )"
		DrawText 414,287,"( meV )"
		DrawText 414,318,"( meV )"
		DrawText 416,347,"( eV )"
		//
		SetDrawEnv fname= "Times New Roman",fsize= 16,fstyle= 1,textrgb= (65280,0,0)
		DrawText 240,66,"Peaks (I)"
		SetDrawEnv fname= "Times New Roman",fsize= 16,fstyle= 1,textrgb= (65280,0,0)
		DrawText 236,221,"Peaks (II)"
		SetDrawEnv gstart
		SetDrawEnv fname= "Times New Roman",fsize= 16,fstyle= 3
		DrawText 472,69,"P  =         e"
		SetDrawEnv fname= "Times New Roman",fsize= 16,fstyle= 3
		DrawText 512,80,"n!"
		SetDrawEnv fname= "Times New Roman",fsize= 16,fstyle= 3
		DrawText 516,60,"s"
		SetDrawEnv linethick= 2
		DrawLine 504,60,532,60
		SetDrawEnv fname= "Times New Roman",fstyle= 3
		DrawText 483,73,"n"
		SetDrawEnv fname= "Times New Roman",fstyle= 3
		DrawText 544,61,"-s"
		SetDrawEnv fname= "Times New Roman",fstyle= 3
		DrawText 525,52,"n"
		SetDrawEnv gstop
		DrawText 488,101,"(increment)"
		DrawText 488,262,"(increment)"
		//
		ListBox WaveSelectorList,pos={10,45},size={216,268},font="Helvetica"
		MakeListIntoWaveSelector(panelName, "WaveSelectorList", selectionMode=WMWS_SelectionSingle, content=WMWS_Waves)
		//
		CheckBox FromTarget,pos={62,324},size={114,18},proc=CheckProc_SingleCheck,title="From Top Graph"
		CheckBox FromTarget,font="Helvetica",value= 0
		//
		Button close,pos={513,388},size={50,20},proc=DonePanelButton,title="Close"
		Button close,font="Helvetica"
		Button AnalyzeThis,pos={30,365},size={190,40},proc=FCfit#AnalysisFCspectra,title="Analyze (create new graph)"
		Button AnalyzeThis,font="Helvetica"
		Button SaveFittingWave,pos={241,389},size={50,20},proc=FCfit#AnalysisFCspectra,title="Save"
		Button SaveFittingWave,font="Helvetica"
		Button KillDependency,pos={332,389},size={90,20},title="kill formula"
		Button KillDependency,font="Helvetica",fSize=10, proc=FCfit#AnalysisFCspectra
		//
		PopupMenu FCPeaksN,pos={237,366},size={164,20},bodyWidth=78,proc=FCfit#N_FCpeaks,title="N of FC peaks"
		PopupMenu FCPeaksN,font="Helvetica"
		PopupMenu FCPeaksN,mode=1,popvalue="Single",value= #"\"Single;Double\""
		//
		SetVariable s_1,pos={308,54},size={105,22},bodyWidth=90,title="s:"
		SetVariable s_1,font="Helvetica"
		SetVariable s_1,limits={0,inf,0.01},value= root:Packages:LaserUPS:s_1
		SetVariable widthSpectrum_1,pos={276,109},size={137,22},bodyWidth=90,title="FWHM:"
		SetVariable widthSpectrum_1,font="Helvetica"
		SetVariable widthSpectrum_1,limits={0,inf,1},value= root:Packages:LaserUPS:widthSpectrum_1
		SetVariable intSpectrum_1,pos={246,82},size={167,22},bodyWidth=90,title="Base height:"
		SetVariable intSpectrum_1,font="Helvetica", limits={0,inf,deltaBaseHeight_1}
		SetVariable intSpectrum_1,value= root:Packages:LaserUPS:baseHeight_1
		SetVariable VibEnergy_1,pos={256,139},size={157,22},bodyWidth=90,title="VibEnergy:"
		SetVariable VibEnergy_1,font="Helvetica"
		SetVariable VibEnergy_1,value= root:Packages:LaserUPS:vibEnergy_1
		SetVariable E0_1,pos={300,167},size={113,22},bodyWidth=90,title="E0:"
		SetVariable E0_1,font="Helvetica",value= root:Packages:LaserUPS:E0_1, limits={-inf, inf, 0.01}
		SetVariable peakN_1,pos={474,167},size={80,22},bodyWidth=35,title="Peak N"
		SetVariable peakN_1,font="Helvetica"
		SetVariable peakN_1,limits={1,inf,1},value= root:Packages:LaserUPS:PeakN_1
		SetVariable deltabaseHeight1,pos={426,82},size={45,22},title=" ",  proc=FCfit#changeIncrementBaseHeight
		SetVariable deltabaseHeight1,font="Helvetica"
		SetVariable deltabaseHeight1,value= root:Packages:LaserUPS:deltaBaseHeight_1

		//
		SetVariable s_2,pos={308,215},size={105,22},bodyWidth=90,title="s:"
		SetVariable s_2,font="Helvetica"
		SetVariable s_2,limits={0,inf,0.01},value= root:Packages:LaserUPS:s_2
		SetVariable widthSpectrum_2,pos={276,270},size={137,22},bodyWidth=90,title="FWHM:"
		SetVariable widthSpectrum_2,font="Helvetica"
		SetVariable widthSpectrum_2,limits={0,inf,1},value= root:Packages:LaserUPS:widthSpectrum_2
		SetVariable intSpectrum_2,pos={246,243},size={167,22},bodyWidth=90,title="Base height:"
		SetVariable intSpectrum_2,font="Helvetica", limits={0,inf,deltaBaseHeight_2}
		SetVariable intSpectrum_2,value= root:Packages:LaserUPS:baseHeight_2
		SetVariable VibEnergy_2,pos={256,300},size={157,22},bodyWidth=90,title="VibEnergy:"
		SetVariable VibEnergy_2,font="Helvetica"
		SetVariable VibEnergy_2,value= root:Packages:LaserUPS:vibEnergy_2
		SetVariable E0_2,pos={300,328},size={113,22},bodyWidth=90,title="E0:"
		SetVariable E0_2,font="Helvetica",value= root:Packages:LaserUPS:E0_2, limits={-inf, inf, 0.01}
		SetVariable peakN_2,pos={474,328},size={80,22},bodyWidth=35,title="Peak N"
		SetVariable peakN_2,font="Helvetica"
		SetVariable peakN_2,limits={1,inf,1},value= root:Packages:LaserUPS:PeakN_2
		SetVariable deltabaseHeight2,pos={426,243},size={45,22},title=" ", proc=FCfit#changeIncrementBaseHeight
		SetVariable deltabaseHeight2,font="Helvetica"
		SetVariable deltabaseHeight2,value= root:Packages:LaserUPS:deltaBaseHeight_2
	endif
End


static Function AnalysisFCspectra(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	//
	String  objs = WS_SelectedObjectsList("FCfit", "WaveSelectorList")
	String theWave = StringFromList(0, objs)
	String thefitWave = theWave+"_FCfit"
	String theresWave = theWave+"_resFCfit"
	String NoteText = ""
	//
	NVAR  s_1 = root:Packages:LaserUPS:s_1
	NVAR  baseHeight_1 = root:Packages:LaserUPS:baseHeight_1
	NVAR  widthSpectrum_1 = root:Packages:LaserUPS:widthSpectrum_1
	NVAR  vibEnergy_1 = root:Packages:LaserUPS:vibEnergy_1
	NVAR  E0_1 = root:Packages:LaserUPS:E0_1
	NVAR  PeakN_1 =  root:Packages:LaserUPS:PeakN_1
	NVAR  s_2 = root:Packages:LaserUPS:s_2
	NVAR  baseHeight_2 = root:Packages:LaserUPS:baseHeight_2
	NVAR  widthSpectrum_2 = root:Packages:LaserUPS:widthSpectrum_2
	NVAR  vibEnergy_2 = root:Packages:LaserUPS:vibEnergy_2
	NVAR  E0_2 = root:Packages:LaserUPS:E0_2
	NVAR  PeakN_2 =  root:Packages:LaserUPS:PeakN_2

	//

	Strswitch (ba.ctrlName)
		case "AnalyzeThis":
			switch(ba.eventCode)
				case 2: // mouse up
					if (strlen(objs)==0)
						return 0
					else
						//				Print ba.ctrlName
						Duplicate /O $theWave root:Packages:LaserUPS:FranckCondon
						Duplicate /O $theWave root:Packages:LaserUPS:residualFranckCondon
						Wave  FranckCondon = root:Packages:LaserUPS:FranckCondon
						Wave  residualFranckCondon = root:Packages:LaserUPS:residualFranckCondon
						String formulaforRes = theWave + "- root:Packages:LaserUPS:FranckCondon"
						ControlInfo /W=FCfit FCPeaksN
						StrSwitch (S_value)
							case "Single":
								Print "S"
								SetFormula  root:Packages:LaserUPS:FranckCondon, "FCgaussian(x, s_1, baseheight_1, widthSpectrum_1, vibEnergy_1, E0_1, PeakN_1-1)"
								SetFormula  root:Packages:LaserUPS:residualFranckCondon, formulaforRes
								break
							case "Double":
								Print "D"
								SetFormula  root:Packages:LaserUPS:FranckCondon, "FCgaussian(x, s_1, baseheight_1, widthSpectrum_1, vibEnergy_1, E0_1, PeakN_1-1) + FCgaussian(x, s_2, baseheight_2, widthSpectrum_2, vibEnergy_2, E0_2, PeakN_2-1)"
								SetFormula  root:Packages:LaserUPS:residualFranckCondon, formulaforRes
								break
						Endswitch
						Display $theWave, root:Packages:LaserUPS:FranckCondon
						AppendToGraph/L=Res root:Packages:LaserUPS:residualFranckCondon
						ChangeSymbolColors("Rainbow")
						ModifyGraph freePos(Res)=0
						ModifyGraph axisEnab(Res)={0.8,1}
						ModifyGraph axisEnab(left)={0,0.75}
					endif
					break
			endswitch
			break
		case "SaveFittingWave":
			switch(ba.eventCode)
				case 2: // mouse up
					if (strlen(objs)==0)
						return 0
					elseif (waveexists (root:Packages:LaserUPS:FranckCondon) ==0)
						return 0
					else
						Duplicate  /O root:Packages:LaserUPS:FranckCondon, $thefitWave
						SetFormula $thefitWave, ""
						Duplicate  /O root:Packages:LaserUPS:residualFranckCondon, $theresWave
						SetFormula $theresWave, ""
						ControlInfo /W=FCfit FCPeaksN
						StrSwitch (S_value)
							case "Single":
								NoteText = "s_1:"+num2str(s_1)+";baseHeight_1:"+num2str(baseHeight_1)+";widthSpectrum_1:"+num2str(widthSpectrum_1)
								NoteText += ";VibEnergy_1:"+num2str(VibEnergy_1)+";E0_1:"+num2str(E0_1)+";PeakN_1:"+num2str(PeakN_1)
								Print NoteText
								Note /K $thefitWave
								Note $thefitWave, NoteText
								break
							case "Double":
								NoteText = "s_1:"+num2str(s_1)+";baseHeight_1:"+num2str(baseHeight_1)+";widthSpectrum_1:"+num2str(widthSpectrum_1)
								NoteText += ";VibEnergy_1:"+num2str(VibEnergy_1)+";E0_1:"+num2str(E0_1)+";PeakN_1:"+num2istr(PeakN_1)
								NoteText += ";s_2:"+num2str(s_2)+";baseHeight_2:"+num2str(baseHeight_2)+";widthSpectrum_2:"+num2str(widthSpectrum_1)
								NoteText += ";VibEnergy_2:"+num2str(VibEnergy_2)+";E0_2:"+num2str(E0_2)+";PeakN_2:"+num2istr(PeakN_2)
								Print NoteText
								Note /K $thefitWave
								Note $thefitWave, NoteText
								break
						Endswitch
					endif
					break
			endswitch
			break
		case "KillDependency":
			switch(ba.eventCode)
				case 2: // mouse up
					SetFormula  root:Packages:LaserUPS:FranckCondon, ""
					SetFormula  root:Packages:LaserUPS:residualFranckCondon, ""
					break
			endswitch
			break
	endswitch

	return 0
End


static Function N_FCpeaks(pa) : PopupMenuControl
	STRUCT WMPopupAction &pa

	switch (pa.eventCode)
		case 2: // mouse up
			Variable popNum = pa.popNum
			String popStr = pa.popStr
			strswitch (popStr)
				case "Single":
					SetFormula  root:Packages:LaserUPS:FranckCondon, "FCgaussian(x, s_1, baseheight_1, widthSpectrum_1, vibEnergy_1, E0_1, PeakN_1-1)"
					break
				case "Double":
					SetFormula  root:Packages:LaserUPS:FranckCondon, "FCgaussian(x, s_1, baseheight_1, widthSpectrum_1, vibEnergy_1, E0_1, PeakN_1-1) + FCgaussian(x, s_2, baseheight_2, widthSpectrum_2, vibEnergy_2, E0_2, PeakN_2-1)"
					break
			endswitch
			break
	endswitch

	return 0
End



Function FCgaussian(x, s, bheight, fwhm, vib, E0, maxn)
	Variable x
	Variable s
	Variable bheight // which means "base height"
	Variable fwhm    // (meV)
	Variable vib       //  (meV)
	Variable E0       //  (eV)
	Variable maxn
	//
	if (maxn < 0)
		return Nan
	endif
	if (bheight ==0)
		return 0
	endif
	if (fwhm == 0)
		return 0
	endif
	//
	Variable sigma =  fwhm/2/sqrt(2*Ln(2)) /1000  // including unit conversion to eV from meV.
	Variable i = 0
	Variable FCgaussian = 0
	For (i = 0; i <= maxn ; i += 1)
		FCgaussian = FCgaussian + s^i * exp(-s) / Fraction(i)  * bheight * Gauss(x, E0 - i * vib/1000, sigma)
	EndFor
	return FCgaussian
End

static Function changeIncrementBaseHeight(sva) : SetVariableControl
	STRUCT WMSetVariableAction &sva
	NVar deltaBaseHeight_1 =  root:Packages:LaserUPS:deltaBaseHeight_1
	NVar deltaBaseHeight_2 =  root:Packages:LaserUPS:deltaBaseHeight_2
	switch(sva.eventCode)
		case 1: // mouse up
		case 2: // Enter key
		case 3: // Live update
			SetVariable intSpectrum_1, limits={0,inf,deltaBaseHeight_1}
			SetVariable intSpectrum_2, limits={0,inf,deltaBaseHeight_2}
			Variable dval = sva.dval
			String sval = sva.sval
			break
	endswitch

	return 0
End
