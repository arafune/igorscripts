ARPES data の Load, Analysis
================================

# Introduction

Prodigyになって以後、いくつかのプロシージャ・ファイルが使いにくかったり、
fs レーザーになってエネルギー分解能が下がった分、気の利いた解析が必要そうだったりしたので改訂。

# 説明するファイル

* arpesanalysis.ipf
* arpes_derivative.ipf
* SPECS_SP2.ipf

# インストール

普通に。

## Mac

* $(HOME)/Documents/WaveMetrics/Igor Pro 9 User Files/User Procedures/
に SPD ディレクトリを作り、その中に全ての ipf ファイルを置く。
* loadRA-procs.ipf の エイリアスを $(HOME)/Documents/WaveMetrics/Igor Pro 9 User Files/Igor Procedures/ におく。

## Windows

* ドキュメント/WaveMetrics/Igor Pro 9 User Files/User Procedures/
に SPD ディレクトリを作り、その中に全ての ipf ファイルを置く。
* loadRA-procs.ipf の ショートカットを ドキュメント/WaveMetrics/Igor Pro 9 User Files/Igor Procedures/ におく。

# 分光ファイルのロード

Prodigyになって、Igor のテキストデータを出力してくれるようになった。
測定毎に一つの itx ファイルを出力にするように設定した方が(結局は便利：まとめるのは後でいくらでもできるので）

## itx ファイル

もともとIgorによって規定されているテキスト・ファイル・フォーマット。
そのまま読み込む事ができる。
基本的には、一つのスペクトルに一つのデータにしておく(の方が分かりやすい）。

ただ、Prodigy が作る itx は

* 角度、エネルギー情報は保存。

するものの、

* Wave名をProdigy における "Name" (Spectrum 名)にしてしまう（.sle ファイル内の一意性は無い。一意性を担保しているのはID)
* User comment, Excitation Energy がWaveには記載されない。
    他は大した情報書いていないけど、ここで書いてある項目ぐらいは wave にも引き継いで欲しい。

という欠点がある。Commentに beta angle や温度の情報を Igor list を想定して記載するようにしておくと解析時便利。
(Beta:-4;Temperature:10 みたいに書いておく)

python で、これらの欠点を修正した itx ファイルを作るようにした。
（このパッケージにはpython script は含まれていない。）

```bash
mkdir orig
mv *.itx orig/
cd orig
for file in *itx
    python3 tune_itx.py -o ../$file $file
end
```

みたいにしておいてから、そのitx を使うと良い。
また、複数のitx を全部一つの itx にするのが、結局手間が省ける。

```bash
python3 tune_itx.py *.itx > all_data.itx
```

とするのが結局は楽。
この all_data.itx で全部のスペクトルデータを一度に読めるようにしてある。

## sp2ファイル

Prodigy が用いているSp2ファイルはシンプルに一つのImageDataに対応する。
中身はテキストファイル。
インストールが正しければ、 Data-> Load Waves -> に "Load SP2 file"
と "Load SP2 files in a folder"
の項目が現れる。itx ファイルのロードは "Load Igor Text" を選ぶ。
![スクリーンショット](imgs/ss01.png)
複数の sp2 ファイルをロードするときには、 "Load SP2 files in a folder" を用いる。
最初のステップで、sp2ファイルのあるフォルダを選ぶと、"Select File(s)" Windowが開く(下図）
![スクリーンショット](imgs/ss02.png)

シフトキー（or Ctrlキー)を押しながら選択することで複数のファイルを選択できる。
Done を押すとロードが始まる。

# 解析

## 分散の強調

arpes_derivatif.ipf に 関数としてまとめている。

### Curvature method

Rev. Sci. Instrum. **82**, 043712 (2011).

* 2階微分が処理の中に含まれるので、スムージング必須。
* 1Dと2Dの二つがある。
* 1Dについては Energy 方向の曲率を求めるアプローチと角度方向の曲率を求めるアプローチの二つがある。

#### 1次元

```
Curvature1D(mat, n_iteration, box, direction, factor)
```

* mat: 処理したいmatrix データ。
* n_iteration : boxcar スムージングを何回行うか
* box: boxcar スムージングの幅
* direction: 曲率を求める方向。0 が エネルギー方向 1 が角度方向
* factor: 論文での *a_0* の値。 0.01-1 の間ぐらい。

#### 2次元

```
Curvature2D(mat, n_iteration1, box1, n_iteration2, box2, factor, weight2d)
```

Curvature1D と似ている。

* mat: 処理したいmatrix データ。
* n_iteration1 : boxcar スムージングを何回行うか (エネルギー方向)
* box1: boxcar スムージングの幅 (エネルギー方向） n_iteration2 : boxcar スムージングを何回行うか (角度方向)
* box2: boxcar スムージングの幅 (角度方向）
* direction: 曲率を求める方向。0 が エネルギー方向 1 が角度方向
* factor: 論文での *a_0* の値。 0.01-1 の間ぐらい。
* weight2d: エネルギー軸と角度軸の重み付け

### Minimum Gradient method

Rev. Sci. Instrum. **88**, 073903 (2017).

* 1階微分だけの処理。でもスムージングは必須（らしい：論文には通常のプロシージャだろ？っとある。）。
* やってみると、画像の端、がいまいち。少なくとも僕のシステムでは　Curvature method の方が優秀に見える。
* "傾斜が少ないところ"  を強調するので、値が小さいところ、も強調されてしまう。
のが欠点といえば欠点か。
(通常のARPESだと、２次電子バックグランドがあるので、
強度ゼロがずっと続くなんてところがあまりないのかも知れない。)

![分散強調のExample](imgs/dispersion_highlihght_example.png)

## 角度 -> 波数の変換

ImageInterpolate を使うのをやめたので、かなり速度向上。
実時間で変換できる。

arpesanalysis.ipf に

```
arpes2band(src, energyshift)
```

を使う。

* src: 変換する ARPESデータ (角度の単位はdegree であること)
* energyshift: 電子のエネルギーが真空準位を0とした運動エネルギー
となるようにシフトさせる。

energyshiftは　通常のProdigy を基点として作ってきた
ものであれば、エネルギーは　``Final state energy`` なので、
この energyshift には ```-"worfunction"``` を入れれば良い。

## ビニング

同じく arpesanalysis.ipf に

```
Binning2D(src, n_bin, axis)
```

* src: ビニングしたいデータ
* n_bin: ビニングの幅
* axis: ビニングの軸 (0: x軸、1: y軸)

もとのWave に "_binned"というおまけが付いた新しいWaveを作る。

上のコマンドのsyntax sugar

```
binning_angle(src, n_bin)
```

もある。普段はコッチを使っておいた方がよいか。

# UI

arpes2band, Curvature1D, Binningについては、GUIを準備。
Analysis->ARPES から適宜選択。
