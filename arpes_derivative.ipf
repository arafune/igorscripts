#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3             // Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}     // Set default tab width in Igor Pro 9 and later
#pragma ModuleName=ARPES_D
#pragma IgorVersion = 8.00
#include <WaveSelectorWidget>

// This module provides two way
// about "peak position enhancement in ARPES image"


// *NOTE* The data must be in the root folder.
// The output data are also in the root folder,



/// Minimum Gradient Method based on Rev. Sci. Instrum. 88, 073903 (2017).
///
/// @param src     Source Wave
/// @param r
/// @param c
Function/S vector_diff(Wave src, Variable r, Variable c)
	String shifted
	shifted = NameOfWave(src)+shift_label(r, c)
	wave tmp_wave, tmp_wave2, tmp_wave3
	if (r != 0)
		MatrixOP /FREE tmp_wave = rotateRows(src, r)
	endif
	if (c != 0)
		if (WaveExists(tmp_wave))
			MatrixOP /FREE tmp_wave2 = rotateCols(tmp_wave, c)
		else
			MatrixOP /FREE tmp_wave2 = rotateCols(src, c)
		endif
	else
		Duplicate /FREE tmp_wave, tmp_wave2
	endif
	//
	MatrixOP /O/FREE tmp_wave = src-tmp_wave2
	//
	if (r > 0)
		MatrixOP /O/FREE tmp_wave2 = rotateRows(tmp_wave, -r)
	else
		Duplicate /O/FREE tmp_wave, tmp_wave2
	endif
	if (c > 0)
		if (WaveExists(tmp_wave2))
			MatrixOP /O $shifted = rotateCols(tmp_wave2, -c)
		else
			Duplicate /O tmp_wave3, $shifted
		endif
	else
		Duplicate /O tmp_wave2, $shifted
	endif
	Variable n_row, n_column
	n_row = DimSize($shifted, 0)
	n_column = DimSize($shifted, 1)
	Redimension /N=(n_row-abs(r), n_column-abs(c)) $shifted
	return shifted
End

/// @param mat
/// @param n_iteration_ek
/// @param box_ek
/// @param n_iteration_angle
/// @param box_angle
Function minimum_gradient(Wave mat, Variable n_iteration_ek, Variable box_ek, Variable n_iteration_angle, Variable box_Angle)
	wave gradient0, gradient1, gradient2, gradient3
	wave gradient4, gradient5, gradient6, gradient7
	// gradient0
	String tmp
	Variable n_row, n_column
	n_row = DimSize(mat, 0)
	n_column = DimSize(mat, 1)
	MatrixOP /FREE src = mat
	//
	//
	Smooth/DIM=0/E=3/B=(n_iteration_Angle) box_Angle, src
	Smooth/DIM=1/E=3/B=(n_iteration_Ek) box_Ek, src
	// オリジナルのデータは変えない。
	// DIM の　"0" と　"1" が　逆転してるのは、 Smooth の仕様
	// DIM=0 が row 方向の､DIM=1 がColumn 方向のスムージングとあるんだが、
	// Igor の場合 Table で表示してあるときは "縦方向" が row で
	// "横方向" が　Column になるので
	tmp = vector_diff(src, 1, 0)
	wave tmpwave = $tmp
	MatrixOP /O/FREE gradient0 = tmpwave
	KillWaves /Z $tmp
	Redimension /N=(n_row, n_column) gradient0
	//
	tmp = vector_diff(src, 0, 1)
	wave tmpwave = $tmp
	MatrixOP /O/FREE gradient1 = tmpwave
	KillWaves /Z $tmp
	Redimension /N=(n_row, n_column) gradient1
	//
	tmp = vector_diff(src, -1, 0)
	wave tmpwave = $tmp
	wave tmpwave2
	MatrixOP /FREE/O tmpwave2 = tmpwave
	Redimension /N=(n_row, n_column) tmpwave2
	KillWaves /Z $tmp
	MatrixOP /O/FREE gradient2 = rotateRows(tmpwave2, 1)
	//
	tmp = vector_diff(src, 0, -1)
	wave tmpwave = $tmp
	wave tmpwave2
	MatrixOP /FREE/O tmpwave2 = tmpwave
	Redimension /N=(n_row, n_column) tmpwave2
	KillWaves /Z $tmp
	MatrixOP /O/FREE gradient3 = rotateCols(tmpwave2, 1)
	//
	tmp = vector_diff(src, 1, 1)
	wave tmpwave = $tmp
	wave tmpwave2
	MatrixOP /FREE/O tmpwave2 = tmpwave
	Redimension /N=(n_row, n_column) tmpwave2
	KillWaves /Z $tmp
	MatrixOP /O/FREE gradient4 = tmpwave2
	//
	tmp = vector_diff(src, 1, -1)
	wave tmpwave = $tmp
	wave tmpwave2
	MatrixOP /FREE/O tmpwave2 = tmpwave
	Redimension /N=(n_row, n_column) tmpwave2
	KillWaves /Z $tmp
	MatrixOP /O/FREE gradient5 = rotateCols(tmpwave2, 1)
	//
	tmp = vector_diff(src, -1, 1)
	wave tmpwave = $tmp
	wave tmpwave2
	MatrixOP /FREE/O tmpwave2 = tmpwave
	Redimension /N=(n_row, n_column) tmpwave2
	KillWaves /Z $tmp
	MatrixOP /O/FREE gradient6 = rotateRows(tmpwave2, 1)
	//
	tmp = vector_diff(src, -1, -1)
	wave tmpwave = $tmp
	wave tmpwave2
	MatrixOP /FREE/O tmpwave2 = tmpwave
	Redimension /N=(n_row, n_column) tmpwave2
	MatrixOP /FREE/O tmpwave3 = rotateRows(tmpwave2, 1)
	KillWaves /Z $tmp
	MatrixOP /O/FREE gradient7 = rotateCols(tmpwave3, 1)
	//
	String gradient_wave_name = NameOfWave(mat) + "_gradient"
	Wave gradient_wave = $gradient_wave_name
	MatrixOP /O gradient_wave = sqrt(magsqr(gradient0) + magsqr(gradient1) + magsqr(gradient2) + magsqr(gradient3) + magsqr(gradient4) + magsqr(gradient5) + magsqr(gradient6) + magsqr(gradient7))
	String dest_wave = NameOfWave(mat) + "_MG"
	MatrixOP /O $dest_wave = src/gradient_wave
	//
	String note_str
	note_str = note(mat)
	note_str += "n_iteration_Angle:"+num2istr(n_iteration_angle)+"\n"
	note_str += "box_Angle:"+num2istr(box_Angle)+"\n"
	note_str += "n_iteration_Ek:"+num2istr(n_iteration_Ek)+"\n"
	note_str += "box_Ek:"+num2istr(box_Ek)+"\n"
	CopyScales /P mat, $dest_wave
	CopyScales /P mat, gradient_wave
	note /NOCR $dest_wave, note_str
End

/// @param r
/// @param c
Static Function/S shift_label(r,c)
	Variable r
	Variable c
	String r_label
	String c_label
	if (!(r==0  || abs(r)==1))
		Abort "Incorrect raw_element"
	endif
	if (!(c==0 || abs(c)==1))
		Abort "Incorrect column_element"
	endif
	if (r<0)
		r_label = "m"+num2istr(abs(r))
	else
		r_label = num2istr(r)
	endif
	if (c<0)
		c_label = "m"+num2istr(abs(c))
	else
		c_label = num2istr(c)
	endif
	return "_" + r_label + c_label
End

///  Curvature method based on Rev. Sci. Instrum. 82, 043712 (2011).
///  The data must be in the root folder.
///  The output data are also in the root folder,
///  with names dataname+"_CV", dataname+"_CH",
///  dataname+"_C2D",dataname+"_DV",
///  dataname+"_DH", and dataname+"_D2D".
///
///  You can also call Curvature1D(mat,n_iteration,box,direction,factor)
///  for EDC/MDC curvature or
///  Curvature2D(mat,n_iteration1,box1,n_iteration2,box2,factor,weight2d)
///  for 2D curvature in your program.
///
///  Curvature(mat, n_iteration, box, direction, factor):
///  mat is the original data.
///  (n_iteration,box) specify the smooth times and
///  box width for boxcar smoothing method.
///  direction = 0 for EDC curvature and EDC 2nd derivative,
///  direction = 1 for MDC curvature and MDC 2nd derivative.
///  factor that is represented a_0 in the literature
///  specifies the arbitrary
///  factor in curvature method.
///  results will be in the current folder with
///  names nameofwave(mat)+"_CV/CH/DV/DH"
///
///  Curvature2D(mat,n_iteration_Ek,box_Ek,n_iteration_Angle,box_Angle,factor,weight2d)
///  mat is the original data.
///  (n_iteration_Ek,box_Ek) specify the smooth times and box width along EDC direction.
///  (n_iteration_Angle,box_Angle) specify the smooth times and box width along MDC direction.
///  factor specifies the arbitrary factor in curvature method.
///  weight2d specifies the weight of the MDC curvature/2nd derivative in the calculation.
///  results will be in the current folder with names nameofwave(mat)+"_C2D/D2D"
/// @param src
/// @param dest
/// @param n_iteration
/// @param box
/// @param direction
Static Function Deviation1D(Wave src, Wave dest, Variable n_iteration, Variable box, Variable direction)
	FastOp dest=src
	switch(direction)
		case 0: //energy direction, col---df/dE
			Smooth/DIM=1/E=3/B=(n_iteration) box, dest
			Differentiate/DIM=1 dest
			break
		case 1: //k direction, row --- df/dk
			Smooth/DIM=0/E=3/B=(n_iteration) box, dest
			Differentiate/DIM=0 dest
			break
		default:
			Abort "direction must be 0 or 1"
			break
	endswitch
End


/// @param mat
/// @param n_iteration
/// @param box
/// @param direction
/// @param factor
Function Curvature1D(Wave mat, Variable n_iteration, Variable box, Variable direction, Variable factor)
	String diff1mat,diff2mat,matcurv
	Variable avg
	String note_str
	note_str = note(mat)
	note_str += "n_iteration:"+num2istr(n_iteration)+"\n"
	note_str += "box:"+num2istr(box)+"\n"
	switch (direction)
		case 0:
			matcurv=NameOfWave(mat)+"_CV"
			diff1mat=NameOfWave(mat)+"dy"; diff2mat=NameOfWave(mat)+"dy2"
			note_str += "direction:Energy\n"
			break
		case 1:
			matcurv=NameOfWave(mat)+"_CH"
			diff1mat=NameOfWave(mat)+"dx"; diff2mat=NameOfWave(mat)+"dx2"
			note_str += "direction:Angle\n"
			break
		default:
			Abort "direction must be 0 or 1"
	endswitch
	if(WaveExists($matcurv)==0)
		Duplicate/o mat $matcurv
	endif
	if(WaveExists($diff1mat)==0)
		Duplicate/o mat $diff1mat, $diff2mat
	endif
	Deviation1d(mat, $diff1mat, n_iteration, box, direction)
	Deviation1d($diff1mat, $diff2mat, 1, 1, direction)
	wave T_diff1mat=$diff1mat
	wave T_diff2mat=$diff2mat
	avg=abs(WaveMin($diff1mat))
	Wave curvature = $matcurv
	curvature=T_diff2mat / (avg * avg * factor + T_diff1mat * T_diff1mat)^1.5
	//
	note /K/NOCR T_diff2mat, note_str
	note_str += "factor_a0:" + num2str(factor) + "\n"
	note /K/NOCR curvature, note_str
End

/// @param mat
/// @param n_iteration_Ek
/// @param box_Ek
/// @param n_iteration_Angle
/// @param box_Angle
/// @param factor
/// @param weight2d
Function Curvature2D(Wave mat, Variable n_iteration_Ek, Variable box_Ek, Variable n_iteration_Angle, Variable box_Angle, Variable factor, Variable weight2d)
	String diff1maty, diff2maty, diff1matx, diff2matx, diff2matyx
	String matcurv, graphcurv, matderiv
	Variable avg, avgv, avgh
	Variable dx, dy, weight
	dx=DimDelta(mat, 0)
	dy=DimDelta(mat, 1)
	weight=(dx / dy) * (dx / dy)
	matcurv=NameOfWave(mat) + "_C2D"
	diff1maty=NameOfWave(mat) + "dy"; diff2maty=NameOfWave(mat) + "dy2"
	diff1matx=NameOfWave(mat) + "dx"; diff2matx=NameOfWave(mat) + "dx2"
	diff2matyx=NameOfWave(mat) + "dy" + "dx"
	matderiv=NameOfWave(mat) + "_D2D"
	if(WaveExists($diff1maty)==0)
		Duplicate/o mat $diff1maty, $diff2maty
	endif
	if(WaveExists($diff1matx)==0)
		Duplicate/o mat $diff1matx, $diff2matx
	endif
	if(WaveExists($diff2matyx)==0)
		Duplicate/o mat $diff2matyx
	endif
	if(WaveExists($matcurv)==0)
		Duplicate/o mat $matcurv
	endif
	if(WaveExists($matderiv)==0)
		Duplicate/o mat $matderiv
	endif
	Deviation1d(mat, $diff1maty, n_iteration_Ek, box_Ek, 0)
	Deviation1d($diff1maty, $diff2maty, 1, 1, 0)
	avgv=abs(WaveMin($diff1maty))
	deviation1d(mat, $diff1matx, n_iteration_Angle, box_Angle, 1)
	Deviation1d($diff1matx, $diff2matx, 1, 1, 1)
	avgh=abs(WaveMin($diff1matx))
	Deviation1d($diff1maty, $diff2matyx, n_iteration_Angle, box_Angle, 1)
	wave T_diff1matV = $diff1maty
	wave T_diff2matV = $diff2maty
	wave T_diff1matH = $diff1matx
	wave T_diff2matH = $diff2matx
	wave T_diff2matVH = $diff2matyx
	wave curvature2D = $matcurv
	if(weight2d>0)
		weight *= weight2d
	endif
	if(weight2d<0)
		weight /= abs(weight2d)
	endif
	avg = max(avgv * avgv, weight * avgh * avgh)
	curvature2D = ((factor * avg+weight * t_diff1math * t_diff1math) \
		* t_diff2matv - 2 * weight * t_diff1math * t_diff1matv * t_diff2matvh + weight * \
		(factor * avg + t_diff1matv * t_diff1matv) * t_diff2math) / \
		(factor * avg + weight * t_diff1math * t_diff1math + t_diff1matv * t_diff1matv)^1.5
	wave Deriv2D = $matderiv
	Deriv2D = t_diff2matV + t_diff2matH * weight
	String note_str
	note_str = note(mat)
	note_str += "n_iteration_angle:" + num2istr(n_iteration_angle) + "\n"
	note_str += "box_Angle:" + num2istr(box_Angle) + "\n"
	note_str += "n_iteration_Ek:" + num2istr(n_iteration_Ek) + "\n"
	note_str += "box_Ek:" + num2istr(box_Ek) + "\n"
	note_str += "factor_a0:" + num2str(factor) + "\n"
	note_str += "weight2d:" + num2str(weight2d) + "\n"
	note /K/NOCR Deriv2D, note_str
	note /K/NOCR curvature2D, note_str
End
