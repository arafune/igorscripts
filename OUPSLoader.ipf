﻿#pragma TextEncoding = "UTF-8"
// Use modern global access method and strict wave access.
#pragma rtGlobals=3
// Author: Ryuichi Arafune
#pragma IgorVertsion = 8.0
#pragma version=1.00
#include "SelectFiles"
#include "WaveSelection"

	Static Constant HEADER_LEN = 504, FIRST_CURSOR_POSITION = 24
	Static Constant META_BLOCK_SIZE = 12

Menu "Load Waves"
	"Load OUPS FTIR Data", load_OUPS("", "")
	"Load OUPS FTIR Data files", Load_OUPS_in_DIR()
End

Menu "Analysis"
	"IRAS spectrum", IRAS_Calc()
End

///
Function Load_OUPS_in_DIR()
	String dir_files, dirName, fileName
	Variable i, numFiles
	dir_files = SelectFilesIndir("????", "????")
	numFiles = ItemsInList(dir_files)
	dirName = StringFromList(0, dir_files)
	for (i=1; i<numFiles; i+=1)
		fileName = dirName + StringFromList(i, dir_files)
		Load_OUPS("", fileName)
	endfor
End


/// @param pathname Name of an existing symbolic path or "" for dialog.
/// @param filename Full path to the file or "" for dialog.
Function load_OUPS(pathname, filename)
	String pathName
	String fileName
	//
	Variable refNum
	String message = "Select OUPS data file"


	// Open file for read.
	Open/R/Z=2/P=$pathName /M=message /T="????" refNum as fileName
	// R: ReadOnly, /Z=2 normal error reporting.
	// When used with /R, it opens the file if it
	// exists or displays a dialog if it does not exist.

	// Store results from Open in a safe place.
	Variable err = V_flag
	String fullPath = S_fileName

	if (err == -1)
		Print "load_OUPS cancelled by user."
		return -1
	endif

	if (err != 0)
		DoAlert 0, "Error in load_OUPS"
		return err
	endif

	Read_OUPSdata(refNum)
	Close refNum
	return 0
End

/// @param refNum File reference number
Static Function Read_OUPSdata (refNum)
	Variable refNum
	//
	String BlockMeta
	BlockMeta = parse_OUPSheader(refNum)
	parse_data(refNum, BlockMeta)
End


/// Parse Header part
/// @param refNum File reference number
Static Function/S parse_OUPSheader(refNum)
	Variable refNum
	// Meta part
	Variable data_type, channel_type, text_type, dummy
	Variable chunk_size, offset, next_offset
	Variable cur=FIRST_CURSOR_POSITION
	String BlockMeta = ""
	FStatus refNum
	do
		if (cur + META_BLOCK_SIZE > HEADER_LEN)
			break
		endif
		FSetPos refNum, cur
		FBinRead /F=1/U refNum, data_type
		FBinRead /F=1/U refNum, channel_type
		FBinRead /F=1/U refNum, text_type
		FBinRead /F=1 refNum, dummy
		FBinRead /F=3/U refNum, chunk_size
		FBinRead /F=3/U refNum, offset
		if (offset <= 0)
			break
		endif
		BlockMeta += num2istr(data_type)+";"+num2istr(channel_type) + ";"
		BlockMeta += num2istr(text_type)+";"+num2istr(chunk_size) + ";"
		BlockMeta += num2istr(offset) + ";"
		//
		next_offset = offset + 4 * chunk_size
		if (next_offset >= V_logEOF)
			break
		endif
		cur += META_BLOCK_SIZE
	while(1)
	return BlockMeta
End

/// @param refNum File reference number
/// @param block_meta List string for header information
Static Function parse_data(refNum, block_meta)
	Variable refNum
	String block_meta
	//
	Variable blocklen = ItemsInList(block_meta)
	Variable i, j
	Variable buf
	Variable data_type, channel_type, text_type, chunk_size, offset
	FStatus refNum
	String fnName = ParseFilePath(3, S_fileName, ":", 0, 0)
	String fnSuffix = ParseFilePath(4, S_fileName, ":", 0, 0)
	String wave_name, param_name, bufstr

	for (i=0; i<blocklen; i+=5)
		data_type = str2num(StringFromList(i, block_meta))
		channel_type = str2num(StringFromList(i+1, block_meta))
		text_type = str2num(StringFromList(i+2, block_meta))
		chunk_size = str2num(StringFromList(i+3, block_meta))
		offset = str2num(StringFromList(i+4, block_meta))
		switch (data_type)
			case 0:
				String TextInfo
				FSetPos refNum, offset
				FReadLine /N=(4*chunk_size) refNum, TextInfo
				// Print TextInfo
				break
			case 7:   // ScSm
				wave_name = BLOCK_7(channel_type)+"_"+fnName+"_"+fnSuffix
				Make /O/N=(chunk_size)  $wave_name
				Wave smWave = $wave_name
				FSetPos refNum, offset
				for (j=0; j<chunk_size; j+=1)
					FBinRead /F=4 refNum, buf
					smWave[j]=buf
				endfor
				break
			case 11:  // ScRf
				wave_name = BLOCK_11(channel_type)+"_"+fnName+"_"+fnSuffix
				Make /O/N=(chunk_size)  $wave_name
				Wave rfWave = $wave_name
				FSetPos refNum, offset
				for (j=0; j<chunk_size; j+=1)
					FBinRead /F=4 refNum, buf
					rfWave[j]=buf
				endfor
				break
			case 15:  // AB
				wave_name = "Ab_"+fnName+"_"+fnSuffix
				Make /O/N=(chunk_size)  $wave_name
				Wave abWave = $wave_name
				FSetPos refNum, offset
				for (j=0; j<chunk_size; j+=1)
					FBinRead /F=4 refNum, buf
					abWave[j]=buf
				endfor
				break
			case 23:  // ScSm Data Parameter
				String ScSm_Param = parse_param(refNum, offset)
				Variable SmFXV, SmLXV
				SmFXV = NumberByKey("FXV", ScSm_Param, "=")
				SmLXV = NumberByKey("LXV", ScSm_Param, "=")
				break
			case 27:  // ScRf Data Parameter
				String RfSm_Param = parse_param(refNum, offset)
				Variable RfFXV, RfLXV
				RfFXV = NumberByKey("FXV", RfSm_Param, "=")
				RfLXV = NumberByKey("LXV", RfSm_Param, "=")
				break
			case 31:  // Abs Data Parameter
				String Ab_Param = parse_param(refNum, offset)
				Variable AbFXV, AbLXV
				AbFXV = NumberByKey("FXV", Ab_Param, "=")
				AbLXV = NumberByKey("LXV", Ab_Param, "=")
				break
			case 32:
				break
			case 40:
				break
			case 48:
				break
			case 56:
				break
			case 64:
				break
			case 72:
				break
			case 96:
				break
			case 104:
				break
			case 160:
				break
		endswitch
	endfor
	if (WaveExists(smWave)==1)
		SetScale/I x SmFXV, SmLXV, "cm\\S-1\M", smWave
		note smWave, ScSm_Param
	endif
	if (WaveExists(rfWave)==1)
		SetScale/I x RfFXV, RfLXV, "cm\\S-1\M", rfWave
		note rfWave, RfSm_Param
	endif
	if (WaveExists(AbWave)==1)
		SetScale/I x AbFXV, AbLXV, "cm\\S-1\M", AbWave
		note AbWave, Ab_Param
	endif
End


/// @param refNum File reference number
/// @param offset offset position
Static Function/S parse_param(refNum, offset)
	Variable refNum, offset
	FSetPos refNum, offset
	String param_name
	Variable dummy
	Variable type_index, param_size
	String dummy_str
	String parameters = ""
	do
		FReadLine /N=3 refNum, param_name
		if (cmpstr(param_name, "END")==0)
			break
		endif

		FBinRead /F=1 refNum, dummy
		FBinRead /F=2/U refNum, type_index
		FBinRead /F=2/U refNum, param_size

		switch (type_index)
			case 0:
				FBinRead /F=3 refNum, dummy
				parameters += param_name + "=" + \
				num2istr(dummy) + ";"
				break
			case 1:
				FBinRead /F=5 refNum, dummy
				sprintf dummy_str, "%4.15f", dummy
				parameters += param_name + "=" + dummy_str +";"
				break
			case 2:
				FReadLine /N=(param_size*2) refNum, dummy_str
				dummy = strsearch(dummy_str, num2char(00), 0)
				parameters += param_name + "=" + \
				dummy_str[0, dummy-1] + ";"
				break
			case 3:
				FReadLine /N=(param_size*2) refNum, dummy_str
				dummy = strsearch(dummy_str, num2char(00), 0)
				parameters += param_name + "=" + \
				dummy_str[0, dummy-1] + ";"
				break
			case 4:
				break
		endswitch
	while (1)

	return parameters
End

Static Function/S BLOCK_0(i)
	Variable i

	switch (i)
		case 8:
			return "Info Block"
			break
		case 104:
			return "History"
			break
		case 152:
			return "Curve Fit"
			break
		case 168:
			return "Signature"
			break
		case 240:
			return "Integration Method"
			break
		default:
			return "Text Information"
	endswitch
End

Static Function/S BLOCK_7(i)
	Variable i

	switch (i)
		case 4:
			return "ScSm"
			break
		case 8:
			return "IgSm"
			break
		case 12:
			return "PhSm"
			break
	endswitch
End

Static Function/S Block_11(i)
	Variable i

	switch (i)
		case 4:
			return "ScRf"
			break
		case 8:
			return "IgRf"
			break
	endswitch
End

Static Function/S Block_23(i)
	Variable i

	switch (i)
		case 4:
			return "ScSm Data Parameter"
			break
		case 8:
			return "IgSm Data Parameter"
			break
		case 12:
			return "PhSm Data Parameter"
			break
	endswitch
End

Static Function/S Block_27(i)
	Variable i

	switch (i)
		case 4:
			return "ScRf Data Parameter"
			break
		case 8:
			return "IgRf Data Parameter"
			break
	endswitch
End

Static Function/S DIFFERENT_BLOCKS(i)
	Variable i

	switch (i)
		case 31:
			return "AB Data Parameter"
			break
		case 32:
			return "Instrument"
			break
		case 40:
			return "Instrument (Rf)"
			break
		case 48:
			return "Acquisition"
			break
		case 56:
			return "Acquisition (Rf)"
			break
		case 64:
			return "Fourier Transformation"
			break
		case 72:
			return "Fourier Transformation (Rf)"
			break
		case 96:
			return "Optik"
			break
		case 104:
			return "Optik (Rf)"
			break
		case 160:
			return "Sample"
			break
	endswitch
End

Function IRAS_Calc()
	DoWindow /F IRAS_Calc_panel
	if (V_Flag !=0)
		return 0
	endif
	//
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	Variable /G root:Packages:LaserUPS:g_isScSm1
	Variable /G root:Packages:LaserUPS:g_isScSm2
	SetDataFolder dfSave
	//
	String wList_Sm, wList_Rf
	NVAR g_isScSm1 = root:Packages:LaserUPS:g_isScSm1
	NVAR g_isScSm2 = root:Packages:LaserUPS:g_isScSm2
	Variable isScSm1 = g_isScSm1
	Variable isScSm2 = g_isScSm2
	wList_Sm = waves2list(isScSm1)
	wList_Rf = waves2list(isScSm2)
	Variable  numWaves_Sm = ItemsInList(wList_Sm)
	Variable  numWaves_Rf = ItemsInList(wList_Rf)
	SetDataFolder root:Packages:LaserUPS
	Make /T/O/N=(numWaves_Sm) root:Packages:LaserUPS:waveList_Sm
	Wave /T wave_Sm = root:Packages:LaserUPS:waveList_Sm
	Make /O/N=(numWaves_Sm) root:Packages:LaserUPS:waveList_Sm_sel
	Wave wave_Sm_sel = root:Packages:LaserUPS:waveList_Sm_sel
	Make /T/O/N=(numWaves_Rf) root:Packages:LaserUPS:waveList_Rf
	Wave /T wave_Rf = root:Packages:LaserUPS:waveList_Rf
	Variable i
	for (i=0; i<numWaves_Sm; i+=1)
		wave_Sm[i] = StringFromList(i, wList_Sm)
		wave_Sm_sel[i] = 0
	endfor
	for (i=0; i<numWaves_Rf; i+=1)
		wave_Rf[i] = StringFromList(i, wList_Rf)
	endfor
	SetDataFolder dfSave
	//
	DrawIRAS_CalcPanel()
End

Static Function DrawIRAS_CalcPanel()
	NVAR g_isScSm1 = root:Packages:LaserUPS:g_isScSm1
	NVAR g_isScSm2 = root:Packages:LaserUPS:g_isScSm2
	Variable isScSm1 = g_isScSm1
	Variable isScSm2 = g_isScSm2
	PauseUpdate; Silent 1		// building window...
	NewPanel /K=1 /W=(107,87,586,584) as "IRAS"
	DoWindow /F/C IRAS_Calc_panel
	SetWindow IRAS_Calc_panel, hook=IRAC_Calc_Panel_Hook
	SetDrawLayer UserBack
	SetDrawEnv fsize= 18
	DrawText 53,67,"Reference Wave"
	SetDrawEnv fsize= 18
	DrawText 270,64,"Sample Wave(s)"
	TitleBox Abs_Calculation,title="Absorption Calculation"
	TitleBox Abs_Calculation,pos={130.00,9.00},size={179.00,30.00}
	TitleBox Abs_Calculation,fSize=18
	ListBox SmWaves,pos={251.00,74.00}
	ListBox smWaves,size={210.00,302.00},font="Helvetica"
	ListBox SmWaves,fSize=18,listWave=root:Packages:LaserUPS:waveList_Sm
	ListBox SmWaves,selWave=root:Packages:LaserUPS:waveList_Sm_sel,mode= 4
	ListBox RfWaves,pos={21.00,75.00},size={210.00,302.00},font="Helvetica"
	ListBox RfWaves,fSize=18,listWave=root:Packages:LaserUPS:waveList_Rf,mode= 2
	ListBox RfWaves,selRow= 11
	Button button0,pos={20.00,432.00},size={100.00,50.00}
	Button button0,Proc=DonePanelButton,title="Close"
	Button button0,font="Helvetica",fSize=18
	Button CalcButton,pos={348.00,434.00},size={100.00,50.00}
	Button CalcButton,Proc=Ab_Calc,title="Calc."
	Button CalcButton,font="Helvetica",fSize=18,fStyle=1
	Button CalcButton,valueColor=(65535,16385,16385)
	CheckBox UseSm_1,pos={267.00,386.00},size={174.00,19.00}
	CheckBox UseSm,Proc=ScSm_check,title="Only \"ScSm_*\" files"
	CheckBox UseSm_1,font="Helvetica",fSize=18,value= isScSm1
	CheckBox UseSm_2,pos={33.00,388.00},size={174.00,19.00}
	CheckBox UseSm_2,Proc=ScSm_check,title="Only \"ScSm_*\" files"
	CheckBox UseSm_2,font="Helvetica",fSize=18,value= isScSm2
End

Function ScSm_check(ctrlName, checked): CheckboxControl
	String ctrlName
	Variable checked
	String wList
	Variable numWaves, i
	NVAR g_isScSm1 = root:Packages:LaserUPS:g_isScSm1
	NVAR g_isScSm2 = root:Packages:LaserUPS:g_isScSm2
	strswitch (ctrlName)
		case "UseSm_1": // Sm
			if (checked==1)
				wList = waves2list(2)
				g_isScSm1 = 2
			else
				wList = waves2list(0)
				g_isScSm1 = 0
			endif
			numWaves = ItemsInList(wList)
			Make /T/O/N=(numWaves) root:Packages:LaserUPS:waveList_Sm
			Wave /T wave_Sm = root:Packages:LaserUPS:waveList_Sm
			Make /O/N=(numWaves) root:Packages:LaserUPS:waveList_Sm_sel
			Wave wave_Sm_sel = root:Packages:LaserUPS:waveList_Sm_sel
			for (i=0; i<numWaves; i+=1)
				wave_Sm[i] = StringFromList(i, wList)
				wave_Sm_sel[i] = 0
			endfor
			break
		case "UseSm_2": // Rf
			if (checked==1)
				wList = waves2list(2)
				g_isScSm2 = 2
			else
				wList = waves2list(0)
				g_isScSm2 = 0
			endif
			numWaves = ItemsInList(wList)
			numWaves = ItemsInList(wList)
			Make /T/O/N=(numWaves) root:Packages:LaserUPS:waveList_Rf
			Wave /T wave_Rf = root:Packages:LaserUPS:waveList_Rf
			for (i=0; i<numWaves; i+=1)
				wave_Rf[i] = StringFromList(i, wList)
			endfor
			break
	endswitch
End

Function Ab_Calc(B_Struct): ButtonControl
	STRUCT WMButtonAction &B_Struct
	if (B_Struct.eventCode ==2)
		String curWinName = B_Struct.win
		ControlInfo RfWaves
		Print V_Flag, V_Value
		Wave /T wave_Sm = root:Packages:LaserUPS:waveList_Sm
		Wave wave_Sm_sel = root:Packages:LaserUPS:waveList_Sm_sel
		Wave /T wave_Rf = root:Packages:LaserUPS:waveList_Rf
		String RefWaveName
		RefWaveName = wave_Rf[V_Value]
		Wave theRef = $RefWaveName
		String dfSave = GetDataFolder(1)
		WaveStats/Q wave_Sm_sel
		String SelectedWaveName = ""
		String Ab_wName
		Variable i
		String notes
		for (i=0; i<V_npnts; i+=1)
			if (wave_Sm_sel[i]==1)
				SelectedWaveName = AddListItem(wave_Sm[i], SelectedWavename, ";")
				Ab_wName = "Ab_" + wave_Sm[i]
				Duplicate/O $wave_Sm[i], $Ab_wName
				Wave AbWave = $Ab_wName
				Wave SigWave = $wave_Sm[i]
				AbWave = -log(SigWave/theRef)
				notes = note(AbWave)
				notes = AddListItem("Ref="+RefWaveName, notes, ";")
				note /K AbWave, notes
			endif
		endfor
	endif
End

Function IRAC_Calc_Panel_Hook(infostr)
	String infostr
	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	GetWindow $win, wsize
	//
	Variable width = 479
	Variable height = 497
	Variable statusCode = 0
	strswitch(event)
		case "resize":
			GetWindow $win, wsize
			Variable res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
			MoveWindow /W=$win V_Left, V_Top, V_left+width/res_ratio, V_Top+height/res_ratio
			break;
	endswitch
End
