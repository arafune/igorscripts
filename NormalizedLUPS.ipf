#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVertsion = 4.0
#pragma ModuleName = NLPES
//Version $Id: NormalizedLUPS.ipf 406 2006-07-27 02:04:23Z arafune $
// Version 3

#include "eachWave2"
#include "RA_Common"

//Menu "Macros"
//	"Laser UPS Normalization", DisplayLUPS_NormalizePanel()
//End

Menu "LaserUPS"
	"Laser UPS Normalization", DisplayLUPS_NormalizePanel()
	//	"Lock-in PEELS Calibraion", DisplayLockinPEELSCalibPanel()
	"Add labels for Laser UPS", AddLabelsLaserUPS()
End

Static Function  RA_LaserUPSNormalizedPanel()   :Panel
	PauseUpdate; Silent 1
	NewPanel /K=1  /W=(60,60,460,460) as "Normalized Laser UP Spectra"
	DoWindow /F/C LaserUPSNormalizedPanel

	SetWindow LaserUPSNormalizedPanel, hook = NLPES#LaserUPSNormalizedPanelHook
	GetWindow LaserUPSNormalizedPanel, wsize
	Variable res_ratio
	res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
	Variable PHeight
	Variable PWidth
	PHeight = V_Bottom - V_top
	PWidth = V_right - V_left
	//
	ListBox WaveSelectorList, frame = 2
	ListBox WaveSelectorList, pos = {(PWidth*0.04)*res_ratio, (PHeight*0.1)*res_ratio}
	ListBox WaveSelectorList, size = {(PWidth*0.5)*res_ratio, (PHeight*0.75)*res_ratio}
	MakeListIntoWaveSelector("LaserUPSNormalizedPanel", "WaveSelectorList", content=WMWS_Waves)
	//
	SetVariable setVarE size = {150,20},title="Energy(eV):", fsize=14, limits={0,Inf,0.01};DelayUpdate
	SetVariable setVarE pos = {(PWidth*0.56)*res_ratio, (PHeight*0.1)*res_ratio}
	SetVariable setVarE value = root:Packages:LaserUPS:gLenergy
	//
	SetVariable setVarP size = {150,20},title="Power(mW):", fsize=14, limits={0,Inf,0.01};DelayUpdate
	SetVariable setVarP pos = {(PWidth*0.56)*res_ratio, (PHeight*0.15)*res_ratio}
	SetVariable setVarP value = root:Packages:LaserUPS:gLPower
	//
	SetVariable setEmissionOrder size = {150,20}, title = "Emission Order:", fsize=14, limits={1,Inf,1};DelayUpdate
	SetVariable setEmissionOrder pos = {(PWidth*0.56)*res_ratio, (PHeight*0.20)*res_ratio}
	SetVariable setEmissionOrder value = root:Packages:LaserUPS:gEmissionOrder
	//
	Button normalize, win = LaserUPSNormalizedPanel
	Button normalize, pos = {(PWidth*0.65)*res_ratio, (PHeight*0.72)*res_ratio}
	Button normalize, proc = NLPES#NormalizationButton, title = "Normalization"
	Button normalize, size = {80*res_ratio, 20*res_ratio}, labelBack=(0,0,1)
	//
	Button correctButton, win = LaserUPSNormalizedPanel
	Button correctButton, pos = {(PWidth*0.65)*res_ratio, (PHeight*0.45)*res_ratio}
	Button correctButton fColor = (30464,30464,30464)
	Button correctButton, proc = NLPES#NormalizationButton, title = "Correct"
	Button correctButton, size = {80*res_ratio, 20*res_ratio}, labelBack = (0,1,1)
	//
	Button revertButton, win = LaserUPSNormalizedPanel, title = "Revert!!"
	Button revertButton, pos = {(PWidth*0.65)*res_ratio, (PHeight*0.80)*res_ratio}
	Button revertButton, proc = NLPES#NormalizationButton
	Button revertButton, size = {80*res_ratio, 20*res_ratio},labelBack=(0,0,1)
	//
	Button closeButton, win = LaserUPSNormalizedPanel
	Button closeButton, pos = {(PWidth*0.85)*res_ratio, (PHeight*0.90)*res_ratio}
	Button closeButton, proc = DonePanelButton, title="Close"
	Button closeButton, size = {40*res_ratio,20*res_ratio}
	//
	CheckBox FromTarget, win = LaserUPSNormalizedPanel, title = "From Top of Graph"
	CheckBox FromTarget, pos = {(PWidth*0.05)*res_ratio, (PHeight*0.92)*res_ratio}
	CheckBox FromTarget, proc = CheckProc
End

static Function NormalizationButton(ba): ButtonControl
	STRUCT WMButtonAction &ba
	//
	NVar gLPower = root:Packages:LaserUPS:gLPower
	NVar gLEnergy = root:Packages:LaserUPS:gLEnergy
	NVar gEmissionOrder = root:Packages:LaserUPS:gEmissionOrder
	//
	//  Common
	//
	String wnLists = WS_SelectedObjectsList("LaserUPSNormalizedPanel", "WaveSelectorList")
	Variable i
	string theWave_name
	Variable numSelection
	numSelection = ItemsInList(wnLists)
	String wavenote
	Variable energy = NaN
	Variable correff =1
	String unitstr
	//
	switch(ba.eventCode)
		case 2: // mouse up (after mouse down)
			strswitch (ba.ctrlName)
				case  "normalize":
					if (gLEnergy == 0 && gLPower ==0)
						DoAlert 0, "Both values are zero \nPlease check !!"
						return 0
					endif
					for (i=0; i < numSelection ; i += 1)
						theWave_name = StringFromList(i, wnLists)
						wave theWave = $theWave_name
						revertLPES(theWave_name)
						unitstr = ""
						wavenote = note(theWave)
						// EmissionOrder
						wavenote = AddListItem("EmissionOrder:"+num2istr(gEmissionOrder), wavenote)
						//Power correction
						If (gLPower > 0)
							wavenote = AddListItem("Power:"+ num2str(gLPower), wavenote)
							correff = gLPower^gEmissionOrder
							If (gEmissionOrder > 1)
								unitstr = "/mW\S"+num2istr(gEmissionOrder)+"\M"
							else
								unitstr = "/mW"
							endif
						else
							correff = 1
						endif
						// Photon energy correction
						If (gLEnergy > 0)
							wavenote = AddListItem("PhotonEnergy:"+ num2str(gLEnergy), wavenote)
							correff /=  (gLEnergy^gEmissionOrder)
							If (gEmissionOrder > 1)
								unitstr = " eV\S"+num2istr(gEmissionOrder)+"\M"+unitstr
							else
								unitstr = " eV/mW"
							endif
						else
							correff *= 1
						endif
						// Correct the wave and add (rewrite) the note
						theWave /= correff
						Note /K theWave
						Note theWave, wavenote
						unitstr = "cps"+unitstr
						SetScale d 0,0, unitstr, theWave
						Print "Normalized :", theWave_name , "by Photon Energy: ", gLEnergy, "and  Laser Power: ", gLPower, "with Order: " , gEmissionOrder
					endfor
					break
				case "correctButton":
					for (i=0; i< numSelection; i+=1)
						theWave_name = StringFromList(i,wnLists)
						wave theWave = $theWave_name
						wavenote = note(theWave)
						energy = NumberByKey("Energy", note(theWave))
						if (numtype(energy) == 0)
							theWave *= energy*energy
							wavenote = RemoveByKey("Energy", wavenote)
							wavenote = ReplaceNumberByKey("PhotonEnergy", wavenote, energy)
							note /k theWave
							note theWave, wavenote
							SetScale d 0,0,"cps eV/mW", theWave
							Print "Correct Normalization :", theWave_name
						else
							Print "No need to correct:", theWave_name
						endif
					endfor
					break
				case "revertButton":
					for (i = 0 ; i < numSelection ; i += 1)
						theWave_name = StringFromList(i, wnLists)
						revertLPES(theWave_name)
					endfor
					break
			endswitch
			break
	endswitch
End




Function revertLPES(theWaveName)
	String theWaveName
	//
	wave theWave = $theWaveName
	String wavenote = note(theWave)
	Variable EmissionOrder = 1
	Variable PhotonEnergy = 1
	Variable Power = 1
	Variable tmp
	tmp = NumberByKey("EmissionOrder", wavenote)
	if (numtype(tmp) == 0)
		EmissionOrder = tmp
		wavenote = RemoveByKey("EmissionOrder", wavenote)
	endif
	tmp = NumberByKey("PhotonEnergy", wavenote)
	if (numtype(tmp) == 0)
		PhotonEnergy = tmp
		wavenote = RemoveByKey("PhotonEnergy", wavenote)
	endif
	tmp = NumberByKey("Power", wavenote)
	if (numtype(tmp) == 0)
		Power = tmp
		wavenote = RemoveByKey("Power", wavenote)
	endif
	Variable correff = (Power^EmissionOrder)/(PhotonEnergy^EmissionOrder)
	theWave *= correff
	note /k theWave
	note theWave, wavenote
	SetScale d 0,0,"cps", theWave
End



static Function LaserUPSNormalizedPanelHook (infostr)
	String infoStr
	//
	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	Variable statusCode = 0 // 0 if nothing done, else 1
	//
	strswitch(event)
		case "resize":
			GetWindow $win, wsize
			Variable res_ratio
			res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
			Variable PHeight
			Variable PWidth
			PHeight = V_Bottom - V_top
			PWidth = V_right - V_left
			if (PWidth < 300)
				MoveWindow /W=$win V_Left, V_Top, V_Left+300, V_Top+PHeight
				PWidth = 300
			endif
			if (PHeight < 300)
				MoveWindow /W=$win V_Left, V_Top, V_Left+PWidth, V_Top + 300
				PHeight = 300
			endif
			ListBox WaveSelectorList, win = $win ,size = {(PWidth*0.5)*res_ratio, (PHeight*0.75)*res_ratio}
			SetVariable setVarE  win = $win, pos = {(PWidth*0.6)*res_ratio, (PHeight*0.1)*res_ratio}
			SetVariable setVarP  win = $win, pos = {(PWidth*0.6)*res_ratio, (PHeight*0.15)*res_ratio}
			SetVariable setEmissionOrder pos = {(PWidth*0.6)*res_ratio, (PHeight*0.20)*res_ratio}
			Button normalize,  win = $win, pos = {(PWidth*0.65)*res_ratio, (PHeight*0.72)*res_ratio}
			Button correctButton, win = $win, pos = {(PWidth*0.65)*res_ratio, (PHeight*0.45)*res_ratio}
			Button revertButton, win = $win, pos = {(PWidth*0.65)*res_ratio, (PHeight*0.80)*res_ratio}
			Button closeButton, win = $win, pos = {(PWidth*0.85)*res_ratio, (PHeight*0.90)*res_ratio}
			CheckBox FromTarget, win = $win, pos = {(PWidth*0.05)*res_ratio, (PHeight*0.92)*res_ratio}
			break;
	endswitch
End


Function DisplayLUPS_NormalizePanel()
	//If the panel is already created, just bring it to the front
	DoWindow/F LaserUPSNormalizedPanel
	if (V_Flag != 0)
		return 0
	endif
	//
	String dfSave = GetDataFolder(1)  //GetDataFolder returns a string containing the full path to the current data folder
	NewDataFolder/O/S root:Packages
	NewDataFolder/O/S root:Packages:LaserUPS
	//
	setDataFolder root:Packages:LaserUPS
	//Create global variables used by LaserUPSNormalizedPanel
	Variable Lpower = NumVarOrDefault(":gLpower", 0)
	Variable/G gLpower = Lpower
	Variable Lenergy = NumVarOrDefault(":gLenergy",0)
	Variable/G gLenergy = Lenergy
	Variable EmissionOrder = NumVarOrDefault(":gEmissionOrder", 1)
	Variable /G gEmissionOrder = EmissionOrder
	//
	SetDataFolder dfSave // Return the current Folder
	RA_LaserUPSNormalizedPanel()
End


Function AddLabelsLaserUPS()
	PauseUpdate
	Label left "Photoelectron intensity  ( \\u ) "
	Label bottom "Energy relative to E\\BF \\M  ( \\U )"
	ModifyGraph LowTrip(bottom) =1
	ResumeUpdate
End



