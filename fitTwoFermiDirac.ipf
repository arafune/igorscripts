#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 8.0
// Version $Id: fitTwoFermiDirac.ipf 256 2005-03-10 15:54:58Z arafune $

#include "LaserPEAnalysis"
#include "waveSelection"
#include "RA_common"

Menu "LaserUPS"
	"Two Fermi-Dirac Curves", DispfitTwoFDPanel()
End

Function TwoFDVac(x, EVac, EF1, T1, Int1, a1, EF2, T2, Int2, a2)
	Variable x, EVac, EF1, T1, Int1, a1, EF2, T2, Int2, a2
	If (x < Evac)
		return 0
	else
		return (a1* x + 1-a1*EF1)*Int1/(exp((x-Ef1)/kb_eV/T1)+1)+ (a2* x + 1-a2*EF2)*Int2/(exp((x-Ef2)/kb_eV/T2)+1)
	endif
End

Function DispfitTwoFDPanel()
	DoWindow /F fitTwoFDPanel
	if (V_Flag !=0)
		return 0
	endif
	//
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	Variable /G root:Packages:LaserUPS:g_isFromTarget
	SetDataFolder dfSave

	String wList
	NVar g_isFromTarget = root:Packages:LaserUPS:g_isFromTarget
	Variable isTarget = g_isFromTarget
	wList = waves2list(isTarget)
	Variable numWaves = ItemsInList(wList)
	//
	SetDataFolder root:Packages:LaserUPS
	//	Textwave which contains the wave name
	Make /T/O/N=(numWaves) root:Packages:LaserUPS:waveList1
	Wave /T wList1 = root:Packages:LaserUPS:waveList1
	//
	Variable i
	for (i = 0; i <numWaves ; i+= 1)
		wList1[i] = StringFromList(i,wList)
	endfor
	//
	Variable resol = NumVarOrDefault(":g_resol", 4)
	Variable /G g_resol = resol
	Variable T1 = NumVarOrDefault(":g_T1", 300)
	Variable /G g_T1 = T1
	Variable T2 = NumVarOrDefault(":g_T2", 300)
	Variable /G g_T2 = T2
	Variable EVac = NumVarOrDefault(":g_EVac", 4)
	Variable /G g_EVac = EVac
	Variable EF1 = NumVarOrDefault(":g_EF1",5)
	Variable /G g_EF1 = EF1
	Variable EF2 = NumVarOrDefault(":g_EF2",4.5)
	Variable /G g_EF2 = EF2
	Variable Int1 = NumVarOrDefault(":g_Int1",10000 )
	Variable /G g_Int1 = Int1
	Variable Int2 = NumVarOrDefault(":g_Int2",10000 )
	Variable /G g_Int2 = Int2
	Variable Coeffa1 = NumVarOrDefault(":g_Coeffa1",0 )
	Variable /G g_Coeffa1 = Coeffa1
	Variable Coeffa2 = NumVarOrDefault(":g_Coeffa2",0 )
	Variable /G g_Coeffa2 = Coeffa2
	//
	SetDataFolder dfSave
	RA_fitTwoFDPanel()
End

Function RA_fitTwoFDPanel()
	PauseUpdate; Silent 1
	//
	PauseUpdate; Silent 1		// building window...
	NewPanel /K=1 /W=(11,50,615,458) as "Fit Two Fermi Dirac Curve"
	ModifyPanel cbRGB=(51456,44032,58880), frameStyle=2
	DoWindow /F/C fitTwoFDPanel
	//
	///	SetDataFolder root:Packages:LaserUPS

	NVar g_EF1=root:Packages:LaserUPS:g_EF1
	NVar g_EF2=root:Packages:LaserUPS:g_EF2
	SetWindow fitTwoFDPanel, hook = fitTwoFDPanelHook
	GetWindow fitTwoFDPanel, wsize
	//
	SetDrawLayer UserBack
	SetDrawEnv linefgc= (13056,0,26112),fillfgc= (13056,0,26112)
	DrawRect 2,2,600,46
	SetDrawEnv linefgc= (49152,65280,32768),rounding= 25,fillfgc= (49152,65280,32768)
	DrawRRect 6,127,248,240
	SetDrawEnv fsize= 24,textrgb= (65535,65535,65535)
	DrawText 28,34,"Fitting Panel (Photoelectron Energy Loss Spectra)"
	SetDrawEnv fsize= 16
	DrawText 48,147,"FermiDirac Curve (1)"
	SetDrawEnv linefgc= (32768,54528,65280),fillfgc= (32768,54528,65280)
	DrawRRect 8,57,240,116
	SetDrawEnv linefgc= (65280,65280,32768),rounding= 25,fillfgc= (65280,65280,32768)
	DrawRRect 6,244,248,397
	SetDrawEnv fsize= 16
	DrawText 52,267,"FermiDirac Curve (2)"
	SetDrawEnv fsize= 16
	DrawText 319,80,"Select a wave"
	SetDrawEnv fsize= 14
	DrawText 498,129,"From Target"
	//
	SetVariable DeltaE,pos={32,65},size={171,21},title="Resol.(meV)"
	SetVariable DeltaE,labelBack=(32768,54528,65280),font="Arial",fSize=16
	SetVariable DeltaE,limits={0,inf,0.1},value= root:Packages:LaserUPS:g_resol,bodyWidth= 80
	//
	SetVariable EVac,pos={55,91},size={148,21},title="EVac (V)"
	SetVariable EVac,labelBack=(32768,54528,65280),font="Arial",fSize=16
	SetVariable EVac,limits={-inf,inf,0.001},value= root:Packages:LaserUPS:g_EVac,bodyWidth= 80
	//
	SetVariable Int1,pos={42,148},size={158,21},title="Intensity(1)"
	SetVariable Int1,labelBack=(49152,65280,32768),font="Arial",fSize=16
	SetVariable Int1,limits={0,inf,100},value= root:Packages:LaserUPS:g_Int1,bodyWidth= 80
	//
	SetVariable EF1,pos={29,170},size={171,21},title="EF_(1)  (eV)"
	SetVariable EF1,labelBack=(49152,65280,32768),font="Arial",fSize=16
	SetVariable EF1,limits={-inf,inf,0.0005},value= root:Packages:LaserUPS:g_EF1,bodyWidth= 80
	//
	SetVariable T1,pos={63,192},size={137,21},title="T(1) (K)"
	SetVariable T1,labelBack=(49152,65280,32768),font="Arial",fSize=16
	SetVariable T1,limits={0,inf,1},value= root:Packages:LaserUPS:g_T1,bodyWidth= 80
	//
	SetVariable Int2,pos={42,272},size={158,21},title="Intensity(2)"
	SetVariable Int2,labelBack=(65280,65280,32768),font="Arial",fSize=16
	SetVariable Int2,limits={0,inf,100},value= root:Packages:LaserUPS:g_Int2,bodyWidth= 80
	//
	SetVariable EF2,pos={33,296},size={167,21},title="EF_(2) (eV)"
	SetVariable EF2,labelBack=(65280,65280,32768),font="Arial",fSize=16
	SetVariable EF2,limits={-inf,inf,0.0005},value= root:Packages:LaserUPS:g_EF2,bodyWidth= 80
	//
	SetVariable T2,pos={63,346},size={137,21},title="T(2) (K)"
	SetVariable T2,labelBack=(65280,65280,32768),font="Arial",fSize=16
	SetVariable T2,value= root:Packages:LaserUPS:g_T2,bodyWidth= 80
	//
	ValDisplay loss,pos={36,321},size={165,20},title="Loss (meV)"
	ValDisplay loss,labelBack=(65280,65280,32768),font="Arial",fSize=16
	ValDisplay loss,limits={0,0,0},barmisc={0,1000},bodyWidth= 80
	ValDisplay loss,value= #"(root:Packages:LaserUPS:g_EF1-root:Packages:LaserUPS:g_EF2)*1000"
	//
	ListBox list0,pos={253,83},size={229,199},font="Arial",fSize=16,frame=2
	ListBox list0,listWave=root:Packages:LaserUPS:waveList1,mode= 1,selRow= 0
	//
	CheckBox fromTgt,pos={499,94},size={16,14},proc=FromTargetCheck,title=""
	CheckBox fromTgt,value= 1
	//
	Button ApndGraph,pos={265,298},size={150,40},proc=AppendTwoFDFit,title="Append To Graph"
	Button ApndGraph,font="Arial",fSize=16
	//
	Button StrResult,pos={443,298},size={150,40},proc=StorTwoFDFit,title="Store Results"
	Button StrResult,font="Arial",fSize=16
	//
	Button Analysis,pos={492,232},size={100,51},proc=RA_fitTwoFD,title="Analysis"
	Button Analysis,labelBack=(65280,43520,0),font="Arial",fSize=16
	Button Analysis,fColor=(65280,43520,0)
	//
	Button GetFitParm,pos={491,161},size={100,51},proc=GetTwoFDFitParams,title="Get Params"
	Button GetFitParm,labelBack=(65280,16384,16384),font="Arial",fSize=16
	Button GetFitParm,fColor=(65280,16384,16384)
	//
	Button DoneButton,pos={488,358},size={100,40},proc=DonePanelButton,title="Done"
	Button DoneButton,font="Arial",fSize=16
	//
	SetVariable Coeffient1,pos={30,214},size={170,21},title="Coefficient(1)"
	SetVariable Coeffient1,labelBack=(49152,65280,32768),font="Arial",fSize=16
	SetVariable Coeffient1,value= root:Packages:LaserUPS:g_Coeffa1,bodyWidth= 80
	SetVariable Coeffient1 limits={-inf,inf,0.01}
	//
	SetVariable Coeffient2,pos={34,371},size={166,21},title="Coefficient(2)"
	SetVariable Coeffient2,labelBack=(65280,65280,32768),font="Arial",fSize=16
	SetVariable Coeffient2,value= root:Packages:LaserUPS:g_Coeffa2,bodyWidth= 80
	SetVariable Coeffient2 limits={-inf,inf,0.01}
	//
	Button RemoveTmpWaves,pos={265,354},size={150,40},proc=RemoveFDcurvs,title="Remove tmp waves"
	Button RemoveTmpWaves,font="Arial",fSize=16
End

Function RA_fitTwoFD(ctrlName):ButtonControl
	String ctrlName
	ControlInfo /W=fitTwoFDPanel list0
	Wave /T wList1 = root:Packages:LaserUPS:waveList1
	String waveNamebase
	waveNamebase= wList1[V_Value]
	//	Analyze experimental wave.
	Variable delx
	delx = deltax($waveNameBase)
	Variable npnts
	npnts = numpnts($waveNameBase)
	Variable left_x
	left_x = leftx($waveNameBase)
	//	gauss curve
	String gaussWaveforFitFullPath
	gaussWaveforFitFullPath = "root:Packages:LaserUPS:gausscurve"
	Make /O/N=600 $gaussWaveforFitFullPath
	Wave gausswave = $gaussWaveforFitFullPath
	SetScale/P x -delx * 300 , delx ,"", gausswave
	SetFormula root:Packages:LaserUPS:gausscurve , "instrument(x, g_resol)"
	// Diff
	String DiffFullPath = "root:Packages:LaserUPS:Diff"
	Make /O/N=(npnts) $DiffFullPath
	Wave Diff = $DiffFullPath
	SetScale /P x left_x, delx, "eV", Diff
	SetFormula root:Packages:LaserUPS:Diff, GetWavesDataFolder($waveNameBase,2) +"-(g_Coeffa1*x+1-g_Coeffa1*g_EF1)*g_Int1*FDVac(x, g_EVac,g_Ef1,g_T1)"
	// Fermi-Dirac Curve	(2)
	String FD2CurveFullPath = "root:Packages:LaserUPS:FDCurve2"
	Make /O/N=(npnts) $FD2CurveFullPath
	Wave FD2Curve = $FD2CurveFullPath
	SetScale /P x left_x, delx, "eV", FD2Curve
	SetFormula root:Packages:LaserUPS:FDCurve2, "(g_Coeffa2*x+1-g_Coeffa2*g_EF2)*g_Int2*FDVac(x, g_EVac,g_Ef2,g_T2)"
	// Two Fermi-Dirac
	String TwoFDFullPath = "root:Packages:LaserUPS:TwoFD"
	Make /O/N=(npnts) $TwoFDFullPath
	Wave TwoFD = $TwoFDFullPath
	SetScale /P x left_x, delx, "eV", TwoFD
	SetFormula root:Packages:LaserUPS:TwoFD, "TwoFDVac(x, g_EVac,g_EF1,g_T1,g_Int1,g_Coeffa1,g_EF2,g_T2,g_Int2,g_Coeffa2)"
	String TwoFDConvFullPath = "root:Packages:LaserUPS:TwoFDfit"
	Make /O/N=(npnts) $TwoFDConvFullPath
	Wave TwoFDFit = $TwoFDConvFullPath
	SetScale /P x left_x, delx, "eV", TwoFDFit
	SetFormula root:Packages:LaserUPS:TwoFDfit, "conv2(p, gausscurve, TwoFD)"
End

Function AppendTwoFDFit(ctrlName): ButtonControl
	String ctrlName
	String curWinName
	curWinName = WinName(0,1)
	GetWindow $curWinName, wavelist
	Variable i
	Wave/T W_WaveList = W_WaveList
	Variable isExistOntheCurrentWindow = 0
	// Check Diff
	for (i =0; i<DimSize(W_WaveList,0);i+=1)
		if (stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:Diff") == 1)
			isExistOnTheCurrentWindow = 1
		endif
	endfor
	if (isExistOnTheCurrentWindow != 1)
		AppendToGraph root:Packages:LaserUPS:Diff
		Print "Append root:Packages:LaserUPS:DiffonvCurve on the Graph"
	endif
	//	Check FDCurve2
	isExistOntheCurrentWindow = 0
	for (i =0; i<DimSize(W_WaveList,0);i+=1)
		if (stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:FDCurve2") == 1)
			isExistOnTheCurrentWindow = 1
		endif
	endfor
	if (isExistOnTheCurrentWindow != 1)
		AppendToGraph root:Packages:LaserUPS:FDCurve2
		Print "Append root:Packages:LaserUPS:FDCurve2 on the Graph"
	endif
	//
	isExistOntheCurrentWindow = 0
	for (i =0; i<DimSize(W_WaveList,0);i+=1)
		if (stringmatch(W_WaveList[i][1], "*Packages:LaserUPS:TwoFDfit") == 1)
			isExistOnTheCurrentWindow = 1
		endif
	endfor
	if (isExistOnTheCurrentWindow != 1)
		AppendToGraph root:Packages:LaserUPS:TwoFDfit
		Print "Append root:Packages:LaserUPS:TwoFDfit on the Graph"
	endif
	//
	Killwaves W_WaveList
End

Function fitTwoFDPanelHook(infostr)
	String infoStr
	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	Variable statusCode = 0
	strswitch(event)
		case "resize":
			GetWindow $win, wsize
			Variable res_ratio
			res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
			MoveWindow /W=$win V_Left, V_Top, V_left+604/ res_ratio, V_Top+408/res_ratio
			break;
	endswitch
End



Function StorTwoFDFit(ctrlName): ButtonControl
	String ctrlName
	ControlInfo /W=fitTwoFDPanel	 list0
	Wave /T wList1 = root:Packages:LaserUPS:waveList1
	String waveNamebase
	waveNamebase= wList1[V_Value]
	//
	if (waveExists(root:Packages:LaserUPS:TwoFDfit))
		String fit_result
		fit_result = "FD2_"+waveNameBase
		//
		Duplicate /O root:Packages:LaserUPS:TwoFDfit, $fit_result
		Wave fitWave = $fit_result
		Wave convWave = root:Packages:LaserUPS:TwoFDfit
		fitWave = convWave
		//
		String fittingparameters
		NVar g_Int1 = root:Packages:LaserUPS:g_Int1
		NVar g_Ef1 = root:Packages:LaserUPS:g_EF1
		NVar g_T1 = root:Packages:LaserUPS:g_T1
		NVar g_Int2 = root:Packages:LaserUPS:g_Int2
		NVar g_EF2 = root:Packages:LaserUPS:g_EF2
		NVar g_T2 = root:Packages:LaserUPS:g_T2
		NVar g_EVac =root:Packages:LaserUPS:g_EVac
		NVar g_resol = root:Packages:LaserUPS:g_resol
		NVar g_Coeffa1 = root:Packages:LaserUPS:g_Coeffa1
		NVar g_Coeffa2 = root:Packages:LaserUPS:g_Coeffa2
		fittingparameters = "fit_Intensity1:"+num2str(g_Int1)+";fit_Ef1:"+num2str(g_Ef1)+";fit_T1:"+num2str(g_T1)+";"+num2Char(13)
		fittingparameters += ";fit_Intensity2:"+num2str(g_Int2)+";fit_Ef2:"+num2str(g_Ef2)+";fit_T2:"+num2str(g_T2)+";"+num2char(13)
		fittingparameters += ";fit_EVac:"+num2str(g_EVac)+";fit_resolution:"+num2str(g_resol)+";"+num2char(13)
		fittingparameters += ";Coeffa1:"+num2str(g_Coeffa1)+";Coeffa2:"+num2str(g_Coeffa2)+";"+num2char(13)
		note fitWave, fittingparameters
		Print "Store the fitting result for the wave : "+ waveNamebase
	else
		String stmp = "Use \"Analyze this spectrum\", first"
		Abort stmp
	Endif
End


Function GetTwoFDFitParms(ctrlName): ButtonControl
	String ctrlName
	ControlInfo /W=fitTwoFDPanel	 list0
	Wave /T wList1 = root:Packages:LaserUPS:waveList1
	String waveNamebase
	waveNamebase= wList1[V_Value]
	String FitWavname = "FD2_"+waveNamebase
	if (waveExists($FitWavname))
		wave FitWave = $FitWavname
		String tmpNote = note(FitWave)
		NVar g_Int1 = root:Packages:LaserUPS:g_Int1
		g_Int1 = NumberByKey("fit_Intensity1", tmpNote)
		NVar g_Int2 = root:Packages:LaserUPS:g_Int2
		g_Int2 = NumberByKey("fit_Intensity2", tmpNote)
		NVar g_EF1 = root:Packages:LaserUPS:g_EF1
		g_EF1 = NumberByKey("fit_EF1", tmpNote)
		NVar g_EF2 = root:Packages:LaserUPS:g_EF2
		g_EF2 = NumberByKey("fit_EF2", tmpNote)
		NVar g_T1 = root:Packages:LaserUPS:g_T1
		g_T1 = NumberByKey("fit_T1", tmpNote)
		NVar g_T2 = root:Packages:LaserUPS:g_T2
		g_T2 = NumberByKey("fit_T2", tmpNote)
		NVar g_EVac = root:Packages:LaserUPS:g_EVac
		g_EVac = NumberByKey("fit_EVac", tmpNote)
		NVar g_resol = root:Packages:LaserUPS:g_resol
		g_resol = NumberByKey("fit_resolution", tmpNote)
		NVar g_Coeffa1 = root:Packages:LaserUPS:g_Coeffa1
		g_Coeffa1 = NumberByKey("Coeffa1", tmpNote)
		NVar g_Coeffa2 = root:Packages:LaserUPS:g_Coeffa2
		g_Coeffa2 = NumberByKey("Coeffa2", tmpNote)
	else
		String smtp = "Can't get the fitting parameters.\n "+ "The wave : FD2_"+waveNamebase+" does not exist"
		Abort smtp
	endif
End

Function RemoveFDcurvs(ctrlName) : ButtonControl
	String ctrlName
	if (WaveExists(root:Packages:LaserUPS:Diff))
		RemoveFromGraph /Z Diff
	endif
	if (WaveExists(root:Packages:LaserUPS:FDCurve2))
		RemoveFromGraph /Z 	FDCurve2
	endif
	if (WaveExists(root:Packages:LaserUPS:TwoFDFit))
		RemoveFromGraph /Z 	TwoFDFit
	endif
	Killwaves /Z root:Packages:LaserUPS:Diff , root:Packages:LaserUPS:TwoFDCurve2, root:Packages:LaserUPS:TwoFDFit
	Killwaves /Z root:Packages:LaserUPS:gausscurve
End
