#pragma rtGlobals=1		// Use modern global access method.
#pragma ModuleName=drawmultipeaks

Function gaussians(paramWave)
	Wave paramWave
	Variable i = 0
	// paramWave is 2D (matrix) wave.  The number of the raw is free, it equals the number of the peaks for considering.
	// The number of the column must be 3.  The first column is the energy position of the peak, the second columns the intensity of the peak, the third column the width of the peak.  
	// Such wave can be build by the following command like:
	// Make/N=(0,3)/D paramWave  
	// 
	Variable size = DimSize(paramWave,0)
	Variable multigaussians=0
	Variable sigma
	Do
		sigma = paramWave[i][2] /2/sqrt(2*Ln(2))
		multigaussians += paramWave[i][1] * exp(-(x-paramWave[i][0])^2/2/sigma^2)
		i += 1
	While (i <  size) 
	return multigaussians
End	 
