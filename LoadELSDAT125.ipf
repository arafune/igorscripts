#pragma rtGlobals=3		// Use modern global access method.
///  Thif file is obsolute functions // Must not be used.
#include "SelectFiles"

Menu "Load Waves"
	"Load EELS_DAT125 File...", LoadEA125File("", "", 2)
	"Load EELS DAT125 Files in Dir", LoadEA125filesInDir(2)
End

/// @param pathName        path namea. "" for dialog.
/// @param filename        file name. partial path and file name of full path or "" for dialog
Function	LoadELSDAT125File(String pathName, String fileName)
	Variable err
	Variable refNum
	wave w1
	String message="Select EELS DAT125 Data file"
	Open /R/Z=2/P=$pathName/M=message refNum as fileName
	FStatus refNum
	if (refNum==0)
		Print "Select EELS DAT125 Data File (*.1).\nAborted"
		Abort "Select EELS DAT125 Data File (*.1)."
	endif
	String wn
	//
	String fnName, fnExt
	sscanf S_fileName, "%20[a-zA-Z0-9_-].%3[0-9a-zA-Z]", fnName, fnExt
	wn = fnName+"_"
	Print "File loaded:", S_fileName
	//
	Variable wnIncrement = 0
	Variable numFrame = 1
	String Buffer
	String wnWithIncrement
	Variable firstChar
	Variable initE, end_E, stepE, cycle, dwell, numData, tmp1, tmp2
	Variable intensity
	Variable numLoaded
	Variable E0 = 2
	Variable thetaIn = 60
	Variable thetaOut = 60
	do
		do
			FReadLine refNum, Buffer
			firstChar = char2num(buffer[0])
			if (strlen(buffer)==0) //End of file
				break
			endif
			if ((firstChar==char2num("\n")) || (firstChar == char2num("\r")))
				continue
			endif
			if ((firstChar==char2num(" ")) || (firstChar == char2num("\t")))
				sscanf buffer, "%g %g %g %g %g %g %g %g",  initE, end_E, stepE, cycle, dwell, numData, tmp1, tmp2
				if (numData ==0 || dwell ==0)
					Print "Empty Data in file: "+ wn
					Abort "Empty Data in file: "+ wn
				endif
				wnIncrement += 1
				if (wnIncrement <= 9)
					wnWithIncrement = wn + "0" + num2str(wnIncrement)
				else
					wnWithIncrement = wn + num2str(wnIncrement)
				endif
				//  <= Insert Dialog routine here.
				Prompt E0, "Enter E0 for FRAME: " + num2str(wnIncrement)
				Prompt thetaIn, "Enter theta_In for FRAME: " + num2str(wnIncrement)
				Prompt thetaOut, "Enter theta_Out for FRAME: " + num2str(wnIncrement)
				DoPrompt "Current File:" + S_fileName, E0, thetaIn, thetaOut
				If (V_Flag)
					Abort
				endif
				Make /O/N=(numData) $wnWithIncrement
				Wave w1 = $wnWithIncrement
				if  (Waveexists(w1)==0)
					continue
				endif
				initE /= 1000
				end_E /= 1000
				stepE /= 1000
				SetScale /P x initE, stepE, "eV", w1
				SetScale d 0,0,"cps", w1
				String comments
				comments = ";E0:" + num2str(E0) + ";thetaIn:" + num2str(thetaIn) + \
				";thetaOut:" + num2str(thetaOut) + \
				";cycle:" + num2str(cycle) + \
				";dwell:" + num2str(dwell) +";"
				Note w1, comments
				Print "Wave generation: ", wnWithIncrement
				numLoaded = 0
				continue
			endif

			if (firstChar > char2num(":"))
				String othercomments
				if (char2num(buffer[strlen(buffer)-1])==char2num("\r"))
					buffer[strlen(buffer)-1, strlen(buffer)]=""
				endif
				othercomments = ";Comments:"+buffer+";"
				Note w1, othercomments
				continue
			endif
			sscanf buffer, "%g", intensity
			If (Waveexists(w1)==0)
				Print "This is not ELS DAT125 Data file \nAborted."
				Abort "This is not ELS DAT125 Data file."
			endif
			w1[numloaded]=intensity / cycle / dwell
			numloaded += 1
		while(1)
		if (strlen(buffer)==0) // End of File
			break
		endif
	while(1)
	// if wnIncrement is "1", this meaningless 1 is obsolute.
	if (wnIncrement==1)
		wnWithIncrement[strlen(wnWithIncrement)-3, strlen(wnWithIncrement)-1]=""
		rename $(wnWithIncrement + "_01") $wnWithIncrement
	endif
	Close refNum
End

