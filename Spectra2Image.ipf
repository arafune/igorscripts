#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 5.0
// Version $Id: Spectra2Image.ipf 795 2007-08-06 17:53:43Z arafune $

Menu "LaserUPS"
	"Spectra to Image", Spectra2Image()
End

Function Spectra2Image()

//	First of all, I'll confirm the Graph window moves on top.
	String WindowName
	WindowName = WinName(0,1)
	DoWindow/F WindowName
//
	String TwoDWaveName="TwoDwave"
	String TwoDWaveXLabel = "eV"
//
	Prompt TwoDWaveName, "Type the 2D wave name (Overwrite if exists)"
	Prompt TwoDWaveXLabel, "X axis label"
	DoPrompt "Enter the wave name for the 2D wave", TwoDWaveName, TwoDWaveXLabel
	if (V_Flag)
//		return -1
		Abort "User cannceled."
	endif
	if (strlen(TwoDWaveName)==0)
		Abort "Type the wave name."
	endif
//  Get Axis information.
	GetAxis /Q bottom
	Variable axisleft, axisright
	axisleft = V_min
	axisright = V_max
	wave w= WaveRefIndexed ("",0,1)
	Variable l, dx
	l = leftx(w)
	dx=deltax(w)
	variable nump
	nump=round(abs(axisleft - axisright)/dx)
    String traceNameLst =TraceNameList("",";",1)
	Variable numTraces = ItemsInList(TraceNameLst)
	Make /O/N = (nump,numTraces) $TwoDWaveName
	Wave TwoDW = $TwoDWaveName
	SetScale /P x axisleft, dx, TwoDWaveXLabel, TwoDW
	TwoDW = Nan
//
	Variable i=0
	String curWaveName
	Variable curWaveleftx
	Variable curWaveleftp
	do
		wave curWave = WaveRefIndexed("",i,1)
		curWaveName =  GetWavesDataFolder(curWave, 2)
		if (leftx(curWave) > axisleft)
			curWaveleftp = 0
			l = leftx(curWave)
			TwoDW[round((l-axisleft)/dx),round((l-axisleft)/dx)+numpnts(curwave)-1][i] = curWave[p-round((l-axisleft)/dx)]  // It;s corresponding point ...
		else
			curWaveleftp = x2pnt(curWave, axisleft)
			TwoDW[p,numpnts(curwave)-1][i]=curWave[p+curWaveleftp]
		endif
		i  += 1
	while (i<numTraces)
End
