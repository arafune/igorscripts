# OUPSLoader の使い方

## これはなにか？

1. Bruker の OUPS を使って測定・保存した FTIR データの Igor への読み込み。

   - バイナリファイルを直接読み込める。
   - バイナリデータ内に保存してある種々のデータ（測定日時など）を、Wave のノートとして保存。

2. 二つのシングルビーム測定から吸収スペクトルを計算する。

   - 測定の現場では、あるシングルビーム測定を「リファレンス」として取扱い、
     吸収スペクトルを表示する。一方詳しい解析のときリファレンス・スペクトルを変更し、
     よりひずみの少ない吸収スペクトルにするという作業が重要。
     いちいち Wave をコピーして、割り算して、log とって・・・ということをするのはあまりにもかったるい。
     一連の作業を自動的にこなす。リファレンス・スペクトルを瞬時に変更できるので、差を見比べたいときなどにも使える。

## Installation

1.　 Mac
~/Documents/WaveMetrics/Igor Pro 8 User Files/User Procedures
以下の（“8” は Igor のバージョンで変わるだろうけど）適当なところに

- OUPSLoader.ipf
- SelectFiles.ipf
- WaveSelection.ipf

を置く。サブディレクトリがあってもよい。

2.　 Windows

忘れた。基本は Mac と似たような手順のはず
（User Procedures ディレクトリに全ての ipf ファイルを置いて、
OUPSLoader.ipf のエイリアスを Igor Procedures ディレクトリに置く）。

## How to load OUPSLoader.ipf into Igor

- OUPSLoader.ipf を Filer→ OpenFile から選択する。
- (または）OUPSLoader.ipf のエイリアスを
  ~/Documents/WaveMetrics/Igor Pro 8 User Files/Igor Procedures
  に置いておくと、Igor の起動時に自動的に読み込まれる。

## How to use (1)（Load data files)

手順

1.1 Data → Load Waves → Load OUPS FTIR Data を選択

![OUPS_loader_1.png](imgs/OUPS_loader_1.png)

1.2 データのあるディレクトリを選ぶ（『開く』をクリック）。（ここではディレクトリを選ぶだけで、ファイルを選択するのは次のステップ）

![OUPS_loader_2.png](imgs/OUPS_loader_2.png)

1.3 そのディレクトリにある全てのファイル（ファイルだけ、サブディレクトリは表示しない）が表示されるので、読み込みたいファイルを選択（複数選択可）

![OUPS_loader_3.png](imgs/OUPS_loader_3.png)

1.4 Done ボタンをクリック。

# Wave 作成のルール

1. ファイル名を基準に自動的に Wave 名がつけられる。

例) ファイル名が 092719_1.0 だったら

- 吸収スペクトル Ab_092719_1_0
- シングルビーム スペクトル ScSm_092719_1_0
- リファレンススペクトル ScRf_092719_1_0（多くの場合このデータは結局要らない）
- ファイル末尾の .0 を \_0 に変換しているのは、Igor で Wave 名に
“.” が着いていると取扱がたまに面倒くさいときがあるため。

2. 作られる Wave についてのtips

- 読み込んだ Wave の XWave に波数の情報が書いてある。横軸（波数）の単位も記録してある。そのまま Display “Wave 名”とするとスペクトルが出る。

![OUPS_loader_4.png](imgs/OUPS_loader_4.png)

# How to use (2)（Absorption spectrum calculation)

1 手順
   1.1 Analysis → IRAS Spectrum を選ぶ。

![OUPS_loader_5.png](imgs/OUPS_loader_5.png)

   1.2 こんな感じのパネルが現れる。(多分直感的に使える。サンプルデータは複数選択できる。）

![OUPS_loader_6.png](imgs/OUPS_loader_6.png)

- リファレンスデータは一つだけ選択できる。（一つ選択しなくてはいけない）

- サンプルデータ、リファレンスデータを選択したら、”Calc.”をクリック。サンプルデータの吸収スペクトルが求められる。-log (SampleData/RefData)しているだけ。

2. 作られる吸収スペクトルのtips

- 作られる Wave の名前は元の名前を基準にして、"Ab*" がつく。なので、多くの場合、 "Ab_ScSm*" で始まる Wave 名になる。したがって、元の Ab*0927…といった Wave は使わない（ことが多い）。
- 異なるリファレンスで吸収スペクトルを作り直すとデータが新たに書き換わる。そのため、一旦 Ab_ScSm*で始まる Wave を表示しておいて、別のリファレンスを使って計算し直すとスペクトルの形が変わることがわかる。
- Wave のノートにはどのシングルビームデータを使って吸収スペクトルを作ったか、が記録される。後で見直すときに便利。
