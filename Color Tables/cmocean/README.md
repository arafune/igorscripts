Color map for igor imported from a python library, cmocean.

The code for making the color table wave:

```
Function Make_color_wave_from_cmap_folder()
	String cmap_folder = "Macintosh HD:Users:arafune:env:py311:lib:python3.11:site-packages:cmocean:rgb:"
	String destfolder = SpecialDirPath("Igor Pro User files", 0, 0, 0) + "User Procedures:SPD:Color Tables:cmocean"
	String color_list = "algae;amp;balance;curl;deep;delta;dense;diff;gray;haline;ice;matter;oxy;phase;rain;solar;speed;tarn;tempo;thermal;topo;turbid"
	Variable n= ItemsInList(color_list)
	NewPath /C/O Path1, destfolder
	Variable i
	String colorname
	for (i=0; i<n; i+=1)
		colorname = StringFromList(i, color_list)
		LoadWave/G/M/D/NAME={colorname, "", 0} cmap_folder  + colorname + "-rgb.txt"
		Wave color = $colorname
		color *= 65536
		Save /O/C/P=Path1 $colorname as colorname + ".ibw"
	endfor
End
```
