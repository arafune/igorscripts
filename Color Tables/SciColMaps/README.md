Color map for igor imported from Science color map.

Color map values are imported from the python package, cmcrameri.

The code

```
Function Make_color_wave_from_cmap_folder()
	String cmap_folder = "Macintosh HD:Users:arafune:env:py311:lib:python3.11:site-packages:cmcrameri:cmaps:"
	String destfolder = SpecialDirPath("Igor Pro User files", 0, 0, 0) + "User Procedures:SPD:Color Tables:SciColMaps"
	String color_list = "bam;batlowK;batlowW;fes;vanimo;bukavu"
	Variable n= ItemsInList(color_list)
	NewPath /O Path1, destfolder
	Print n
	Variable i
	String colorname
	for (i=0; i<n; i+=1)
		colorname = StringFromList(i, color_list)
		LoadWave/G/M/D/NAME={colorname, "", 0} cmap_folder + colorname + ".txt"
		Wave color = $colorname
		color *= 65536
		Save /O/C/P=Path1 $colorname as colorname + ".ibw"
	endfor
End
```
