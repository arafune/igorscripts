Color map for igor imported from a python library, CMasher.

The code for making the color table wave:

```
Function Make_color_wave_from_cmap_folder()
	String cmap_folder = "Macintosh HD:Users:arafune:env:py311:lib:python3.11:site-packages:cmasher:colormaps:"
	String destfolder = SpecialDirPath("Igor Pro User files", 0, 0, 0) + "User Procedures:SPD:Color Tables:CMasher"
	String color_list = "amber;amethyst;apple;arctic;bubblegum;chroma;copper;cosmic;dusk;eclipse;ember;emerald;emergency;fall;flamingo;freeze;fusion;gem;ghostlight;gothic;guppy;holly;horizon;iceburn;infinity;jungle;lavender;lilac;neon;neutral;nuclear;ocean;pepper;pride;prinsenvlag;rainforest;redshift;sapphire;savanna;seasons;seaweed;sepia;sunburst;swamp;torch;toxic;tree;tropical;viola;voltage;waterlily;watermelon;wildfire"
	Variable n= ItemsInList(color_list)
	NewPath /O Path1, destfolder
	Print n
	Variable i
	String colorname
	for (i=0; i<n; i+=1)
		colorname = StringFromList(i, color_list)
		LoadWave/G/M/D/NAME={colorname, "", 0} cmap_folder + "cm_" + colorname + ".txt"
		Wave color = $colorname
		color *= 65536
		Save /O/C/P=Path1 $colorname as colorname + ".ibw"
	endfor
End

```
