#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 4.0
// Version 2.0
#include "SelectFiles"

Menu "Load Waves"
	"Load EA125 File...", LoadEA125File("", "", 0)
	"Load EA125 Files in Dir", LoadEA125filesInDir(0)
	"Load EELS_DAT125 File...", LoadEA125File("", "", 2)
	"Load EELS DAT125 Files in Dir", LoadEA125filesInDir(2)
End

/// @param pathName Igor symbolic path name of "" for dialog
/// @param fileName Partial path and file name, or full path or "" for dialog
/// @param filemode 0 or 2 to switch parse way
Function LoadEA125File(pathName, fileName, filemode)
	String pathName
	String fileName
	Variable filemode
	//
	Variable err	, refNum
	wave w1
	String message
	switch (filemode)
		case 0:
			message = "Select EA125 Data file"
			break
		case 2:
			message = "Select EELS DAT125 Data file"
			break
		default:
			break
	endswitch
	Open /R/Z=2/P=$pathName/M=message refNum as fileName
	FStatus refNum
	if (refNum == 0)
		switch (filemode)
			case 0:
				Print "Select EA125 file!!\nAborted"
				Abort "Select EA125 file!!"
			case 2:
				Print "Select EELS DAT125 Data File (*.1).\nAborted"
				Abort "Select EELS DAT125 Data File (*.1)."
				break
			default:
				break
		endswitch
	endif
	String wn
	wn = fn2wn(S_fileName)
	Print "File loaded :", S_fileName   //ファイルのロード
	LoadEA125FileData(refNum, wn, filemode)
	Close refNum		//close file
End

/// @param refNum reference number
/// @param wn wave name
/// @param filemode
Static Function LoadEA125FileData(refNum, wn, filemode)
	Variable refNum
	String wn
	Variable filemode
	//
	Variable wnIncrement = 0
	String wnWithIncrement
	Variable numFrame =1
	String Buffer
	Variable firstChar
	Variable initE, End_E, stepE, cycle, dwell, numData, tmp1, tmp2
	Variable Intensity
	Variable numLoaded
	Variable Region10Flag = 0
	String Region10wvnm =""
	String hUnit = "eV"
	String vUnit = "cps"
	String comments = ""
	//
	switch (filemode)
		case 0:
			// Judging EA125 file type
			// EIS generated EA125 SPECTRA file contains the string
			// "Experiment Type: UPS\r" in the first line.
			// The first line of the original EA125 SPECTRA file is just "\r".
			FReadLine refNum, buffer
			if (stringmatch (buffer, "Experiment Type: UPS\r")==1)
				Variable underbarposition = strsearch(wn, "_", Inf, 1)
				Variable lengthWn = strlen(wn)
				wn[underbarposition, lengthWn-1]=""
			endif
			break
		case 2:  // EELS
			Variable E0 = 2
			Variable thetaIn = 60
			Variable thetaOut = 60
			vUnit = "cps"
			// EELS output does not require the suffix number in wave name.
			underbarposition = strsearch(wn, "_", Inf, 1)
			lengthWn = strlen(wn)
			wn[underbarposition, lengthWn-1]=""
			break
		default:
			break
	endswitch
	do
		FReadLine refNum, buffer
		firstChar = char2num(buffer[0])
		if (strlen(buffer) == 0)  // End of File
			break
		endif
		if ((firstChar==char2num("\n")) || (firstChar ==char2num("\r"))) // ここでエラーとなるとき、|| を %| に変えてみる。特にVer.3 を経験した Igor ファイルは要注意
			continue
		endif
		if ((firstChar== char2num(" ") ) || (firstChar == char2num("\t") ) )
			sscanf buffer, "%g %g %g %g %g %g %g %g", initE, End_E, stepE, cycle, dwell, numData, tmp1, tmp2
			if (numData == 0 || dwell == 0)
				Print "Empty Data in file: "+wn
				Abort "Empty Data in file: "+wn
			endif
			if (cycle==0) // Region 10
				Region10Flag = 1
				cycle = 1
			endif
			if (Region10Flag==0)  // if this is normal wave.
				wnIncrement += 1
				if (wnIncrement<=9)
					wnWithIncrement = wn + "_0" + num2str(wnIncrement)
				else
					wnWithIncrement = wn + "_" + num2str(wnIncrement)
				endif
				Make /O/N=(numData) $wnWithIncrement
				Wave w1 = $wnWithIncrement
				if (WaveExists(w1)==0)
					continue
				endif
				switch (filemode)
					case 0:
						Prompt hUnit, "Unit for the Horizontal Axis"
						Prompt vUnit,  "Unit for the Vertical Axis"
						DoPrompt "Enter the units for the axes for Wave : "+wnWithIncrement, hUnit, vUnit
						break
					case 2:
						Prompt  E0, "Enter E0 for FRAME: " + num2str(wnIncrement)
						Prompt thetaIn, "Enter theta_In for FRAME: " + num2str(wnIncrement)
						Prompt thetaOut, "Enter theta_Out for FRAME: " + num2str(wnIncrement)
						DoPrompt "Wave:"+wnWithIncrement, E0, thetaIn, thetaOut
						initE /= 1000
						end_E /= 1000
						stepE /= 1000
						comments = ";E0:" + num2str(E0) + ";thetaIn:" + num2str(thetaIn)
						comments += ";thetaOut:" + num2str(thetaOut) + ";cycle:" + num2str(cycle)
						comments += ";dwell:" + num2str(dwell)+";"
						break
					default:
						break
				endswitch
				if (V_Flag)
					Abort
				endif
				//				UnitDialog(hUnit, vUnit, wnWithIncrement)
				SetScale/P x initE, stepE, hUnit, w1
				SetScale d 0, 0, vUnit, w1
				note w1, comments
				Print "Wave generation:", wnWithIncrement
			else 														 // Region 10 wave
				Region10wvnm = wn+"R"+num2str(10)
				Make /O/N=(numData) $Region10wvnm
				Wave Region10W = $Region10wvnm
				if (WaveExists (Region10W) == 0)
					continue
				endif
				SetScale/P x initE, stepE, hUnit, Region10W
				SetScale d 0, 0, Region10W
				Print "Region 10 Wave generation:", Region10wvnm
			endif
			numLoaded=0
			continue
		endif
		///
		if (firstChar > char2num(":"))
			String othercomment
			if (char2num(buffer[strlen(buffer)-1]) ==char2num("\r"))
				buffer[strlen(buffer)-1, strlen(buffer)] = ""
			endif
			othercomment = "Others:"+buffer+";"
			if (Region10Flag ==0)
				note w1, othercomment
			else
				note Region10W, othercomment
			endif
			continue
		endif
		sscanf buffer, "%g", intensity
		if (WaveExists (w1)==0 &&  WaveExists(Region10W)==0)
			Print "This is not EA125 Data file\nAborted..."
			Abort "This is not EA125 File"
		endif
		if (Region10Flag ==0)
			w1[numloaded]=intensity / cycle /dwell
			numloaded +=1
		else
			Region10W[numloaded] = intensity
			numloaded +=1
		endif
	while (1)
	//
	//	print  "wnIncrement", wnIncrement //ここで wnIncrement が 1 のときにうまいことすれば自明の_1が消せる。
	//
	if (wnIncrement == 1)
		wnWithIncrement[strlen(wnWithIncrement)-3, strlen(wnWithIncrement)-1] = ""
		Duplicate $(wnWithIncrement + "_01") $wnWithIncrement
		KillWaves $(wnWithIncrement + "_01")
		Print "The Wave name \" "+ wnWithIncrement + "_01 " + "\" has the meaningless \"_01\", it is removed..."
	endif
	if (WaveExists(Region10W)==1)
		WaveStats /Q/R=[1] Region10W
		String notesforRegion10
		notesforRegion10 = ";Average:"+num2str(V_avg)+";SDEV:"+num2str(V_sdev)+";\r"
		Region10W /= V_avg
		note Region10W, notesforRegion10
	endif
	if (WaveExists(Region10W)==1 && wnIncrement ==1)
		Duplicate $wnWithIncrement $(wnWithIncrement+"N")
		Wave Region1Wave =  $(wnWithIncrement+"N")
		Region1Wave /= Region10W
	endif
	Print "Completed..."       //終了
End

/// @param fn filename whose extension ends with the number
Static Function/S fn2wn(fn)
	String fn
	String wn
	//
	String fnName, fnExtension
	fnName = ParseFilePath(3, fn, ":", 0, 0)
	fnExtension = ParseFilePath(4, fn, ":", 0, 0)
	if (str2num(fnExtension) <= 9 && strlen(fnExtension) ==1)
		fnExtension = "0" + fnExtension
	endif
	wn = fnName + "_" + fnExtension
	return wn
End

/// @param flag  if flag 0, load EA125, 2 Load EA125 (EIS compatible)
/// 				  flag 1 has not been determined yet.  It may be Auger file...
Function LoadEA125filesInDir (flag)
	Variable flag
	//
	String dir_files
	Variable i
	Variable numFiles
	String dirName
	String fileName
	dir_files = SelectFilesIndir ("????","????")
	numFiles = ItemsInList (dir_files)
	dirName = StringFromList (0, dir_files)
	switch (flag)
		case 0:
			for (i=1; i<numFiles; i+=1)
				fileName = dirName+StringFromList(i, dir_files)
				LoadEA125file("", fileName, 0)
			endfor
			break
		case 2:
			for (i=1; i<numFiles; i+=1)
				fileName = dirName+StringFromList(i, dir_files)
				LoadEA125file("", fileName, 2)
			endfor
			break
		default:
			break
	endswitch
End

