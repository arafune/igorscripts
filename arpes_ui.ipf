#/pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma IgorVertsion = 9.0
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later
#include <WaveSelectorWidget>
#pragma ModuleName=ARPES_UI
#include "arpesanalysis"
#include "arpes_derivative"


Menu "Analysis"
	Submenu "ARPES"
		"ARPES to Band", UI_arpes2band()
		"Binning APRES data", UI_binning()
		"Maximum Curvature 1D", UI_maximum_curvature()
		"Maximum Curvature 2D", UI_maximum_curvature2D()
		"Minimum Gradient", UI_minimum_gradient()
	End
End


Function UI_minimum_gradient()
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	//
	Variable n_iter_x = NumVarOrDefault("root:Packages:LaserUPS:NITERATION_X", 9)
	Variable n_iter_y = NumVarOrDefault("root:Packages:LaserUPS:NITERATION_Y", 9)
	Variable box_width_x = NumVarOrDefault("root:Packages:LaserUPS:BOXWIDTH_X", 9)
	Variable box_width_y = NumVarOrDefault("root:Packages:LaserUPS:BOXWIDTH_Y", 9)
	DFREF dfr = root:Packages:LaserUPS
	Variable /G dfr:NITERATION_X = n_iter_x
	Variable /G dfr:NITERATION_Y = n_iter_y
	Variable /G dfr:BOXWIDTH_X = box_width_x
	Variable /G dfr:BOXWIDTH_Y = box_width_y
	//
	NVAR NITERATION_X = dfr:NITERATION_X
	NVAR NITERATION_Y = dfr:NITERATION_Y
	NVAR BOXWIDTH_X = dfr:BOXWIDTH_X
	NVAR BOXWIDTH_Y = dfr:BOXWIDTH_Y
	//
	Variable w_width =  500
	Variable w_height = 520
	//
	String panelName = "MinimumGradient"
	if (WinType(panelName)==7)
		DoWindow /F $panelName
	else
		NewPanel /N=$panelName /W=(630, 175, 630+w_width, 175+w_height)
		DoWindow /F/C $panelName
		SetWindow $panelName hook=ARPES_UI#MinGradUIhook
		SetWindow $panelName sizeLimit = {w_width, w_height, w_width, Inf}
		//
		ListBox WaveSelectorList,pos={20.00,72.00},size={240.00,378.00}
		ListBox WaveSelectorList,fSize=14
		CheckBox FromTopGraphCheck,pos={80.00,477.00},size={123.00,17.00}
		CheckBox FromTopGraphCheck,title="From Top Graph",fSize=14,value=0
		CheckBox FromTopGraphCheck,Proc=eachWave2#CheckProc
		//
		SetVariable n_iteration_x, pos={275.00,69.00},size={202.00,20.00}
		SetVariable n_iteration_x, title="num of iteration(x)",fSize=14
		SetVariable n_iteration_x, limits={1, Inf, 1}
		SetVariable n_iteration_x, value=dfr:NITERATION_X
		SetVariable n_iteration_y, pos={275.00,98.00},size={202.00,20.00}
		SetVariable n_iteration_y, title="num of iteration(y)",fSize=14
		SetVariable n_iteration_y, value=dfr:NITERATION_Y
		SetVariable n_iteration_y, limits={1, Inf, 1}
		SetVariable boxcar_width_x, pos={275.00,133.00},size={202.00,20.00}
		SetVariable boxcar_width_x, title="boxcar width (x)",fSize=14
		SetVariable boxcar_width_x, value=dfr:BOXWIDTH_X
		SetVariable boxcar_width_x, limits={1, Inf, 1}
		SetVariable boxcar_width_y, pos={275.00,167.00},size={202.00,20.00}
		SetVariable boxcar_width_y, title="boxcar width (y)",fSize=14
		SetVariable boxcar_width_y, value=dfr:BOXWIDTH_Y
		SetVariable boxcar_width_y, limits={1, Inf, 1}
		TitleBox Minimum_Gradient, pos={158.00,16.00},size={156.00,30.00}
		TitleBox Minimum_Gradient, title="Minimum Gradient",fSize=18
		//
		Button Perform_MinGradient, pos={300.00,378.00},size={168.00,42.00}
		Button Perform_MinGradient, title="Minimum Gradient"
		Button Perform_MinGradient, Proc=ARPES_UI#CalcMinGrad
		String hlpmsg
		hlpmsg="Name rule\r\"wavename\"_MG:"
		Button Perform_MinGradient, help={hlpmsg}
		Button ClosePanelButton, pos={300.00,445.00},size={168.00,42.00}
		Button ClosePanelButton, title="Close panel"
		Button ClosePanelButton, help={"Close this panel"}
		Button ClosePanelButton, Proc=DonePanelButton
		MakeListIntoWaveSelector(panelName, "WaveSelectorList", content=WMWS_Waves)
	endif
End

Static Function MinGradUIhook(infostr)
	String infostr
	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	//
	Variable statusCode = 0
	strswitch(event)
		case "resize":
			GetWindow $win, wsize
			Variable w_height = V_Bottom - V_top
			ListBox WaveSelectorList, size={240.00,  w_height-142.0}
			CheckBox FromTopGraphCheck, pos={80.00, w_height-45.0}
			Button Perform_MinGradient, pos={300.00, w_height-142.0}
			Button ClosePanelButton, pos={300.00, w_height-75.0}
			break
	endswitch
End

Static Function CalcMinGrad(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("MinimumGradient", "WaveSelectorList")
	String the_matrix
	Variable n = ItemsInList(objs)
	Variable i
	DFREF dfr = root:Packages:LaserUPS
	//
	NVAR NITERATION_X = dfr:NITERATION_X
	NVAR NITERATION_Y = dfr:NITERATION_Y
	NVAR BOXWIDTH_X = dfr:BOXWIDTH_X
	NVAR BOXWIDTH_Y = dfr:BOXWIDTH_Y
	switch(ba.eventCode)
		case 2: //mouse Up
			for (i=0; i<n; i+=1)
				the_matrix = StringFromList(i, objs)
			    minimum_gradient($the_matrix, NITERATION_Y, BOXWIDTH_Y, NITERATION_X, BOXWIDTH_X)
			endfor
			break
	endswitch
End

Function UI_maximum_curvature()
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	Variable /G root:Packages:LaserUPS:NITERATION1
	Variable /G root:Packages:LaserUPS:BOXWIDTH1
	Variable /G root:Packages:LaserUPS:COEFFA
	Variable /G root:Packages:LaserUPS:DIRECTION
	NVAR NITERATION1 = root:Packages:LaserUPS:NITERATION1
	NVAR BOXWIDTH1 = root:Packages:LaserUPS:BOXWIDTH1
	NVAR COEFFA = root:Packages:LaserUPS:COEFFA
	NVAR DIRECTION = root:Packages:LaserUPS:DIRECTION
	DIRECTION=1
	Variable w_width = 470
	Variable w_height = 500
	//
	String panelName = "MaximumCurvature"
	if (WinType(panelName)==7)
		DoWindow /F $panelName
	else
		NewPanel /N=$panelName /W=(630,175,630+w_width,175+w_height)
		DoWindow /F/C $panelName
		SetWindow $panelName hook=ARPES_UI#MaxCurvUIhook
		SetWindow $panelName sizeLimit = {w_width, w_height, w_width, Inf}
		//
		TitleBox Title, pos={143.00, 11.00},size={170.00, 22.00}
		TitleBox Title, title="Curvature calculation (1D)"
		TitleBox Title, fSize=18,frame=0
		ListBox WaveSelectorList, pos={14.00, 45.0}
		ListBox WaveSelectorList, size={220.00, w_height-120.0}, row=10
		ListBox WaveSelectorList, fSize=14
		//
		SetVariable n_iteration, pos={253.00, 52.00}, size={190.00, 19.00}
		SetVariable n_iteration, title="num of iteration", fSize=14
		SetVariable n_iteration, limits={0, Inf, 1}, bodyWidth=84
		SetVariable n_iteration, value=root:Packages:LaserUPS:NITERATION1
		SetVariable n_iteration, help={"Number of iteration about boxcar smoothing"}
		//
		SetVariable boxwidth, pos={256.0, 84.0}, size={187.0, 19.0}
		SetVariable boxwidth, title="width of boxcar",fSize=14
		SetVariable boxwidth, value=root:Packages:LaserUPS:BOXWIDTH1
		SetVariable boxwidth, limits={0, Inf, 1}, bodyWidth=84
		SetVariable boxwidth, help ={"Width of Boxcar. around 5-11"}
		//
		SetVariable coeffa, pos={267.0, 120.0},size={176.0, 19.0}
		SetVariable coeffa, title="coefficient a0", fSize=14
		SetVariable coeffa, limits={0, 1, 0.01}, bodyWidth=84
		SetVariable coeffa, help={"Coefficient a0, around 0.02-0.1"}
		SetVariable coeffa, value=root:Packages:LaserUPS:COEFFA
		//
		PopupMenu popup_axis, pos={290.0, 157.0}, size={99.0, 20.0}
		PopupMenu popup_axis, fSize=14, mode=1, title="Axis"
		PopupMenu popup_axis, popvalue="Energy", value=#"\"Energy;Angle\""
		PopupMenu popup_axis, Proc=ARPES_UI#set_direction
		//
		CheckBox FromTarget, pos={37.0, w_height-60},size={149.0, 22.0}
		CheckBox FromTarget, title="From Top Graph",fSize=18,value=0
		CheckBox FromTarget, Proc=eachWave2#CheckProc
		//
		Button MCCalcButton, pos={278.0, 400.0}, size={150.0, 35.0}
		Button MCCalcButton, title="Maximum Cruvature"
		String hlpmsg
		hlpmsg = "Naming rule\r\"wavename\"_CV(H):  "
		hlpmsg += "Maximum Curvature\r\"wavename\"dx(dy): "
		hlpmsg += "1st Derivative\n\"wavename\"dx2(dy2): 2ndDerivative"
		Button MCCalcButton, help={hlpmsg}
		Button MCCalcButton, Proc=ARPES_UI#CalcMaxCurv
		//
		Button Close, pos={277.0, w_height-50.0}, size={150.0, 35.0}
		Button Close, title="Close panel"
		Button Close, help={"Close this panel"}
		Button Close, Proc=DonePanelButton
		MakeListIntoWaveSelector(panelName, "WaveSelectorList", content=WMWS_Waves)
	endif
End


Static Function set_direction(pu): PopupMenuControl
	STRUCT WMPopupAction &pu
	NVAR DIRECTION = root:Packages:LaserUPS:DIRECTION
	switch (pu.eventCode)
		case 2: // mouse up
			Print pu.popNum
			if (pu.popNum == 1)
				DIRECTION = 1
			else
				DIRECTION = 0
			endif
			break
	endswitch
End

Static Function CalcMaxCurv(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("MaximumCurvature", "WaveSelectorList")
	String the_matrix
	Variable n = ItemsInList(objs)
	Variable i
	NVAR NITERATION1 = root:Packages:LaserUPS:NITERATION1
	NVAR BOXWIDTH1 = root:Packages:LaserUPS:BOXWIDTH1
	NVAR COEFFA = root:Packages:LaserUPS:COEFFA
	//
	switch(ba.eventCode)
		case 2: //mouse up
			ControlInfo /W=MaximumCurvature popup_axis
			Variable direction
			direction = V_Value-1
			for (i=0; i<n; i+=1)
				the_matrix = StringFromList(i, objs)
				Curvature1D($the_matrix, NITERATION1, BOXWIDTH1, direction, COEFFA)
			endfor
			break
	endswitch
	return 0
End


Static Function MaxCurvUIhook(infostr)
	String infostr
	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	//
	Variable statusCode = 0
	strswitch(event)
		case "resize":
			GetWindow $win, wsize
			Variable w_height = V_Bottom - V_top
			ListBox WaveSelectorList, size={220.0, w_height-120}
			CheckBox FromTarget,pos={37.0, w_height-60}
			Button MCCalcButton,pos={278.0, w_height-100.0}
			Button Close, pos={277.0, w_height-50.0}
			break
	endswitch
End


Function UI_arpes2band()
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	Variable energy_shift = NumVarOrDefault("root:Packages:LaserUPS:ENERGYSHIFT", -4.401)
	DFREF dfr = root:Packages:LaserUPS
    Variable /G dfr:ENERGYSHIFT = energy_shift
	NVAR ENERGYSHIFT = dfr:ENERGYSHIFT
	Variable w_width = 470
	Variable w_height = 500
	//
	String panelName = "ARPES_to_Band"
	if (WinType(panelName)==7)
		DoWindow /F $panelName
	else
		NewPanel /N=$panelName /W=(630, 175, 630+w_width, 175+w_height)
		DoWindow /F/C $panelName
		SetWindow $panelName hook=ARPES_UI#arpes2bandUIhook
		SetWindow $panelName sizeLimit = {w_width, w_height, w_width, Inf}
		//
		ListBox WaveSelectorList, pos={14.0, 45.0}
		ListBox WaveSelectorList, size={220, 380}, row=10
		ListBox WaveSelectorList, fSize=14
		//
		SetVariable energyshift, pos={253.0, 52.0}, size={190.0, 19.0}
		SetVariable energyshift, title="Energy shift", fSize=14
		SetVariable energyshift, limits={-Inf, Inf, 1}, bodyWidth=84
		SetVariable energyshift, value=root:Packages:LaserUPS:ENERGYSHIFT
		SetVariable energyshift, help={"Usually -'workfunction of analyzer'"}
		//
		CheckBox FromTarget, pos={37.0, 440.0},size={149.0, 22.0}
		CheckBox FromTarget, title="From Top Graph", fSize=18, value=0
		CheckBox FromTarget, Proc=eachWave2#CheckProc
		//
		Button actionbutton, pos={278.0, w_height-120.0}, size={150.0, 35.0}
		Button actionbutton, title="Convert to band"
		Button actionbutton, Proc=ARPES_UI#GenerateBand
		//
		Button Close, pos={277.0, w_height-50.0}, size={150.0, 35.0}
		Button Close, title="Close panel"
		Button Close, help={"Close this panel"}
		Button Close, Proc=DonePanelButton
		MakeListIntoWaveSelector(panelName, "WaveSelectorList", content=WMWS_Waves)
	endif
End


Static Function GenerateBand(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("ARPES_to_Band", "WaveSelectorList")
	String the_matrix
	Variable n = ItemsInList(objs)
	Variable i
	NVAR ENERGYSHIFT = root:Packages:LaserUPS:ENERGYSHIFT
	//
	switch(ba.eventCode)
		case 2: //mouse up
			for (i=0; i<n; i+=1)
				the_matrix = StringFromList(i, objs)
				arpes2band($the_matrix, ENERGYSHIFT)
			endfor
			break
	endswitch
	return 0
End


Function UI_binning()
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	Variable /G root:Packages:LaserUPS:BINNINGWIDTH
	NVAR BINNINGWIDTH = root:Packages:LaserUPS:BINNINGWIDTH
	Variable w_width = 470
	Variable w_height = 500
	//
	String panelName = "Binning"
	if (WinType(panelName)==7)
		DoWindow /F $panelName
	else
		NewPanel /N=$panelName /W=(630, 175, 630+w_width, 175+w_height)
		DoWindow /F/C $panelName
		SetWindow $panelName hook=ARPES_UI#arpes2bandUIhook
		SetWindow $panelName sizeLimit={w_width, w_height, w_width, Inf}
		//
		ListBox WaveSelectorList, pos={14.0, 45.0}
		ListBox WaveSelectorList, size={220.0, w_height-120},row=10
		ListBox WaveSelectorList, fSize=14
		//
		SetVariable energyshift, pos={253.0, 52.0}, size={190.0, 19.0}
		SetVariable energyshift, title="Binning width", fSize=14
		SetVariable energyshift, limits={0, Inf, 1}, bodyWidth=84
		SetVariable energyshift, value=root:Packages:LaserUPS:BINNINGWIDTH
		SetVariable energyshift, help={"Binning width"}
		//
		PopupMenu popup_axis, pos={335.0, 88.0}, size={99.0, 20.0}
		PopupMenu popup_axis, bodyWidth=116, title="Axis"
		PopupMenu popup_axis, fSize=14, mode=1
		PopupMenu popup_axis, popvalue="Angle", value=#"\"Angle;Energy\""
		//
		CheckBox FromTarget, pos={37.0, w_height-60}, size={149.0, 22.0}
		CheckBox FromTarget, title="From Top Graph", fSize=18, value=0
		CheckBox FromTarget, Proc=eachWave2#CheckProc
		//
		Button actionbutton, pos={278.0, w_height-120.0}, size={150.0, 35.0}
		Button actionbutton, title="Binning the Data"
		Button actionbutton, Proc=ARPES_UI#BinningButton
		//
		Button Close, pos={277.00, w_height-70.00}, size={150.00, 35.00}
		Button Close, title="Close panel"
		Button Close, help={"Close this panel"}
		Button Close, Proc=DonePanelButton
		MakeListIntoWaveSelector(panelName, "WaveSelectorList", content=WMWS_Waves)
	endif
End


Static Function BinningButton(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("Binning", "WaveSelectorList")
	Variable n = ItemsInList(objs)
	String the_matrix
	Variable i
	NVAR BINNINGWIDTH = root:Packages:LaserUPS:BINNINGWIDTH
	switch(ba.eventCode)
		case 2: //mouse up
			ControlInfo /W=Binning popup_axis
			Variable direction
			direction = V_Value-1
			for (i=0; i<n; i+=1)
				the_matrix = StringFromList(i, objs)
				Binning2D($the_matrix, BINNINGWIDTH, direction)
			endfor
			break
	endswitch
	return 0
End


Static Function arpes2bandUIhook(infostr)
	String infostr
	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	GetWindow $win, wsize
	//
	Variable statusCode = 0
	strswitch(event)
		case "resize":
			GetWindow $win, wsize
			Variable w_height = V_Bottom - V_top
			ListBox WaveSelectorList, size={220, w_height-120}
			CheckBox FromTarget, pos={37.0, w_height-60}
			Button actionbutton, pos={278.0, w_height-120.0}
			Button Close, pos={278.00, w_height-70.0}
			break
	endswitch
End


Function UI_maximum_curvature2D()
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	//
	Variable n_iter_x = NumVarOrDefault("root:Packages:LaserUPS:NITERATION_X", 9)
	Variable n_iter_y = NumVarOrDefault("root:Packages:LaserUPS:NITERATION_Y", 9)
	Variable box_width_x = NumVarOrDefault("root:Packages:LaserUPS:BOXWIDTH_X", 9)
	Variable box_width_y = NumVarOrDefault("root:Packages:LaserUPS:BOXWIDTH_Y", 9)
	//
	Variable coeff_a = NumVarOrDefault("root:Packages:LaserUPS:COEFFA", 0.01)
	Variable weight_2d = NumVarOrDefault("root:Packages:LaserUPS:WEIGHT2D", 1)
	DFREF dfr = root:Packages:LaserUPS
	Variable /G dfr:NITERATION_X = n_iter_x
	Variable /G dfr:NITERATION_Y = n_iter_y
	Variable /G dfr:BOXWIDTH_X = box_width_x
	Variable /G dfr:BOXWIDTH_Y = box_width_y
	Variable /G dfr:COEFFA = coeff_a
	Variable /G dfr:WEIGHT2D = weight_2d
	//
	NVAR NITERATION_X = dfr:NITERATION_X
	NVAR NITERATION_Y = dfr:NITERATION_Y
	NVAR BOXWIDTH_X = dfr:BOXWIDTH_X
	NVAR BOXWIDTH_Y = dfr:BOXWIDTH_Y
	NVAR COEFFA = dfr:COEFFA
	NVAR WEIGHT2D = dfr:WEIGHT2D
	//
	Variable w_width = 500
	Variable w_height = 520
	//
	String panelName = "MaximumCurvature2D"
	if (WinType(panelName)==7)
		DoWindow /F $panelName
	else
		NewPanel /N=$panelName /W=(630, 175, 630+w_width, 175+w_height)
		DoWindow /F/C $panelName
		SetWindow $panelName hook=ARPES_UI#MinGradUIhook
		SetWindow $panelName sizeLimit = {w_width, w_height, w_width, Inf}
		//
		ListBox WaveSelectorList, pos={20.0, 72.0}, size={240.0, 378.0}
		ListBox WaveSelectorList, fSize=14
		CheckBox FromTopGraphCheck, pos={80.00, 477.00},size={123.00, 17.00}
		CheckBox FromTopGraphCheck, title="From Top Graph", fSize=14, value=0
		CheckBox FromTopGraphCheck, Proc=eachWave2#CheckProc
		//
		SetVariable n_iteration_x, pos={275.0, 69.0}, size={202.0, 20.0}
		SetVariable n_iteration_x, title="num of iteration(x)", fSize=14
		SetVariable n_iteration_x, value=dfr:NITERATION_X
		SetVariable n_iteration_x, limits={1, Inf, 1}
		SetVariable n_iteration_y, pos={275.0, 98.00}, size={202.0,20.0}
		SetVariable n_iteration_y, title="num of iteration(y)", fSize=14
		SetVariable n_iteration_y, value=dfr:NITERATION_Y
		SetVariable n_iteration_y, limits={1, Inf, 1}
		SetVariable boxcar_width_x, pos={275.0, 133.0}, size={202.0, 20.0}
		SetVariable boxcar_width_x, title="boxcar width (x)",fSize=14
		SetVariable boxcar_width_x, value=dfr:BOXWIDTH_X
		SetVariable boxcar_width_x, limits={1,Inf,1}
		SetVariable boxcar_width_y, pos={275.00, 167.00},size={202.00, 20.00}
		SetVariable boxcar_width_y, title="boxcar width (y)",fSize=14
		SetVariable boxcar_width_y, value=dfr:BOXWIDTH_Y
		SetVariable boxcar_width_y, limits={1, Inf, 1}
		//
		SetVariable coeff_a_x, pos={275.00, 226.00},size={202.00, 20.00}
		SetVariable coeff_a_x, title="coefficient a",fSize=14
		SetVariable coeff_a_x, value=dfr:COEFFA
		SetVariable coeff_a_x, limits={0.01, 1, 0.01}
		String hlpmsg
		hlpmsg = "The arbitrary factor in curvature method\n"
		hlpmsg += "This factor might be smaller than that for 1D."
		SetVariable coeff_a_x, help={hlpmsg}
		SetVariable coeff_weight2d, pos={275.0, 251.0}, size={202.00, 20.00}
		SetVariable coeff_weight2d, title="Weight 2D", fSize=14
		SetVariable coeff_weight2d, limits={-Inf, Inf, 1}
		hlpmsg = "The weight of the MDC curvature/2nd derivative in the calculation\n"
		hlpmsg += "Zero means ignoring. +N means N times bigger. -N means 1/N or N times smaller.\n"
		hlpmsg += "Very small number such as -100 lead to the maximum curvature 1D along the energy.\n "
		hlpmsg += "Thus unity should be the best."
		SetVariable coeff_weight2d, help={hlpmsg}
		SetVariable coeff_weight2d, value=dfr:WEIGHT2D
		//
		TitleBox Minimum_Gradient, pos={158.00, 16.00}, size={156.00, 30.00}
		TitleBox Minimum_Gradient, title="Maximum Curvature 2D", fSize=18
		//
		Button Perform_MinGradient, pos={300.0, 378.0}, size={168.0, 42.0}
		Button Perform_MinGradient, title="Maximum Curvature 2D"
		Button Perform_MinGradient, Proc=ARPES_UI#CalcMaxCurv2D
		hlpmsg="Name rule\nMax curvature(2D):\"wavename\"_C2D\n"
		hlpmsg += "2nd Derivative:\"wavename\"_D2D"
		Button Perform_MinGradient,help={hlpmsg}
		Button ClosePanelButton, pos={300.0, 445.0}, size={168.0, 42.0}
		Button ClosePanelButton, title="Close panel"
		Button ClosePanelButton, help={"Close this panel"}
		Button ClosePanelButton, Proc=DonePanelButton
		MakeListIntoWaveSelector(panelName, "WaveSelectorList", content=WMWS_Waves)
	endif
End

Static Function CalcMaxCurv2D(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("MaximumCurvature2D", "WaveSelectorList")
	String the_matrix
	Variable n = ItemsInList(objs)
	Variable i
	DFREF dfr = root:Packages:LaserUPS
	NVAR NITERATION_X = dfr:NITERATION_X
	NVAR NITERATION_Y = dfr:NITERATION_Y
	NVAR BOXWIDTH_X = dfr:BOXWIDTH_X
	NVAR BOXWIDTH_Y = dfr:BOXWIDTH_Y
	NVAR COEFFA = dfr:COEFFA
	NVAR WEIGHT2D = dfr:WEIGHT2D
//
	switch(ba.eventCode)
		case 2: //mouse up
			for (i=0; i<n; i+=1)
				the_matrix = StringFromList(i, objs)
				Curvature2D($the_matrix, NITERATION_Y, BOXWIDTH_Y, NITERATION_X, BOXWIDTH_X, COEFFA, WEIGHT2D)
			endfor
			break
	endswitch
	return 0
End


