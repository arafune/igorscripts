﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later
#pragma ModuleName = LEED
#include <WaveSelectorWidget>

Menu "Analysis"
	Submenu "LEED"
		"Show LEED picture", UI_display_leed_image()
		"Intensity along the circle", UI_leed_azimuth()
	End
End


//  Return the wave contains the intensity as a function of the angle
//  Appl Phys. Lett. 98 141903 (2011)
//
Function leed_azimuth_angle_dependence(Wave image_name, Variable center_x, Variable center_y, Variable radius, Variable width)
	Variable angle_deg
	Variable x_pos, y_pos
	Variable intensity, i
	Variable j=0
	Variable angle_deg_step = 0.01
	//
	String azimuth_data_name = NameOfWave(image_name)+"_azimuth"
	Make /O/N=(360/angle_deg_step+1) $azimuth_data_name
	wave azimuth_data = $azimuth_data_name
	Variable x_pixcel = DimSize(image_name, 0)
	Variable y_pixcel = DimSize(image_name, 1)
	for (angle_deg = 0; angle_deg < 360; angle_deg += angle_deg_step)
		intensity = 0
		for (i=-width/2; i<=width/2; i+=1)
			x_pos = radius * cos(angle_deg * Pi / 180) + center_x + i
			y_pos = radius * sin(angle_deg * Pi / 180) + center_y + i
			if (((x_pos > 0)  && (x_pos < x_pixcel)) && ((y_pos > 0) && (y_pos < y_pixcel)))
				intensity = intensity + image_name[x_pos][y_pos]
			else
				intensity = NaN
			endif
		endfor
		azimuth_data[j]	 = intensity
		j+=1
    endfor
	String note_str = ""
	note_str = "center_x:" + num2str(center_x)
	note_str += ";center_y:" + num2str(center_y)
	note_str += ";radius:" + num2str(radius)
	note_str += ";width:" + num2str(width)
	SetScale/P x 0,angle_deg_step, "Degrees", azimuth_data
	note /K/NOCR azimuth_data, note_str
End


Function UI_leed_azimuth()
	String targetwindow = WinName(0,1)
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	//
	Variable leed_c_x = NumVarOrDefault("root:Packages:LaserUPS:LEED_CENTER_X", 840)
	Variable leed_c_y = NumVarOrDefault("root:Packages:LaserUPS:LEED_CENTER_Y", 840)
	Variable leed_r = NumVarOrDefault("root:Packages:LaserUPS:LEED_RADIUS", 725)
	Variable leed_w = NumVarOrDefault("root:Packages:LaserUPS:LEED_WIDTH", 31)
	DFREF dfr = root:Packages:LaserUPS
	Variable /G dfr:LEED_CENTER_X = leed_c_x
	Variable /G dfr:LEED_CENTER_Y = leed_c_y
	Variable /G dfr:LEED_RADIUS = leed_r
	Variable /G dfr:LEED_WIDTH = leed_w
	//
	NVAR LEED_CENTER_X = dfr:LEED_CENTER_X
	NVAR LEED_CENTER_Y = dfr:LEED_CENTER_Y
	NVAR LEED_RADIUS = dfr:LEED_RADIUS
	NVAR LEED_WIDTH = dfr:LEED_WIDTH
	//
	Variable w_width =  500
	Variable w_height = 520
	//
	String panelName = "LEEDintensityPlot"
	if (WinType(panelName)==7)
		DoWindow /F $panelName
	else
		GetWindow $targetwindow, wsize
		SetWindow $targetwindow, hook=LEED#followpanelhook
		NewPanel /N=$panelName /W=(V_right, V_top, V_right+w_width, V_top+w_height)
		DoWindow /F/C $panelName
		SetWindow $panelName hook=LEED#AnglePlotUIhook
		SetWindow $panelName sizeLimit = {w_width, w_height, w_width, w_height}
		//
		SetVariable leed_center_x, pos={275.0, 69.0}, size={202.0, 20.0}
		SetVariable leed_center_x, title="Center of spots(x)", fSize=14
		SetVariable leed_center_x, value=dfr:LEED_CENTER_X
		SetVariable leed_center_x, help={"set the (0,0) spot position"}
		SetVariable leed_center_y, pos={275.0, 98.0}, size={202.0,20.0}
		SetVariable leed_center_y, title="Center of spots(y)", fSize=14
		SetVariable leed_center_y, help={"set the (0,0) spot position"}
		SetVariable leed_center_y, value=dfr:LEED_CENTER_Y
		SetVariable leed_radius, pos={275.0, 133.0}, size={202.0, 20.0}
		SetVariable leed_radius, title="Radius", fSize=14
		SetVariable leed_radius, value=dfr:LEED_RADIUS
		SetVariable leed_width, pos={275.0, 167.0}, size={202.0,20.0}
		SetVariable leed_width, title="WIDTH", fSize=14
		SetVariable leed_width, value=dfr:LEED_WIDTH
		TitleBox LEED_analysis, pos={158.0, 16.0},size={156.0, 30.0}
		TitleBox LEED_analysis, title="LEED angle dependence", fSize=18
		String image_name = StringFromList(0, ImageNameList(targetwindow, ";"))
		TitleBox IMG_NAME, title=image_name, fSize=18
		TitleBox IMG_NAME, pos={46,238}, size={156.0, 180.0}
		//
		Button Perform_AngleDep, pos={300.0, 378.0}, size={168.0,42.0}
		Button Perform_AngleDep, title="Make Angle dependence"
		Button Perform_AngleDep, Proc=LEED#GenAngleDep
		String hlpmsg
		hlpmsg="Name rule\r\"wavename\"_azimuth:"
		Button Perform_AngleDep,help={hlpmsg}
//		Button GuessParam,pos={300, 313}, size={168.00,42.00}
//		Button GuessParam,title="Guess parameter"
//		Button GuessParam,proc=LEED#GuessPosition
//		hlpmsg = "Guess the parameter from the cursor"
//		hlpmsg += "Place the one of them at the center and the other at the circle"
//		Button GuessParam, help={hlpmsg}
		Button ClosePanelButton, pos={300.0, 445.0}, size={168.0,42.0}
		Button ClosePanelButton, title="Close panel"
		Button ClosePanelButton, help={"Close this panel"}
		Button ClosePanelButton, Proc=LEED#DonePanelButton
	endif
End


Static Function GenAngleDep(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String targetwindow = WinName(0,1)
	String image_name = StringFromList(0, ImageNameList(targetwindow, ";"))
	//
	DFREF dfr = root:Packages:LaserUPS
	//
	NVAR LEED_CENTER_X = dfr:LEED_CENTER_X
	NVAR LEED_CENTER_Y = dfr:LEED_CENTER_Y
	NVAR LEED_RADIUS = dfr:LEED_RADIUS
	NVAR LEED_WIDTH = dfr:LEED_WIDTH
	switch (ba.eventcode)
		case 2:
		Wave image = ImageNameToWaveRef(targetwindow, image_name)
		if (WaveExists(image))
	 		leed_azimuth_angle_dependence(image, LEED_CENTER_X, LEED_CENTER_Y, LEED_RADIUS, LEED_WIDTH)
		else
			Print "No such wave" + image_name
		endif
		SetDrawLayer /K/W=$targetwindow UserFront
		SetDrawEnv /W=$targetwindow gstart, gname=circle
		SetDrawEnv /W=$targetwindow xcoord= bottom,ycoord= left
		SetDrawEnv /W=$targetwindow linefgc= (65535, 65535, 65535), fillpat = 0, linethick = 2.0
		DrawOval /W=$targetwindow  LEED_CENTER_X - LEED_RADIUS, LEED_CENTER_Y + LEED_RADIUS, LEED_CENTER_X + LEED_RADIUS, LEED_CENTER_Y-LEED_RADIUS
		SetDrawEnv /W=$targetwindow gstop
		break
	endswitch
End


Static Function DonePanelButton(B_struct): ButtonControl
	STRUCT WMButtonAction &B_Struct
	String targetwindow = WinName(0,1)
	if (B_Struct.eventCode ==2)
		SetWindow $targetwindow, hook=$""
		String curWinName = B_Struct.win
		DoWindow /K $curWinName
	endif
End


Static Function followpanelhook(infostr)
	String infostr
	String event = StringByKey("EVENT", infostr)
	String win_name = StringByKey("WINDOW", infostr)
	Variable w_width =  500
	Variable w_height = 520
	strswitch(event)
		case "moved":
			GetWindow $win_name, wsize
			MoveWindow /W=LEEDintensityPlot V_right, V_top, V_right+w_width, V_top+w_height
		break;
	endswitch
End


Function UI_display_leed_image()
	String targetwindow = WinName(0,1)
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	//
	Variable w_width =  500
	Variable w_height = 520
	//
	String panelName = "DisplayLEEDImage"
	if (WinType(panelName)==7)
		DoWindow /F $panelName
	else
		NewPanel /N=$panelName /W=(100, 100, 100+w_width, 100+w_height)
		DoWindow /F/C $panelName
		SetWindow $panelName sizeLimit = {w_width, w_height, w_width, w_height}
		//
		TitleBox Panel_title, pos={158.0, 16.0}, size={156.0, 30.0}
		TitleBox Panel_title, title="Display Image", fSize=18
		//
		ListBox WaveSelectorList, pos={20.0, 72.0}, size={240.0, 378.0}
		ListBox WaveSelectorList, fSize=14
		//
		Button display_button, pos={300.00, 378.0}, size={168.0, 42.0}
		Button display_button, title="Display imgae"
		Button display_button, Proc=LEED#DisplayImageButton
		String hlpmsg
		hlpmsg="Display LEED"
		Button display_button, help={hlpmsg}
		//
		Button ClosePanelButton, pos={300.0, 445.0}, size={168.0,42.0}
		Button ClosePanelButton, title="Close panel"
		Button ClosePanelButton, help={"Close this panel"}
		Button ClosePanelButton, Proc=DonePanelButton
		MakeListIntoWaveSelector(panelName, "WaveSelectorList", content=WMWS_Waves)
	endif
End


Static Function DisplayImageButton(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("DisplayLEEDImage", "WaveSelectorList")
	Variable n = ItemsInList(objs)
	String the_matrix
	Variable i
	switch(ba.eventCode)
		case 2:
			for (i=0; i<n; i+=1)
				the_matrix = StringFromList(i, objs)
				Display
				String targetwindow = WinName(0,1)
				AppendImage /W=$targetwindow $the_matrix
				ModifyGraph /W=$targetwindow height={Aspect,1}
				ModifyGraph /W=$targetwindow width=700
				DoUpdate /W=$targetwindow
				ModifyGraph /W=$targetwindow width=0
				Wave image_wave = ImageNameToWaveRef(targetwindow, the_matrix)
				ModifyImage ''#0, ctab={300,*,Turbo,0}, log=1
			endfor
			break
	endswitch
	return 0
End
