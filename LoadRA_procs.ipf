/// @file LoadRA_procs.ipf
/// @brief Load Function made by RA.
#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 4.0

/// 2019/09/28　EA125でとったデータを解析するルーチンをとりあえずの除いておこう。


#include "LoadEA125"
// #include "fitVLPES"
// #include "AnalysisLUPS"
// #include "NormalizedLUPS"
#include "RA_common"
#include "eachWave2"
#include "LoadQmassData"
#include "arpes_ui"
#include "Load_SP2"
#include "ImgColorChange"
#include "ParabolicFit"
#include "leed_analysis"
#include "fitVLPES"
// #include "OUPSLoader"
// #include "SAC"
// #include "Loadsp2”

Menu "Misc"
	"-"
	"Load color tables", Load_all_color_tables()
End

Function Load_all_color_tables()
	LoadIBWColorTables(1)
	LoadIBWColorTables(2)
End

Function IgorStartOrNewHook(igorApplicationNameStr)
	String igorApplicationNameStr
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder root:Packages:LaserUPS
	Variable /G K_inv_AA = 0.5123167219534328
	Variable /G light_speed_m = 299792458 // m unit
	Variable /G delay_fs_um = 3.335640951981521 // fs/um unit.
	Variable /G degrees = 1.745329251E-2 // π/180
	SetDataFolder root:
End


Function AfterFileOpenHook(refNum,file,pathName,type,creator,kind)
	Variable refNum,kind
	String file,pathName,type,creator
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder root:Packages:LaserUPS
	Variable /G K_inv_AA = 0.5123167219534328
	Variable /G light_speed_m = 299792458
	Variable /G delay_fs_um = 3.335640951981521 // (fs/micron)
	Variable /G degrees = 1.745329251E-2
	SetDataFolder dfSave
End


////////////////////////////////////////////////////////////////////g////
// Loading Igor Color Tables as ibw
////////////////////////////////////////////////////////////////////////
Function LoadIBWColorTables(optVar)
	Variable optVar
	NewDataFolder/O root:Packages
	NewDataFolder/O root:Packages:ColorTables
	String pathToIBWs, dirList, ibwList, pathString, folderName
	Variable i
	if ((optVar & 2^0) != 0) // bit 0 is set
		pathToIBWs = SpecialDirPath("Igor Application",0,0,0) + "Color Tables"
		TreeLoad(pathToIBWs, "IgorExtra")
	endif
	if ((optVar & 2^1) != 0) // bit 1 is set
		pathToIBWs = SpecialDirPath("Igor Pro User Files",0,0,0) + "User Procedures:SPD:Color Tables"
		TreeLoad(pathToIBWs, "UserExtra")
	endif
	SetDataFolder root:
End

Static Function TreeLoad(dirPath, folderName)
	String dirPath, folderName
	NewPath/O/Q/Z path1, dirPath
	if(V_Flag == -1)
		return -1
	endif
	String ibwList = IndexedFile(path1,-1,".ibw")
	if (strlen(ibwList) > 0)
		FindAndLoad(folderName,ibwList)
	endif
	String dirList = IndexedDir(path1,-1,1) // full file paths
	String pathString
	Variable i
	for(i = 0; i < ItemsInList(dirList); i += 1)
		pathString = StringFromList(i, dirList)
		folderName = ParseFilePath(0,pathstring,":",1,0)
		NewPath/O/Q path1, pathString
		ibwList = IndexedFile(path1,-1,".ibw")
		FindAndLoad(folderName,ibwList)
	endfor
End

Static Function FindAndLoad(folderName, fList)
	String folderName, fList
	fList = SortList(fList, ";", 16)
	NewDataFolder/O/S $("root:Packages:ColorTables:" + folderName)
	// Remove low resolution color table in SciColMaps.
	fList = GrepList(fList, "(10|25|50|100).ibw$", 1)
	Variable i
	for(i = 0; i < ItemsInList(fList); i += 1)
		LoadWave/H/O/P=path1/Q ":" + StringFromList(i,fList)
	endfor
	KillStrings/A/Z
	KillVariables/Z V_flag
End
