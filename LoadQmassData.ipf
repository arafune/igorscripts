#pragma rtGlobals=3		// Use modern global access method.
// Load Q-mass data of SPECTRA
#include "SelectFiles"
Menu "Load Waves"
	"Load Qmass Data", LoadQmassDataSpectra("","")
End

/// @param pathName
/// @param fileName
Function LoadQmassDataSpectra(String pathName, String fileName)
	Variable err
	Variable refNum
	String message="Select Q-mass spectrum (.wan) file"
	Open /R/Z=2/P=$pathName/M=message refNum as fileName
	FStatus refNum
	if (refNum==0)
		Print  "Select QMass Data File (*.wan).\nAborted"
		Abort "Select QMass Data File (.*.wan)."
	endif
	String baseWavename = "Qmass"
	Prompt baseWavename, "Enter base name of wave"
	DoPrompt "Enter base name", baseWavename

	String wn
	//
	Variable wnIncrement=0
	String Buffer
	String tmpstr
	String itemName
	//
	do
		FReadLine refNum, Buffer
		tmpstr = StringFromList(0,Buffer,",")
		sscanf tmpstr, "\"[%[ABCDEFGHIJKLMNOPQRSTUVWXYZ ]]\"", itemName
		//		print itemName
		strswitch (itemName)
			case "FILE TYPE":
				String fileType
				tmpstr = StringFromList(1, Buffer, ",")
				sscanf tmpstr, "\"%[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ]\"", fileType
				break
			case "NUMBER OF SCANS":
				Variable numScans
				tmpstr = StringFromList (1, Buffer, ",")
				numScans=str2num(tmpstr)
				break
			case "PEAK DATA LENGTH":
				Variable length
				tmpstr = StringFromList (1, Buffer,",")
				length = str2num(tmpstr)
				Variable physicalstep
				switch (length)
					case 64:
						physicalstep = 0.25
						break
					case 32:
						physicalstep = 0.25/2
						break
					case 16:
						physicalstep = 0.25/4
						break
					case 8:
						physicalstep = 0.25/8
						break
				endswitch
			case "MASS RANGE":
				Variable firstMass
				Variable endMass
				tmpstr = StringFromList (1, Buffer,",")
				firstMass = str2num(tmpstr)
				tmpstr = StringFromList (2, Buffer, ", ")
				endMass  = str2num(tmpstr)
				break
			case "SCAN NO":  // This line contains the pressure values.
				Variable physicalendmass = endmass +0.5
				Variable physicalinitmass = endmass + 0.5 -255 * physicalstep
				Buffer= RemoveListItem (0, Buffer, ",")
				tmpstr = StringFromList(0, Buffer, ",")
				Variable scanN
				sscanf tmpstr, "%i", scanN
				Buffer= RemoveListItem (0, Buffer, ",")
				Buffer= RemoveListItem (0, Buffer, ",")   // Remove "[DATE]"
				Buffer= RemoveListItem (0, Buffer, ",")   // Remove contents of "[DATE]"
				Buffer= RemoveListItem (0, Buffer, ",")   // Remove "[TIME]"
				Buffer= RemoveListItem (0, Buffer, ",")   // Remove contents of "[TIME]"
				Buffer= RemoveListItem (0, Buffer, ",")   // Remove "[PEAK DATA]"
				Variable n = ItemsInList(Buffer, ",")
				Buffer= RemoveListItem (n, Buffer, ",")   // Remove contents of "[TOTAL]"
				Buffer= RemoveListItem (n, Buffer, ",")   // Remove "[TOTAL]"
				n = ItemsInList(Buffer, ",")
				String WaveName =baseWavename+num2str(scanN)
				Make /N=(n-2) $WaveName
				wave awave=$WaveName
				Variable i = 0
				for(i=0; i<n-2; i+=1)
					awave[i] = str2num(StringFromList(i, Buffer, ","))
				endfor
				SetScale /I x, physicalinitmass, physicalendmass, "", awave
				SetScale d 0,0, "mbar", awave
				break
			default:
				break
		endswitch
		if (strlen(Buffer)==0)
			break
		endif
	while(1)
	Close refNum
End
