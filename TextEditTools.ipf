﻿#pragma TextEncoding="UTF-8"
#pragma rtGlobals=3
#pragma IgorVersion=8
#pragma IndependentModule=TextEditTools
#pragma version=1.80

#if (NumberByKey("BUILD", IgorInfo(0)) < 36043)
#define TextEditTools error: please update Igor Pro to a later nightly build - see Misc Menu
#endif

// A compendium of text-editing tools

// Contributors:
// https://www.wavemetrics.com/user/tony
// https://www.wavemetrics.com/user/chozo
// https://www.wavemetrics.com/user/thomasbraun

// https://www.wavemetrics.com/code-snippet/text-editing-tools-compendium-user-contributions

// define the functions you wish to use
#define CommentWrap
#define Prettify
#define AlignTabs
#define TransposePaste
#define ControlFunctions

// Default menu shortcuts:
// To format selection as a wrapped comment: ctrl-4 (Windows) or cmd-4 (Mac)
// To prettify selection: ctrl-5 (Windows) or cmd-5 (Mac)
// To align comments in the selected code use:
// Left-aligned: cmd-7 (Mac), ctrl-7 (Windows)
// Right-aligned: cmd-8 (Mac), ctrl-8 (Windows)

// Edit menu shortcuts here:
Function /S addShortcut(String sMenu)
	String shortcut = ""
	strswitch(sMenu)
		case "Wrap Selected Comment":
			shortcut = "/4"
			break
		case "Prettify Selected Code":
			shortcut = "/5"
			break
		case "Align Comments in Selected Code (Left)":
			shortcut = "/7"
			break
		case "Align Comments in Selected Code (Right)":
			shortcut = "/8"
			break
	endswitch
	return sMenu + shortcut
End

// For comment aligning, dial in your procedure window settings here:
// You can find the default tab size under Procedure->Document Settings...
Strconstant FontName = "Courier New"
Constant FontSize = 11
Constant DefaultTab = 20	// in points

// Set line length for word wrapping comments here:
Constant kCommentWidth = 72 // word wrapping is done as a 'greedy' wrap

// Customize prettification here:
Strconstant ksIgnoreList = "wave;" // function names, operation names and keywords to ignore
Strconstant ksCustomList = "" // add or override items
Constant kMinWordLength = 2
Constant kPreserveIndentationTabs = 1

// ------ End of customizable options -------

Menu "Edit"
	"-"
End

// *** comment wrapping ***

#ifdef CommentWrap

Menu "Edit"
	//	"Comment Wrap Clipboard", /Q, WrapScrap()
	"Unwrap Clipboard", /Q, UnwrapScrap()
	addShortcut("Wrap Selected Comment"), /Q, WrapSelection()
End

Function WrapScrap()
	PutScrapText CommentWrap(GetScrapText(), kCommentWidth)
End

Function UnwrapScrap()
	PutScrapText CommentUnwrap(GetScrapText())
End

Function PrettifyScrap()
	PutScrapText Prettify(GetScrapText())
End

Function WrapSelection()
	String strScrap = GetScrapText()
	PutScrapText ""
	DoIgorMenu "Edit" "Copy"
	WrapScrap()
	DoIgorMenu "Edit" "Paste"
	PutScrapText strScrap // leave clipboard state unchanged
End

// returns string wrapped at V_wrap characters, commented,
// and indented to match indent at start of s_in.
Function /T CommentWrap(String s_in, Variable v_wrap)
	if(strlen(s_in)==0)
		return ""
	endif
	String s_out = "", s_char = ""
	s_in = ReplaceString("\t", s_in, "   ")
	
	// try to guess how far we want to indent comment
	Variable indent = 0, maxIndent = 25
	for(indent=0;indent<maxIndent;indent+=1)
		if(cmpstr(s_in[indent], " "))
			break
		endif
	endfor
	
	s_in = ReplaceString("//", s_in, " ")
	s_in = ReplaceString("\r", s_in, " ")
	
	// condense whitespace
	Variable len
	do
		len = strlen(s_in)
		s_in = ReplaceString("  ", s_in, " ")
	while(strlen(s_in) != len)
	
	Variable i = 0,j = 0,k = 0
	// i is character count in s_in
	// j is character count in current line of s_out
	do
		s_char = s_in[i]
		i += 1
		if(j == 0)
			for(k=0;k<indent;k+=1,j+=1)
				s_out += " "
			endfor
			s_out += "// "
			j += 3
			if (cmpstr(s_char, " "))
				s_out += s_char
				j += 1
			endif
		elseif (j > v_wrap)
			if(cmpstr(s_char, " "))
				do
					j -= 1
					if (cmpstr(s_out[strlen(s_out)-(v_wrap+1-j)], " ")==0)
						break
					endif
				while (j > indent+3)
				if(j > indent+3)
					s_out = s_out[0, strlen(s_out)-(v_wrap+1-j)]
					i -= (v_wrap + 1 - j)
				endif
			endif
			s_out += "\r"
			j = 0
		else
			s_out += s_char
			j += 1
		endif
	while (i < len)
	s_out = ReplaceString("   ", s_out, "\t")
	
	return s_out
End

// returns unwrapped string with comment indicators removed
Function /T CommentUnwrap(String s)
	s = ReplaceString("\t", s, "   ")
	s = ReplaceString("//", s, "")
	s = ReplaceString("\r\r", s, "<$$> ")
	s = ReplaceString("\r", s, " ")
	s = ReplaceString("<$$>", s, "\r\r")
	
	// condense whitespace
	Variable len
	do
		len = strlen(s)
		s = ReplaceString("  ", s, " ")
	while(strlen(s)!=len)
	do
		len = strlen(s)
		s = ReplaceString("\r ", s, "\r")
	while(strlen(s)!=len)
	do
		s[0,0] = SelectString(cmpstr(s[0]," "), "", s[0])
	while(strlen(s) && cmpstr(s[0]," ")==0)
	
	return s
End

#endif

// *** prettify ***

#ifdef Prettify

Menu "Edit"
	addShortcut("Prettify Selected Code"), /Q, PrettifySelection()
End

Function PrettifySelection()
	String strScrap = GetScrapText()
	PutScrapText ""
	DoIgorMenu "Edit" "Copy"
	PutScrapText Prettify(GetScrapText())
	DoIgorMenu "Edit" "Paste"
	PutScrapText strScrap // leave clipboard state unchanged
End

// Returns a string with known function and operation names case-corrected.
// Commented and quoted text is ignored. Trailing whitespace is eradicated.
// Blocks of code representing proc pictures and machine-generated window
// macros are ignored, provided the selected text starts before the
// Picture or Window keyword.
Function /S Prettify(String strText, [String ignoreList, String customList, Variable minWordLength])
	if (strlen(strText)==0)
		return ""
	endif
	ignoreList = SelectString(ParamIsDefault(ignoreList), ignoreList, ksIgnoreList)
	customList = SelectString(ParamIsDefault(customList), customList, ksCustomList)
	minWordLength = ParamIsDefault(minWordLength) ? 2 : minWordLength
	wave wList = CreateWordListWave(ignoreList, customList, minWordLength)
	
	Variable endsWithReturn = (cmpstr(strText[strlen(strText)-1], "\r") == 0)
	wave /T wText = ListToTextWave(strText, "\r")
	Variable lastLine = numpnts(wText) - 1
	wText += SelectString(p<(lastLine) || endsWithReturn, "", "\r")
	
	PrettifyTextWave(wText, ignoreList, customList, minWordLength)
	wfprintf strText, "%s", wText
	return strText
End

Function PrettifyTextWave(wave /T wText, String ignoreList, String customList, Variable minWordLength)
	wave wList = CreateWordListWave(ignoreList, customList, minWordLength)
	// create a mask for lines that should not be altered
	Make/FREE/N=(DimSize(wText, 0)) wIgnore
	CreateMask(wText, wIgnore, "Picture", "End")	
//	CreateMask(wText, wIgnore, "Static Picture", "End")
	CreateMask(wText, wIgnore, "Window", "EndMacro")
	if(numpnts(wText)>1)
		MultiThread wText = PrettifyLine(wText, wList, wIgnore)
	else // possible bug when single point wave assignment is multithreaded in an independent module
		wText = PrettifyLine(wText, wList, wIgnore)
	endif
	return 1
End

// create a text wave of words to be case corrected
Function /WAVE CreateWordListWave(String ignoreList, String customList, Variable minWordLength)
	
	// create a default list of words to be case corrected
	String keyWords = "Constant;DefaultTab;DoPrompt;End;EndMacro;EndStructure;Function;GalleryGlobal;hide;IgorVersion;"
	keyWords += "IndependentModule;Macro;Menu;ModuleName;MultiThread;Override;Picture;Popup;Proc;ProcGlobal;"
	keyWords += "Prompt;root;rtGlobals;Static;Strconstant;Structure;Submenu;TextEncoding;ThreadSafe;version;Window;"
	String subtypes = "ButtonControl;CameraWindow;CDFFunc;CheckboxControl;CursorStyle;DrawUserShape;FitFunc;"
	subtypes += "GizmoPlot;Graph;GraphMarquee;GraphStyle;GridStyle;Layout;LayoutMarquee;LayoutStyle;ListBoxControl;"
	subtypes += "Panel;PopupMenuControl;SetVariableControl;SliderControl;TabControl;Table;TableStyle;"
	String objectRefs = "DFREF;FUNCREF;NVAR;STRUCT;SVAR;WAVE;"
	String flowControl = "AbortOnRTE;AbortOnValue;break;case;catch;continue;default;do;else;elseif;endfor;endif;"
	flowControl += "endswitch;endtry;for;if;return;strswitch;switch;try;while;"
	String strList = FunctionList("*",";","") + OperationList("*",";","") + keyWords + subtypes + objectRefs + flowControl
	
	// remove words in ignoreList, substitute or add words in customList
	strList = RemoveFromList(ignoreList, strList, ";", 0)
	strList = RemoveFromList(customList, strList, ";", 0) + customList
	WAVE /T wList = ListToTextWave(strList, ";")
	
	// remove words with fewer than minWordLength characters
	Variable i
	for(i=numpnts(wList)-1; i>=0; i-=1)
		if(strlen(wList[i]) < minWordLength)
			DeletePoints i, 1, wList
		endif
	endfor
	return wList
End

// Creates a mask wave in wIgnore
// wIgnore will have 1 if the line should be ignored, 0 otherwise
Function CreateMask(WAVE/T wText, WAVE wIgnore, String beginStr, String endStr)
	
	String beginRegExp = "(?i)^\s*(static\s*)?\\Q" + beginStr + "\\E\b"
	String endRegExp = "(?i)^\s*\\Q" + endStr + "\\E\b"
	Variable vLine, i, numLines
	numLines = DimSize(wText, 0)

	// create a mask for lines that should not be altered
	Grep /Q/INDX/E=beginRegExp wText
	WAVE W_Index
   
	for (i=0;i<numpnts(W_Index);i+=1)
		vLine = w_Index[i] + 1 // the line that starts the code to be masked
		if(vLine > numLines)
			break
		endif
		do
			wIgnore[vLine] = 1
			vLine += 1
		while(vLine<numLines && GrepString(wText[vLine],endRegExp)==0)
	endfor
End

// strLine should include eol if one was present in input text
ThreadSafe Function /S PrettifyLine(String strLine, WAVE /T wordList, Variable ignore)
	if(ignore)
		return strLine
	endif
	WAVE /B mask = QuoteAndCommentMask(strLine)
	String nonWhitespace = "", whitespace = ""
	String strCode = "", word = ""
	int bytes = -1, len = numpnts(mask)
	// find position of last unmasked character in line
	Duplicate /free mask rmask
	Reverse mask /D=rmask
	FindValue /I=0 rmask // find first 0 in reversed mask wave
	if (v_value == -1) // entire line is masked
		strCode = ""
	else
		strCode = strLine[0,len-1-v_value]
	endif
	// strCode is stripped of commented text

	do // loop through words in line
		SplitString /E="([[:alnum:]\.]+)(.*)" strCode, word, strCode
		FindValue /TEXT=word/TXOP=4/Z wordList
		if(V_value > -1)
			strLine = ReplaceWord(word, strLine, wordList[V_value], mask)
		endif
	while(strlen(strCode))

	// remove trailing whitespace
	// if kPreserveIndentationTabs = 1, check for non-whitespace at start of line
	String regex = SelectString(kPreserveIndentationTabs, "\r$", "\S[[:blank:]]+\r$")
	if(GrepString(strLine, regex))
		SplitString/E=("^(?U)(.*)([[:space:]]*)$") strLine, nonWhitespace, whitespace
		strLine = nonWhitespace + "\r"
	endif
	return strLine
End

ThreadSafe Function /WAVE QuoteAndCommentMask(String strLine)
	Variable startByte, endByte, len
	len = strlen(strLine)
	Make /B/free/N=(len) mask = 0
	startByte = 0
	do
		startByte = strsearch(strLine, "\"", startByte)
		if (startByte == -1) // no more quoted text before end of strLine
			break
		endif
		endByte = startByte
		do
			endByte = strsearch(strLine, "\"", endByte+1)
			if (endByte == -1)
				endByte = len - 1
				break
			endif
			// endByte is possible end of quote
			// remove escaped backslashes!
			strLine[startByte,endByte] = ReplaceString("\\\\", strLine[startByte,endByte], "  ")
		while(cmpstr(strLine[endByte-1], "\\")==0) // ignore escaped quotes
		// found end of quote
		mask[startByte, endByte] = 1
		startByte = endByte + 1
	while(1) // look for next quoted text
	// quoted text is now masked
	startByte = 0
	do
		startByte = strsearch(strLine, "//", startByte)
		if (startByte == -1) // no comment before end of strLine, we're done
			return mask
		endif
		if (mask[startByte] == 1)
			startByte += 1
			continue
		endif
		// startByte is start of comment
		mask[startByte,len-1] = 1
		return mask
	while(1)
End

// Wrapper for ReplaceString, replaces whole words
// characters inStr[i] where mask[i]==1 are ignored
ThreadSafe Function /S ReplaceWord(String replaceThisWord, String inStr, String withThisWord, WAVE /B mask)
	String substring = ""
	Variable startByte = 0, endByte = 0
	if (strlen(replaceThisWord) == 0)
		return inStr // quick exit
	endif
	do // loop through matches
		startByte = strsearch(inStr, replaceThisWord, endByte, 2)
		if(startByte == -1)
			return inStr
		endif
		endByte = startByte + strlen(replaceThisWord)
		if(mask[startbyte]==1)
			continue
		endif
		substring = inStr[startByte-1, endByte+1] // will be clipped to 0, strlen(inStr)
		// check that match is whole word
		if(GrepString(substring, "(?i)\\b" + replaceThisWord + "\\b"))
			substring = ReplaceString(replaceThisWord, substring, withThisWord)
		endif
		inStr = inStr[0, startByte-2] + substring + inStr[endByte+2, Inf]
	while(1)
End

Function PrettifyFileGUI()
	Open /D/R/F="Procedure Files (*.ipf):.ipf;"/MULT=1 refnum
	Make /free/N=(ItemsInList(s_filename, "\r")) w
	w = PrettifyFile(StringFromList(p, s_filename, "\r"))
End

// unless eol is specified this will substitute \r for any eol used by file
Function PrettifyFile(String filePath, [String ignoreList, String customList, Variable minWordLength, String eol])
	ignoreList = SelectString(ParamIsDefault(ignoreList), ignoreList, ksIgnoreList)
	customList = SelectString(ParamIsDefault(customList), customList, ksCustomList)
	minWordLength = ParamIsDefault(minWordLength) ? 2 : minWordLength
	String strContents = LoadFile(filePath)
	strContents = ReplaceString("\r\n", strContents, "\r")
	strContents = ReplaceString("\n", strContents, "\r")
	strContents = Prettify(strContents, ignoreList=ignoreList, customList=customList, minWordLength=minWordLength)
	if (!ParamIsDefault(eol))
		strContents = ReplaceString("\r", strContents, eol)
	endif
	WriteFile(filePath, strContents)
End

Function/S LoadFile(String fullPath)
    Variable fnum
    String data
    Open/R/Z=1 fnum as fullPath
    FStatus fnum
    data = ""
    data = PadString(data, V_logEOF, 0x20)
    FBinRead fnum, data
    Close fnum
    return data
End

Function WriteFile(String fullPath, String contents)
    Variable fnum
    Open/Z fnum as fullPath
    FBinWrite fnum, contents
    Close fnum
End

#endif

// *** transpose paste ***

#ifdef TransposePaste

Menu "Edit", hideable, dynamic
	SelectString((WinType("")==2), "", "Transpose Paste"), /Q, TextEditTools#TransposePaste()
End

Function TransposePaste()
	String clip = GetScrapText(), strOut = ""
	Variable rows = ItemsInList(clip, "\r")
	Variable columns = ItemsInList(StringFromList(0,clip,"\r"), "\t")
	Variable i, j
	for(i=0;i<columns;i+=1)
		for(j=0;j<rows;j+=1)
			strOut += StringFromList(i,StringFromList(j, clip,"\r"), "\t") + "\t"
		endfor
		strOut = RemoveEnding(strOut,"\t")
		strOut += "\r"
	endfor
	PutScrapText strOut
	DoIgorMenu "Edit", "Paste"
	PutScrapText clip // leave state of clipboard unchanged
End

#endif

// *** align comments ***

#ifdef AlignTabs

Menu "Edit"
	"Align Comments in Clipboard Code (Right)", /Q, PutScrapText AlignComments(GetScrapText())
	addShortcut("Align Comments in Selected Code (Left)"), /Q, AlignCommentsInSelection(1)
	addShortcut("Align Comments in Selected Code (Right)"), /Q, AlignCommentsInSelection(0)
End

Function AlignCommentsInSelection(Variable LeftOrRight)
	String strScrap = GetScrapText()
	DoIgorMenu "Edit" "Copy"
	PutScrapText AlignComments(GetScrapText(),AligntoLeft=LeftOrRight)
	DoIgorMenu "Edit" "Paste"
	PutScrapText strScrap // leave clipboard state unchanged
End

// For setting a determined tab position of all selected comments.
// Execute from the commandline as AlignComments#toTabPos(desiredPos).
Function toTabPos(Variable TabPos)															// for aligning all comments to a certain tab position
	DisplayProcedure/W=$StringFromList(0,WinList("*", ";","WIN:128"))				// bring to front
	String strScrap = GetScrapText()
	DoIgorMenu "Edit" "Copy"
	PutScrapText AlignComments(GetScrapText(),TabPos=TabPos)
	DoIgorMenu "Edit" "Paste"
	PutScrapText strScrap
End

// The optional variable TabPos lets you choose a fixed offset (in tab widths) for all comments.
// For example, TabPos = 30 will position all comments at 30 tabs, if possible.
Function/S AlignComments(String strText, [Variable TabPos, Variable AlignToLeft])
	Variable LorR = 0
	if(!ParamIsDefault(AlignToLeft))
		LorR = AlignToLeft
	endif
	
	Variable fsPix = FontSize * ScreenResolution/72										// font size in pix
	Variable TabPix = floor(DefaultTab * ScreenResolution/72)							// tab width in pix
	
	WAVE/T wText = ListToTextWave(strText, "\r")
	Variable endsWithReturn = (cmpstr(strText[strlen(strText)-1], "\r") == 0)
	Variable entries = numpnts(wText)

	String strEverything = "", strStartOfFirst	 = "", strEndOfFirst = wText[0]		// extract the start of the first line from the whole text
	strEverything = ProcedureText("",0,StringFromList(0,WinList("*", ";","WIN:128")))
	strEverything = ReplaceString(strText,strEverything,"\rCUT\r")					// create a break at selected text
	strEverything = StringFromList(0, strEverything, "\rCUT\r")						// only the stuff before the selection
	if (cmpstr(strEverything[strlen(strEverything)-1], "\r") != 0 && strlen(strEverything)>0)	// only if it is not a full line
		WAVE/T wBegin = ListToTextWave(strEverything, "\r")
		strStartOfFirst	= wBegin[numpnts(wBegin)-1]										// the extra part of the first line
	endif

	if (strlen(strStartOfFirst) > 0)															// put the full first line in here
		wText[0] = strStartOfFirst + strEndOfFirst											// this is needed to calculate the correct line length
	endif
	wText += SelectString(p<(entries-1) || endsWithReturn, "", "\r")

	Make/D/FREE/N=(entries) CodeSize = 0													// saves the code size in 'tab widths'
	Make/T/FREE/N=(entries) CodeOnly, CommentOnly
	
	Variable i,j, bytes = 0, strSize = 0
	String strLine = "", strCode = ""
	
	for (i = 0; i < entries; i += 1)
		strLine = wText[i]
		//bytes = strsearch(strLine, "//",Inf,1)
		bytes = startOfComment(strLine)														// is there a comment?
		bytes = bytes == strlen(strLine) ? -1 : bytes										// no comment found

		if (i == 0 && strlen(strStartOfFirst) > bytes)									// if the full comment in not selected -> skip
			bytes = -1
		endif
		
		if(bytes > 1)
			strCode = strLine[0,bytes-1]
			if(GrepString(strCode, "\S")) 													// line contains non-whitespace
				CodeOnly[i]		= strCode														// just the code
				CommentOnly[i]	= strLine[bytes,Inf]										// just the comment
				if (cmpstr(strCode[strlen(strCode)-1], " ") == 0)
					CodeOnly[i] 	= RemoveEnding(CodeOnly[i] , " ")						// get rid of a possible space character in front of the comments
					CodeOnly[i] 	+= "\t"
				endif
				
				// Below code calculates the line length in units of 'tab length'.
				// Multiplication by TabPix would give the length in pix.
				WAVE/T wTabList = ListToTextWave(CodeOnly[i], "\t")						// split code line by tabs
				for (j = 0; j < DimSize(wTabList,0); j += 1)
					strCode = wTabList[j]
					if (strlen(strCode) > 0)													// tab only or code?
						strSize = FontSizeStringWidth(FontName,fsPix,0,strCode,"")		// get the length of all characters
						if (mod(strSize,TabPix) < 0.05)										// does this have roughly the size of a certain no. of tabs?
							strSize = ceil(strSize/TabPix + 1)								// a full tab length is added
						else
							strSize = ceil(strSize/TabPix)									// this will add a fractional tab length
						endif
						CodeSize[i] += strSize
					else
						CodeSize[i] += 1															// if it's just a tab then add one tab length
					endif
				endfor
				if (cmpstr(strCode[strlen(strCode)-1], "\t") == 0)						// no tab in last entry -> not aligned to the next tab position
					CodeSize[i] -= 1
				endif
			endif
		endif
	endfor
	
	wText[0] = strEndOfFirst + "\r"															// put only the end line back (in case there is no modification)
	if (strlen(strStartOfFirst) > 0)															// remove the first part of the first line again
		CodeOnly[0] = ReplaceString(strStartOfFirst,CodeOnly[0],"")
	endif
	
	Variable AlignDistance = 0																// how many tabs define the new alignment
	if(!ParamIsDefault(TabPos))
		AlignDistance = TabPos
	else
		Extract/Free CodeSize,SizeVals,CodeSize!=0
		AlignDistance = LorR == 1 ? max(WaveMin(SizeVals),AlignDistance) :  max(WaveMax(SizeVals),AlignDistance)
	endif
	
	CodeSize = CodeSize[p] > 0 ? AlignDistance - CodeSize[p] : 0						// how many tabs to add or subtract
	for (i = 0; i < entries; i += 1)
		String newTabs = ""
		if(abs(CodeSize[i]) > 0)
			for (j = 0; j < abs(CodeSize[i]); j += 1)
				if (CodeSize[i] < 0)
					CodeOnly[i] = RemoveEnding(CodeOnly[i],"\t")
				else
					newTabs += "\t"
				endif
			endfor
			wText[i] = CodeOnly[i] + newTabs + CommentOnly[i]
		endif
	endfor
		
	wfprintf strText, "%s", wText
	return strText
End

Function startOfComment(String strLine)
	Variable startByte, commentByte, startQuote, endQuote, lineLength
	lineLength=strlen(strLine)
	startByte=0
	do
		if(startByte>=(lineLength-1))
			return lineLength
		endif
		commentByte = strsearch(strLine, "//", startByte)
		if(commentByte == -1)
			return lineLength
		endif
		// found "//"
		startQuote=strsearch(strLine, "\"", startByte)
		if(startQuote==-1 || startQuote>commentByte) // no quotes before //, we're done
			return commentByte
		endif
		// we have a quotation mark before //
	   
		do
			endQuote=startQuote
			do
				endQuote=strsearch(strLine, "\"", endQuote+1)
				if(endQuote==-1)
					return lineLength
				endif
				// endQuote is possible end of quote
				// remove escaped backslashes!
				strLine[startQuote,endQuote]=ReplaceString("\\\\", strLine[startQuote,endQuote], "  ")
			while(cmpstr(strLine[endQuote-1], "\\")==0) // ignore escaped quotes
		   
			// found end of quote
			if(endQuote>commentByte) // commentByte was within commented text
				startByte=endQuote+1
				break // look for another comment mark
			else
				startQuote=strsearch(strLine, "\"", endQuote+1)
				if(startQuote==-1 || startQuote>commentByte) // no quotes before //, we're done
					return commentByte
				endif
			endif
			// if we get to here we've found another start-of-quoted-text before //
		while(1) // next quoted text before //
	while(1) // next comment marker
End

#endif

// *** insert control functions ***

#ifdef ControlFunctions

Menu "Procedure"
	Submenu "Insert control function"
		"Popup;SetVariable;CheckBox;Button;Listbox;TabControl;Slider;CustomControl", /Q, InsertControlActionCode()
	End
End

Function /S InsertControlActionCode()
	GetLastUserMenuInfo
	String strSav = GetScrapText()
	String strFunc = ""
	sprintf strFunc, "function %sProc(STRUCT WM%sAction &s) : %sControl\r%s\t\r\treturn 0\rend\r", s_value, s_value, s_value, actionStructHelp(s_value)
	strFunc = ReplaceString("PopupControl", strFunc, "PopupMenuControl")
	strFunc = ReplaceString("TabControlControl", strFunc, "TabControl")
	strFunc = ReplaceString(" : CustomControlControl", strFunc, "")
	PutScrapText strFunc
	DoIgorMenu "Edit" "Paste"
	PutScrapText strSav
End

Static Function/S actionStructHelp(String which)
	String comment = "\t// WM"+which+"Action eventCode info:\r"
	if (!stringmatch(which,"CustomControl"))
		comment += "\t// -3\tControl received keyboard focus\r"
		comment += "\t// -2\tControl lost keyboard focus\r"
		comment += "\t// -1\tControl being killed\r"
	endif
	
	strswitch (which)
		case "Popup":
			comment += "\t//  2\tMouse up\r"
			comment += "\t//  3\tHovering\r"
		break
		case "SetVariable":
			comment += "\t//  1\tMouse up\r"
			comment += "\t//  2\tEnter key\r"
			comment += "\t//  3\tLive update\r"
			comment += "\t//  4\tMouse scroll wheel up\r"
			comment += "\t//  5\tMouse scroll wheel down\r"
			comment += "\t//  6\tValue changed by dependency update\r"
			comment += "\t//  7\tBegin edit\r"
			comment += "\t//  8\tEnd edit\r"
			comment += "\t//  9\tMouse down\r"
		break
		case "CheckBox":
			comment += "\t//  2\tMouse up, checkbox toggled\r"
		break
		case "Button":
			comment += "\t//  1\tMouse down\r"
			comment += "\t//  2\tMouse up\r"
			comment += "\t//  3\tMouse up outside control\r"
			comment += "\t//  4\tMouse moved\r"
			comment += "\t//  5\tMouse enter\r"
			comment += "\t//  6\tMouse leave\r"
			comment += "\t//  7\tMouse dragged while outside the control\r"
		break
		case "Listbox":
			comment += "\t//  1\tMouse down\r"
			comment += "\t//  2\tMouse up\r"
			comment += "\t//  3\tDouble click\r"
			comment += "\t//  4\tCell selection (mouse or arrow keys)\r"
			comment += "\t//  5\tCell selection plus Shift key\r"
			comment += "\t//  6\tBegin edit\r"
			comment += "\t//  7\tEnd edit\r"
			comment += "\t//  8\tVertical scroll - See Scroll Event Warnings under ListBox\r"
			comment += "\t//  9\tHorizontal scroll by user or by the hScroll=h keyword\r"
			comment += "\t//  10\tTop row set by row=r or first column set by col=c keywords\r"
			comment += "\t//  11\tColumn divider resized\r"
			comment += "\t//  12\tKeystroke, character code is place in row field- See Note on Keystroke Event under ListBox\r"
			comment += "\t//  13\tCheckbox was clicked. This event is sent after selWave is updated.\r"
		break
		case "TabControl":
			comment += "\t//  2\tMouse up\r"
		break
		case "Slider":
			comment += "\t//  1\tValue set\r"
			comment += "\t//  2\tMouse down\r"
			comment += "\t//  4\tMouse up\r"
			comment += "\t//  8\tMouse moved or arrow key moved the slider\r"
			comment += "\t//  16\tRepeat timer fired (see Repeating Sliders)\r"
		break
		case "CustomControl":
			comment += "\t// kCCE_mousedown = 1\tMouse down in control.\r"
			comment += "\t// kCCE_mouseup = 2\tMouse up in control.\r"
			comment += "\t// kCCE_mouseup_out = 3\tMouse up outside control.\r"
			comment += "\t// kCCE_mousemoved = 4\tMouse moved (only when mouse is over control).\r"
			comment += "\t// kCCE_enter = 5\tMouse entered control.\r"
			comment += "\t// kCCE_leave = 6\tMouse left control.\r"
			comment += "\t// kCCE_mouseDraggedOutside = 7\tMouse moved while it was outside the control (mouse pressed inside and dragged outside).\r"
			comment += "\t// kCCE_draw = 10\tTime to draw custom content.\r"
			comment += "\t// kCCE_mode = 11\tSent when executing CustomControl name, mode=m.\r"
			comment += "\t// kCCE_frame = 12\tSent before drawing a subframe of a custom picture.\r"
			comment += "\t// kCCE_dispose = 13\tSent as the control is killed.\r"
			comment += "\t// kCCE_modernize = 14\tSent when dependency (variable or wave set by value=varName) or draw event fires.\r"
			comment += "\t// kCCE_tab = 15\tSent when user tabs into the control. If you want keystrokes (kCCE_char), then set needAction.\r"
			comment += "\t// kCCE_char = 16\tSent on keyboard events (keyboard character => kbChar and modifiers bit field => kbMods). Sets needAction if key event was used and requires a redraw.\r"
			comment += "\t// kCCE_drawOSBM = 17\tCalled after drawing pict from picture parameter into an offscreen bitmap. You can draw custom content here.\r"
			comment += "\t// kCCE_idle = 18\tIdle event typically used to blink insertion points etc. Set needAction to force the control to redraw. Sent only when host window is on top.\r"
		break
	endswitch
	
	comment += "\t// eventMod bits: 0 = mouse down, 1 = shift, 2 = option | alt, 3 = command | ctrl, 4 = contextual click\r"
	return comment
End

#endif
