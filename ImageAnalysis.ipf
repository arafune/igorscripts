#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion  = 8.0
//Version $Id: ImageAnalysis.ipf 252 2005-03-10 04:42:42Z arafune $
Menu "Macros"
	"Sum of Images", SumOfImages()
	"Image To Waves", ImageToWaves ()
End

Function ImageToWaves()
	String distImagename = "image"
	Prompt distImagename, "Enter image wave name"
	String destwavename = "line"
	Prompt destwavename , "Enter wave name header"
	String destImagename = "imageR"
	Prompt destImagename , "Enter new (Extracted) image name"
	DoPrompt "Input Wavenames", distImagename, destwavename, destImagename
	if (V_Flag)
		Abort "Canceled"
	endif
	Variable startline= 110
	Variable endline = 210
	Prompt startline, "Enter start line for extraction"
	Prompt endline, "Enter end line for extraction"
	DoPrompt "Input start end line number", startline, endline
	if (V_Flag)
		Abort "Canceled"
	endif
	Variable i
	// GetImageSize
	Variable maxy
	Variable maxx
	maxx = DimSize($distimagename,0)
	maxy = DimSize($distimagename,1)
	if (maxy == 0)
		Abort "Did you really select image file ?"
	endif
	if (endline > maxy)
		String abort_msg1 = "You must select the value less" \
		+ "  than Y value of the original file"
		Abort abort_msg1
	endif
	if (startline > maxx)
		String abort_msg2 = "You must select the value less" \
		+ " than X value of the original file"
		Abort abort_msg2
	endif
	Wave theImage = $distImagename
	String theWavename
	for (i = startline-1; i < endline; i += 1)
		theWavename = destwavename + num2str(i+1)
		Make/O/N=(maxx), $theWavename
		Wave theWave = $thewavename
		theWave = theImage[p][i]; DelayUpdate
	endfor
	Wave theNewImage = $destImageName
	Duplicate /R=[][startline-1,endline-1]$distImagename, $destImageName
End


Function SumOfImages()
	DoWindow/F SumOfImagePanel
	if (V_Flag !=0)
		return 0
	endif
	String dfSave = GetDataFolder(1)
	String wList = ""
	wList = WaveList("*",";","")
	Variable numWaves
	numWaves = ItemsInList(wlist)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:ImageAnal
	Make /T/O/N=(numWaves) root:Packages:ImageAnal:tifwavelist1
	Wave /T tifwList1 = root:Packages:ImageAnal:tifwavelist1
	Make /O/N=(numWaves) root:Packages:ImageAnal:tifwavelist2
	Wave /T tifwList2 = root:Packages:ImageAnal:tifwavelist2
	//
	String /G gwList = wList
	Variable/G gnumWaves = numWaves
	Variable i
	for (i=0 ; i < numWaves ; i += 1)
		tifwList1[i] = StringFromList(i, wList)
	endfor
	SetDataFolder dfSave
	Execute "SumOfImagePanel()"
End

Static Function closeSumOfImagePanel (ctrlName) : ButtonControl
	String ctrlName
	DoWindow /K SumOfImagePanel
End

Window SumOfImagePanel() : Panel
	PauseUpdate; Silent 1
	NewPanel /K=1 /W = (60,60,760,660) as "Summention of images"
	DoWindow /F/C SumOfImagePanel

	//	SetWindow SumOfImagePanel, hook = SumOfImagePanelHook
	GetWindow SumOfImagePanel, wsize
	Variable res_ratio
	res_ratio = ScreenResolution / PanelResolution(WinName(0, 64))
	Variable PHeight
	Variable PWidth
	PHeight = V_Bottom - V_top
	PWidth = V_right - V_left

	ListBox tifwavesListbox, listWave = root:Packages:ImageAnal:tifwavelist1
	ListBox tifwavesListbox, selWave = root:Packages:ImageAnal:tifwavelist2
	ListBox tifwavesListbox, mode = 4
	ListBox tifwavesListbox, size={(PWidth*0.40)*res_ratio, (PHeight*0.80)*res_ratio}
	ListBox tifwavesListBox, pos={(PWidth*0.05)*res_ratio, (PHeight*0.05)*res_ratio}

	Button calcButton, pos = {(PWidth*0.5)*res_ratio, (PHeight*0.80)*res_ratio}
	Button calcButton, proc=calcSumOfImages, title = "Sum !!"
	Button calcButton, size={120*res_ratio,20*res_ratio}, labelBack=(1,0,1)

	Button closeButton, pos = {(PWidth*0.85)*res_ratio, (PHeight*0.90)*res_ratio}
	Button closeButton, proc = closeSumOfImagePanel, title = "Close"
	Button closeButton, size = {40*res_ratio, 20*res_ratio}
EndMacro

Function calcSumOfImages(ctrlName) : ButtonControl
	String ctrlName

	Wave /T SelectWaves = root:Packages:ImageAnal:tifwavelist1
	Wave checkSelectWaves = root:Packages:ImageAnal:tifwavelist2
	NVAR gnumWaves = root:Packages:ImageAnal:gnumWaves


	String wnLists = ""
	Variable i
	for (i=0; i< gnumWaves; i+=1)
		if (checkSelectWaves[i] != 0)
			wnLists = AddListItem(selectWaves[i],wnLists,";",ItemsInList(wnLists))
		endif
	endfor

	String tmpwave = ""
	tmpwave = StringFromList(0,wnLists)

	String destImagename = "image"
	Prompt destImagename, "Enter (new) wave name"

	DoPrompt "wavename", destImagename
	if (V_Flag)
		Abort "Canceled"
	endif

	Make $destImagename
	Wave destimage = $destimagename
	Duplicate/O $tmpwave, $destImagename
	Redimension/S $destImagename

	Variable numSelection
	numSelection = ItemsInList(wnLists)
	String theWavename
	for (i = 1; i < numSelection; i +=1 )
		theWavename = StringFromList(i, wnLists)
		wave theWave = $theWavename
		destImage += theWave; DelayUpdate
	endfor
	Display; AppendImage $destImagename
	DoWindow /K SumOfImagePanel
End
