﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 7.00
//  Author: Ryuichi Arafune

Menu "Load Waves"
	"Load SAC (Qmass) Data", OpenSACfile("", "")
End

Function OpenSACfile(pathName, fileName)
	String pathName	// Name of symbolic path or "" for dialog.
	String fileName	// File name, partial path, full path or "" for dialog.
	Variable refNum
	String str
	String message = "Select SAC file"

	// Open file for read.
	// R: ReadOnly, /Z=2 normal error reporting.
	// When used with /R, it opens the file if it exists or displays a dialog if it does not exist.
	Open/R/Z=2/P=$pathName /M=message /F="SAC files (*.SAC):.SAC;" refNum as fileName
	//Open/R/Z=2/P=$pathName /M=message refNum as fileName  // for IgrorVersion = 6.00

	// Store results from Open in a safe place.
	Variable err = V_flag
	String fullPath = S_fileName
	if (err == -1)
		Print "OpenSACfile cancelled by user."
		return -1
	endif
	if (err != 0)
		DoAlert 0, "Error in OpenSACfile"
		return err
	endif
	ReadSACdata(refNum)
	Close refNum
	return 0
End


Static Constant DATA_START_ADD = 392

/// @param refNum reference number specifies the file
Static Function ReadSACdata(refNum)
	Variable refNum
	FStatus refNum
	String fnName = ParseFilePath(3, S_fileName, ":", 0, 0) // filename w/o suffix
	String wn = "QMS_" + fnName
	Variable n_cyc, n_dbc, start_time, cycle_length, cyc_type, hpos, dpos
	Variable df, did, sid, firstmass, scan_width,  n_m, anz, range
	Variable mass_start, mass_end
	Variable i=0, j=0
	FSetPos refNum, 100
	FBinRead /F=3 refNum, n_cyc
	FSetPos refNum, 345
	FBinRead /U/F=2 refNum, scan_width
	FSetPos refNum, 341
	FBinRead /F=4 refNum, firstmass
	FSetPos refNum, 348
	FBinRead /F=4 refNum, mass_start
	FBinRead /F=4 refNum, mass_end
	FSetPos refNum, 194
	FBinRead /U/F=3 refNum, start_time
	FSetPos refNum, 347
	FBinRead /F=3 refNum, n_m  // Number of measurements for each mass
	//
	Variable total_n_points = round(scan_width * n_m)
	Variable buf
	if (n_cyc==1)
		read_data(refNum, wn, total_n_points, firstmass, scan_width, DATA_START_ADD)
	endif
	if (n_cyc>1)
		FSetPos refNum, DATA_START_ADD - 12
		for (i=0; i<n_cyc; i+=1)
			String wn_cycle = wn + "_" + num2str(i+1)
			FStatus refNum
			FSetPos refNum, V_filePos + 12
			FStatus refNum
			read_data(refNum, wn_cycle, total_n_points, mass_start, mass_start+scan_width, V_filePos)
		endfor
	endif
End

/// @param refNum Reference number for file
/// @param wave_name Wave name
/// @param N_Pts Number of points
/// @param first_mass The first number of the mass
/// @param scan_width Scan width
/// @param start_add Address for data starts
Static Function read_data(refNum, wave_name, N_Pts, mass_start, mass_end, start_add)
	Variable refNum, N_Pts, mass_start, mass_end, start_add
	String wave_name
	//
	Variable i=0, buf
	Make /O/N=(N_pts) $wave_name
	Wave workwave = $wave_name
	FSetPos refNum, start_add
	for (i=0; i<N_Pts; i+=1)
		FBinRead /F=4 refNum, buf
		workwave[i] = buf
	endfor
	SetScale /I x mass_start, mass_end, "", workwave
End
