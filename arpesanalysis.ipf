#pragma TextEncoding = "UTF-8"
// Use modern global access method and strict wave access.
#pragma rtGlobals=3
#pragma version = 0.99
#pragma ModuleName=ARPES_A
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later
#pragma IgorVersion = 8.00
#include <WaveSelectorWidget>
#include "RA_common"

/// Matrix2xy converts a 2-D matrix of Z values into 3 column 2D matrix
/// containing X, Y, AND Z values.
///
/// @param src             Source Wave
/// @param destwave_name   Wave name for store
Function Matrix2xyz(Wave src, String destwave_name)
	Variable n_angle, n_energy
	Variable min_angle, min_energy
	Variable delta_angle, delta_energy
	if (WaveDims(src) != 2)
		Abort NameOfWave(src) + " is not 2D wave"
	endif
	n_energy = DimSize(src, 1)
	min_energy = DimOffset(src, 1)
	delta_energy = DimDelta(src, 1)
	n_angle = DimSize(src, 0)
	min_angle = DimOffset(src, 0)
	delta_angle = DimDelta(src, 0)
	//
	Make /O/N=(n_energy * n_angle, 3) $destwave_name
	Wave destwave = $destwave_name
	MultiThread destwave[][0] = min_angle + delta_angle * mod(p, n_angle)
	MultiThread destwave[][1] = min_energy + delta_energy * floor(p/n_angle)
	MultiThread destwave[][2] = src[p]
End


/// Convert ARPES data (row is Angle, colmumn is Energy) to Band
///
/// Note for the Data orientaation.
///
/// If the Data orientation is transposed, use "MatrixTranspose".
///
/// energy_shift is the shift value as to set energy to Kinetic energy.
/// **Important**
/// Energy must be positive to produce the valid band structure.
/// Use "energy_shift" if your data is not fit.
/// If the "kinetic energy" is "Final state energy",
/// the energy_shift should be set at  -(analyzer work function)
///
/// @param src           Source wave
/// @param energy_shift  Energy shift value
Function arpes2band(Wave src, Variable energy_shift)
	Variable n_angle, n_energy
	Variable min_angle, min_energy
	Variable delta_angle, delta_energy
	Variable max_angle, max_energy
	//
	NVAR degrees = root:Packages:LaserUPS:degrees
	if (WaveDims(src) != 2)
		Abort NameOfWave(src) + " is not 2D wave"
	endif
	//
	n_energy = DimSize(src, 1)
	min_energy = DimOffset(src, 1)
	delta_energy = DimDelta(src, 1)
	n_angle = DimSize(src, 0)
	min_angle = DimOffset(src, 0)
	delta_angle = DimDelta(src, 0)
	//
	max_energy = min_energy + delta_energy * (n_energy -1)
	max_angle = min_angle + delta_angle * (n_angle-1)
	//
	Variable k_bottom_left, k_top_left, k_bottom_right, k_top_right
	Variable delta_k_bottom, delta_k_top, delta_k
	//
	NVAR K_inv_AA = root:Packages:LaserUPS:K_inv_AA
	//
	k_bottom_left = K_inv_AA * sqrt(min_energy+energy_shift) * sin(min_angle * degrees)
	k_top_left = K_inv_AA * sqrt(max_energy+energy_shift) * sin(min_angle * degrees)
	k_bottom_right = K_inv_AA * sqrt(min_energy+energy_shift) * sin(max_angle * degrees)
	k_top_right = K_inv_AA * sqrt(max_energy+energy_shift) * sin(max_angle * degrees)
	// print k_bottom_left, k_top_left, k_bottom_right, k_top_right  // for debug
	delta_k_bottom = abs(k_bottom_right - k_bottom_left) / (n_angle - 1)
	delta_k_top = abs(k_top_right - k_top_left) / (n_angle - 1)
	if (n_angle > 100)
		delta_k = max(delta_k_top, delta_k_bottom)
	else
		delta_k = min(delta_k_top, delta_k_bottom)
	endif
	Variable max_k, min_k
	max_k = max(k_bottom_left, k_top_left, k_bottom_right, k_top_right)
	min_k = min(k_bottom_left, k_top_left, k_bottom_right, k_top_right)
	Make /FREE/O/N=(n_angle+2) angle
	//
	Wave kx_data
	Make /FREE/O/N=((max_k+delta_k - min_k)/delta_k) kx_data
	SetScale /P x, min_k, delta_k, "Å\\S-1\\M", kx_data
	Wave intensity
	String band_data_name = NameOfWave(src)+"_band"
	Make /O/N=((max_k+delta_k-min_k)/delta_k, n_energy), $band_data_name
	Wave band_data = $band_data_name
	Variable i
	Variable ek
	for (i=0; i<n_energy; i+=1)
		ek = min_energy + delta_energy * i
		angle = min_angle - delta_angle + delta_angle * p
		angle = K_inv_AA * sqrt(ek+energy_shift) * sin(angle * degrees)
		MatrixOP /FREE/O intensity = col(src, i)
		InsertPoints /V=(NaN) 0, 1, intensity
		InsertPoints /V=(NaN) numpnts(intensity) + 1, 1, intensity
		kx_data = interp(x, angle, intensity)  // Note: This "angle" is the data of "kp".
		band_data[][i] = kx_data[p]
	endfor
	SetScale /P x, min_k, delta_k, "Å\\S-1\\M", $band_data_name
	SetScale /P y, min_energy, delta_energy, "eV", $band_data_name
	String srcnote
	srcnote = note(src)
	note /NOCR $band_data_name, srcnote
End



Function angle2k(Variable angle_deg, Variable ek)
	NVAR K_inv_AA = root:Packages:LaserUPS:K_inv_AA
	NVAR degrees = root:Packages:LaserUPS:degrees
	return K_inv_AA * sqrt(ek) * sin(angle_deg * degrees)
End


/// Return k value from the wave.
/// x value of the input wave is angle_degree.
/// offset_energy is energy shift to set energy (value of inputwave)
/// to kinetic energy.
///
/// @param src            Source wave
/// @param offset_energy  Energy offset
Function k_from_angle(Wave src, Variable offset_energy)
	String newname = NameOfWave(src) + "_k"
	Duplicate /O src, $newname
	Wave outwave = $newname
	//
	NVAR K_inv_AA = root:Packages:LaserUPS:K_inv_AA
	//
	MultiThread outwave = K_inv_AA * \
	sqrt(src + offset_energy) * sin(x*Pi/180)
	SetScale /P y, 0, 0, "Å\\S-1\\M", outwave
End


/// Dispersion of free electron
/// E(k) = hbar^2/2m k^2 (k is Å-1 unit).
/// m is effective mass of electron
/// ratio = m/m_e where m_e is the static electron mass.
///
/// @param x         x
/// @param ratio     effective mass in static mass unit
/// @param offset    offset energy
Function dispersion_free_electron(Variable x, Variable ratio, Variable offset)
	NVAR K_inv_AA = root:Packages:LaserUPS:K_inv_AA
	return ((1/K_inv_AA)^2/ratio) * x^2 + offset
End

// Dispersion of free electron (function of angle)
Function dispersion_free_electron_deg(x, ratio, offset)
	Variable x, ratio, offset
	NVAR degrees = root:Packages:LaserUPS:degrees
	return offset * ratio / (ratio - sin(x * degrees)^2)
End


/// Binning (Not smoothing) the 2D-wave
///
/// @param src      source wave
/// @param n_bin    number of row (or column) for binning
/// @param axis     0 or 1
Function Binning2D(Wave src, Variable n_bin, Variable axis)
	Variable n_x, min_x, delta_x
	Variable n_y, min_y, delta_y
	String unit_x, unit_y
	n_x = DimSize(src, 0)
	min_x = DimOffset(src, 0)
	delta_x = DimDelta(src, 0)
	n_y = DimSize(src, 1)
	min_y = DimOffset(src, 1)
	delta_y = DimDelta(src, 1)
	unit_x = WaveUnits(src, 0)
	unit_y = WaveUnits(src, 1)
	//
	String dest_name
	dest_name = NameOfWave(src) + "_binned"
	//
	Variable min_original, delta_original
	switch (axis)
		case 0:
			if (mod(n_x, n_bin) !=0)
				Abort "the number for binning width must be matched with number of pixels"
			endif
			min_original = min_x
			delta_original = delta_x
			ImageInterpolate /PXSZ={n_bin, 1} pixelate src
			break
		case 1:
			if (mod(n_y, n_bin) !=0)
				Abort "the number for binning width must be matched with number of pixels"
			endif
			min_original = min_y
			delta_original = delta_y
			ImageInterpolate /PXSZ={1, n_bin} pixelate src
			break
		default:
			Abort "axis should be 0 or 1"
	endswitch
	Variable new_init
	Variable new_delta
	if (mod(n_bin/2, 2)==0)  // even
		new_init = min_original + (n_bin/2 -1) * delta_original + delta_original /2
	else
		new_init = min_original + (n_bin/2 -1 ) * delta_original
	endif
	new_delta = delta_original * n_bin
	Wave pixlated = M_PixelatedImage
	MatrixOP /FREE tmp_wave = pixlated*n_bin
	Duplicate /O tmp_wave, $dest_name
	switch (axis)
		case 0:
			SetScale /P x, new_init, new_delta, unit_x, $dest_name
			SetScale /P y, min_y, delta_y, unit_y, $dest_name
			break
		case 1:
			SetScale /P x, min_x, delta_x, unit_x, $dest_name
			SetScale /P y, new_init, new_delta, unit_y, $dest_name
			break
	endswitch
	String srcnote
	srcnote = note(src)
	note /NOCR $dest_name, srcnote
End

/// Binning the angle axis
///
/// @param src     Source wave
/// @param n_bin   number of rows
Function binning_angle(Wave src, Variable n_bin)
	Binning2D(src, n_bin, 0)
End

/// @param src     source wave
Function CorrectionAngleBeta(Wave src)
	if (cmpstr(fetchStringfromWaveNote(src, "BetaCorrection"), "DONE")==0)
		Print NameOfWave(src) + " has been corrected by beta."
		return 1
	endif
	Variable beta = fetchValuefromWaveNote(src, "beta")
	if (numtype(beta)==2)
		Print "Beta is not identified in: " + NameOfWave(src)
		return 1
	endif
	String xunit = WaveUnits(src, 0)
	Variable left_x = DimOffset(src, 0) + beta
	Variable dx = DimDelta(src, 0)
	SetScale /P x, left_x, dx, xunit, src
	Print "Beta angle: ",  num2str(beta), " in", NameOfWave(src), " is corrected."
	note /NOCR src, "\r\nBetaCorrection:DONE\r\n"
End


// Split ARPES data into the 1D spectrum waves
// Copy wavescale
// Copy Note
// Destwave naeme is the original wave name + _0, _1, _2,,,
// @param src     Source Wave
Function ARPESSplit(Wave src)
	Variable XPixels = DimSize(src, 0)
	String note_str = note(src)
	String YUnit = WaveUnits(src, 1)
	Variable MinY = DimOffset(src, 1)
	Variable DeltaY = DimDelta(src, 1)
    //
	Variable i
	String destwave_name=NameOfWave(src)
	for (i=0; i<XPixels; i+=1)
		destwave_name = NameOfWave(src)+"_"+num2str(i)
		Make /O/N=(DimSize(src,1)) $destwave_name
		Wave destwave = $destwave_name
		destwave = src[i][p]
		note /NOCR destwave, note_str
		SetScale /P x, MinY, DeltaY, YUnit, destwave
	endfor
End
