#pragma rtGlobals=3		// Use modern global access method.
#include <WaveSelectorWidget>
#pragma ModuleName = eachWave2
Menu "Data"
	Submenu "Process at once"
		"Shift scaling X", eachWave(1)
		"Shift scaling Y", eachWave(2)
		"Duplicate to subfolder", eachWave(3)
		"Add string to wavename", eachWave(4)
		"ROI crop", eachWave(6)
	End
End

Menu "Analysis"
	Submenu "ARPES"
		"Beta Correction", eachWave(5)
	End
End

Function  eachWave(Variable func)
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	//
	String panelName = "WaveSelector"
	if (WinType(panelName)==7)
		DoWindow/F $panelName
	else
		Variable w_width = 430
		Variable w_height = 330
		NewPanel /N=$panelName /W=(627,125,627+w_width,125+w_height) as "Waves for..."
		DoWindow /F/C $panelName
		SetWindow $panelName hook=eachWave2#eachWavePanelhook
		SetWindow $panelName sizeLimit = {w_width, w_height, w_width, Inf}
		// List Box for wave selection (with "From Top Graph" checkbox)
		ListBox WaveSelectorList, pos={10,60},size={240,230}, fsize=18
		CheckBox FromTarget, pos={55,300}, size={114,18}, Proc=eachWave2#CheckProc
		CheckBox FromTarget, title="From Top Graph", fsize=14
		CheckBox FromTarget, value= 0
		// Close button
		Button Close, pos={330,w_height-50}, size={70,30}
		Button Close, Proc=DonePanelButton, title="Close", fSize=18
		Button Close, font="メイリオ"
		// Function Button
		switch (func)
			case 1:  // X Shift
				Variable /G root:Packages:LaserUPS:XShifts
				TitleBox title title="X Shift", pos={10,10}
				TitleBox title fSize=18, font="メイリオ"
				SetVariable Shift, pos={257,11},size={120,22}
				SetVariable Shift, bodyWidth=80,title="Shift: "
				SetVariable Shift, font="メイリオ", fsize=18
				SetVariable Shift, value=root:Packages:LaserUPS:XShifts
				Button Calc, pos={287,40}, size={90,20}
				Button Calc, title="Shift Scale", fSize=16
				Button Calc, Proc=eachWave2#XShifts
				break
			case 2: // Y Shift
				Variable /G root:Packages:LaserUPS:YShifts
				TitleBox title title="Y Shift", pos={10,10}
				TitleBox title fSize=18, font="メイリオ"
				SetVariable Shift, pos={257,11},size={120,22}
				SetVariable Shift, bodyWidth=80,title="Shift: "
				SetVariable Shift, font="メイリオ", fsize=18
				SetVariable Shift, value=root:Packages:LaserUPS:YShifts
				Button Calc, pos={287,40}, size={90,20}
				Button Calc, title="Shift Scale", font="メイリオ"
				Button Calc, Proc=eachWave2#YShifts
				break
			case 3: // Duplicate
				TitleBox title title="Duplicate waves"
				TitleBox title pos={10,10}, fSize=18, font="メイリオ"
				DrawText 276,33,"Folder name"
				String /G root:Packages:LaserUPS:subFolderName
				SetVariable subFolder, pos={266,32}
				SetVariable subFolder, size={110,22}
				SetVariable subFolder, bodyWidth=110,font="メイリオ"
				SetVariable subFolder, title=" "
				SetVariable subFolder, value=root:Packages:LaserUPS:subFolderName
				Button duplicateIt, pos={266,65}
				Button duplicateIt, size={110,20},title="Copy to this folder"
				Button duplicateIt, font="メイリオ"
				Button duplicateIt, Proc=eachWave2#DuplicateThem
				break
			case 4: // add_suffix
				TitleBox title title="Add string to wavename"
				TitleBox title pos={10,10}, fSize=18, font="メイリオ"
				DrawText 276,33, "string to add"
				String /G root:Packages:LaserUPS:addstr
				SetVariable addstr, pos={266, 32}, size={110, 22}
				SetVariable addstr, bodyWidth=110, font="メイリオ"
				SetVariable addstr, title=" "
				SetVariable addstr, value = root:Packages:LaserUPS:addstr
				Button duplicateIt, pos={266,65}, size={110,20}
				Button duplicateIt, title="Add str."
				Button duplicateIt, font="メイリオ", Proc=eachWave2#Add_suffix
				break
			case 5: //Beta Angle correction (Arpes data only)
				TitleBox title title="β Angle Correction"
				TitleBox title pos={10,10}, fSize=18, font="メイリオ"
				Button Calc, pos={287,80}, size={90,20}
				Button Calc, title="β Correction", font="メイリオ"
				Button Calc, Proc=eachWave2#Beta_correction_button
				break
			case 6:// ROIcrop
				Variable /G root:Packages:LaserUPS:MINP
				Variable /G root:Packages:LaserUPS:MAXP
				Variable /G root:Packages:LaserUPS:MINQ
				Variable /G root:Packages:LaserUPS:MAXQ
				//
				TitleBox title title = "ROI Cropping"
				TitleBox title pos = {10,10}, fSize=18, font="メイリオ"
				//
				SetVariable MINP, pos={290,18},size={120,22}
				SetVariable MINP, bodyWidth=80,title="MIN P:"
				SetVariable MINP, font="メイリオ", fsize=18
				SetVariable MINP, value=root:Packages:LaserUPS:MINP
				//
				SetVariable MAXP, pos={290,44},size={120,22}
				SetVariable MAXP, bodyWidth=80,title="MAX P:"
				SetVariable MAXP, font="メイリオ", fsize=18
				SetVariable MAXP, value=root:Packages:LaserUPS:MAXP
				//
				SetVariable MINQ, pos={290,90},size={120,22}
				SetVariable MINQ, bodyWidth=80,title="MIN Q:"
				SetVariable MINQ, font="メイリオ", fsize=18
				SetVariable MINQ, value=root:Packages:LaserUPS:MINQ
				//
				SetVariable MAXQ, pos={290,116},size={120,22}
				SetVariable MAXQ, bodyWidth=80,title="MAX Q:"
				SetVariable MAXQ, font="メイリオ", fsize=18
				SetVariable MAXQ, value=root:Packages:LaserUPS:MAXQ
				//
				Button CROPROI, pos={290,265}, size={110,20}
				Button CROPROI, title="CROP ROI."
				Button CROPROI, font="メイリオ", Proc=eachWave2#crop_roi
				break
			case 7:
				TitleBox title title="Tune Angle to Make single 2D wavewave"
				TitleBox title pos={10,10}, fSize=18, font="メイリオ"
				Button Calc, pos={287,80}, size={120,20}
				Button Calc, title="Tune Angle", font="メイリオ"
				Button Calc, Proc=eachWave2#tune_emission_angle
				break
			default:
				break
		endswitch
		//
		MakeListIntoWaveSelector(panelName, \
			"WaveSelectorList", \
			content=WMWS_Waves)
	endif
End

Static Function eachWavePanelhook(String infostr)
	String event = StringByKey("EVENT", infostr)
	String win = StringByKey("WINDOW", infostr)
	GetWindow $win, wsize
	//
	Variable w_width = 430
	Variable statusCode = 0
	strswitch(event)
		case "resize":
			GetWindow $win, wsize
			Variable w_height = V_Bottom - V_top
			ListBox WaveSelectorList, size={240,w_height-100}
			CheckBox FromTarget, pos={55, w_height-30}
			Button Close, pos={330,w_height-50}
			break;
	endswitch
End

Static Function CheckProc(cba) : CheckboxControl
	STRUCT WMCheckboxAction &cba
	switch(cba.eventCode)
		case 2: // mouse up
			Variable checked = cba.checked
			String windowName = cba.win
			if (checked == 0)
				MakeListIntoWaveSelector(windowName, \
				"WaveSelectorList",\
				selectionMode=WMWS_SelectionNonContiguous, \
				content=WMWS_Waves)
			else
				MakeListIntoWaveSelector(windowName, \
				"WaveSelectorList", \
				selectionMode=WMWS_SelectionNonContiguous, \
				content=WMWS_Waves, \
				nameFilterProc="TopGraphFilter")
			endif
			break
	endswitch
	return 0
End

Function CheckProc_SingleCheck(cba) : CheckboxControl
	STRUCT WMCheckboxAction &cba
	switch(cba.eventCode)
		case 2: // mouse up
			Variable checked = cba.checked
			String windowName = cba.win
			if (checked == 0)
				MakeListIntoWaveSelector(windowName, \
				"WaveSelectorList", \
				selectionMode=WMWS_SelectionSingle, \
				content=WMWS_Waves)
			else
				MakeListIntoWaveSelector(windowName, \
				"WaveSelectorList", \
				selectionMode=WMWS_SelectionSingle, \
				content=WMWS_Waves, \
				nameFilterProc="TopGraphFilter")
			endif
			break
	endswitch
	return 0
End

Function TopGraphFilter(aName, contents)
	String aName
	Variable contents
	Variable flag
	String WavesList = WaveListOnWin()
	flag = WhichListItem(aName, WavesList)
	if (flag >0)
		return 1
	else
		return 0
	endif
End

Static Function /S WaveListOnWin()
	Variable i = 0
	String FullpathWave, FullPathList="", F
	Wave theWave = WaveRefIndexed("", i, 1)
	// For 1D data
	if (WaveExists(theWave))
		do
			FullpathWave = GetWavesDataFolder(theWave, 2)
			FullPathList=FullPathList+";"+FullPathWave
			i += 1
			Wave theWave = WaveRefIndexed("", i ,1)
		while (WaveExists(theWave))
	endif
	// For 2D image
	String waves_in_image = ImageNameList("", ";")
	if (ItemsInList(waves_in_image) > 0)
		i = 0
		String image_name
		for (i=0; i<ItemsInList(waves_in_image); i +=1)
			image_name = StringFromList(i, waves_in_image)
			Wave theWave = ImageNameToWaveRef("", image_name)
			FullpathWave = GetWavesDataFolder(theWave, 2)
			FullPathList=FullPathList+";"+FullPathWave
		endfor
	endif
	return FullPathList
End

Function shift_by(src, axis_index, value)
	Wave src
	Variable axis_index // 0 is X, 1 is Y
	Variable value
	//
	Variable offset = DimOffset(src, axis_index)
	Variable delta = DimDelta(src, axis_index)
	String unit = WaveUnits(src, axis_index)
	if (axis_index==0)
		SetScale /P x, offset+value, delta, unit, src
		Print "X scale of the Wave: ", NameOfWave(src), "is shifted by ", value
	elseif (axis_index==1)
		SetScale /P y, offset+value, delta, unit, src
		Print "Y scale of the Wave: ", NameOfWave(src), "is shifted by ", value
	else
		Abort "axis_index must be 0 or 1"
	endif
End

/// Subroutines for FUNCTION
// 1: X Shift
Static Function XShifts(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("WaveSelector", "WaveSelectorList")
	String theWave
	Variable n = ItemsInList(objs)
	Variable i
	NVAR XShift = root:Packages:LaserUPS:XShifts
	Variable left_x, dx
	String xunit

	switch(ba.eventCode)
		case 2: // mouse up
			// click code here
			for  (i=0; i< n; i+=1)
				theWave = StringFromList(i , objs)
				xunit = WaveUnits($theWave, 0)
				left_x = DimOffset($theWave, 0) + XShift
				dx = DimDelta($theWave, 0)
				SetScale /P x, left_x, dx, xunit, $theWave
				Print "X scale of the Wave: ", theWave, "is shifted by ", XShift
			endfor
			break
	endswitch
	return 0
End

Static Function YShifts(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("WaveSelector", "WaveSelectorList")
	String theWave
	Variable n = ItemsInList(objs)
	Variable i
	NVAR YShift = root:Packages:LaserUPS:YShifts
	Variable offset_y, dy
	String yunit
	switch(ba.eventCode)
		case 2: // mouse up
			// click code here
			for  (i=0; i< n; i+=1)
				theWave = StringFromList(i , objs)
				yunit = WaveUnits($theWave, 1)
				Offset_y = DimOffset($theWave, 1) + YShift
				dy = DimDelta($theWave, 1)
				SetScale /P y, offset_y, dy, yunit, $theWave
				Print "Y scale of the Wave: ", theWave, "is shifted by ", YShift
			endfor
			break
	endswitch
	return 0
End

// 3: Duplicate
Static Function DuplicateThem(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("WaveSelector", "WaveSelectorList")
	String theWave
	Variable n = ItemsInList(objs)
	Variable i
	SVAR  subFolderName = root:Packages:LaserUPS:subFolderName
	String subFolderNameLiberal = PossiblyQuoteName(subFolderName)
	String FullpathToFolder
	String TotheWave
	switch(ba.eventCode)
		case 2: // mouse up
			if (stringmatch(subFolderName, "*:*"))
				Abort "Pathname must not contain \":\"\nAborted..."
			endif
			if (strlen(subFolderName)==0)
				Abort "Input subfolder name."
			endif
			for (i=0; i<n; i+=1)
				theWave = StringFromList(i, objs)
				FullPathtoFolder = ParseFilePath(1, theWave, ":", 1, 0) + \
				SubFolderNameLiberal
				NewDataFolder /O $FullPathtoFolder
				toTheWave =FullPathtoFolder+ ":" + \
				ParseFilePath(0, theWave, ":", 1, 0)
				Duplicate /O $theWave $TotheWave
			endfor
			// click code here
			break
	endswitch
	return 0
End

// 4: AddString
Static Function Add_suffix(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("WaveSelector", "WaveSelectorList")
	String theWave
	Variable n = ItemsInList(objs)
	Variable i
	SVAR addstr = root:Packages:LaserUPS:addstr
	String addstrLiberal = PossiblyQuoteName(addstr)
	String FullpathToFolder
	String TotheWave
	switch(ba.eventCode)
		case 2: // mouse up
			if (stringmatch(addstr, "*:*"))
				Abort "\":\" is not allowed for wavename"
			endif
			if (strlen(addstr) == 0)
				Abort "Input string for added"
			endif
			for (i=0; i<n; i+=1)
				theWave = StringFromList(i, objs)
				TotheWave = ParseFilePath(0, theWave, ":", 1, 0) + \
				addstr // Extract last element.
				Rename $theWave, $TotheWave
			endfor
	endswitch
	return 0
End


//5: Beta angle correction (ARPES data only)
Static Function Beta_correction_button(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("WaveSelector", "WaveSelectorList")
	String theWave
	Variable n = ItemsInList(objs)
	Variable i
	switch(ba.eventCode)
		case 2: // mouse up
			// click code here
			for  (i=0; i< n; i+=1)
				theWave = StringFromList(i , objs)
				CorrectionAngleBeta($theWave)
			endfor
			break
	endswitch
	return 0
End


//6: CLIP ROI
Static Function crop_roi(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("WaveSelector", "WaveSelectorList")
	String theWave, roiWave
	NVAR MINP = root:Packages:LaserUPS:MINP
	NVAR MAXP = root:Packages:LaserUPS:MAXP
	NVAR MINQ = root:Packages:LaserUPS:MINQ
	NVAR MAXQ = root:Packages:LaserUPS:MAXQ
	Variable n = ItemsInList(objs)
	Variable i
	switch(ba.eventCode)
		case 2:
			for (i=0; i<n; i+=1)
				theWave = StringFromList(i, objs)
				roiWave = NameOfWave($theWave) + "_ROI"
				Duplicate /O/R=[MINP, MAXP][MINQ, MAXQ] $theWave $roiWave
				// TODO: DUPLICATE IN SAME FOLDER
			endfor
			break
	endswitch
	return 0
End


Static Function tune_emission_angle(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String objs = WS_SelectedObjectsList("WaveSelector", "WaveSelectorList")
	String theWave, refwave
	Variable n = ItemsInList(objs)
	Variable i
	Variable xoffset = Inf
	switch(ba.eventCode)
		case 2: // mouse up
			// click code here
			for  (i=0; i< n; i+=1)
				theWave = StringFromList(i, objs)
				if (DimOffset($theWave, 0) < xoffset)
					xoffset = DimOffset($theWave, 0)
					refWave = theWave
				endif
			endfor
			Variable offset_correction
			for (i=0; i<n ; i+=1)
				theWave = StringFromList(i, objs)
				offset_correction = match_emission_angle($theWave, $refWave)
				shift_by($theWave, 0, -offset_correction)
			endfor
			break
	endswitch
	return 0
End


Function match_emission_angle(Wave src, Wave refwave)
	Variable delta_angle = DimDelta(src, 0)
	if (DimDelta(refwave, 0) != delta_angle)
		Abort
	endif
	Variable start_x = DimOffset(src, 0)
	Variable ref_x = DimOffset(refwave, 0)
	if (start_x < ref_x)
		Abort
	endif
	Variable i = 0
	Variable diff_angle = Inf
	do
		diff_angle =  start_x  - (ref_x + i * delta_angle)
		i = i + 1
	while (diff_angle > delta_angle)
//	print i
//	print ref_x + i * delta_angle
//	print start_x
//	print diff_angle
	return diff_angle
End
