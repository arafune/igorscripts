#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVertsion = 5.0
#pragma version=1.00
//Version $Id: waveSelection.ipf 676 2007-03-07 09:55:28Z arafune $

Function/S waves2list(isTarget)
	Variable isTarget
	String wList = ""
	wList=WaveList("*",";","")
	switch (isTarget)
		case 1:
			wList = WaveList("*", ";", "WIN:")
			break
		case 2:
			wList = WaveList("ScSm_*", ";", "")
			break
		default:  // 0
			wList= WaveList("*", ";", "")
			break
	endswitch
	return wList
End

Function FromTargetCheck(ctrlName, checked) : CheckboxControl
	String ctrlName
	Variable checked
	String wList
	Variable numWaves
	if (checked)
		wList = waves2list(1) // isTarget :true
	else
		wList = waves2list(0) // isTarget : false
	endif
	numWaves = ItemsInList(wList)
	Make /T/O/N=(numWaves) root:Packages:LaserUPS:waveList1
	Wave /T wList1 = root:Packages:LaserUPS:waveList1
	Variable i
	for (i = 0; i <numWaves ; i+= 1)
		wList1[i] = StringFromList(i,wList)
	endfor
End

Function FromTargetCheckMulti(ctrlName, checked): CheckboxControl
	String ctrlName
	Variable checked
	String wList
	Variable numWaves
	if (checked)
		wList = waves2List(1)
	else
		wList = waves2list(0)
	endif
	numWaves = ItemsInList(wList)
	Make/T/O/N=(numWaves) root:Packages:LaserUPS:waveList1
	Wave /T wList1 = root:Packages:LaserUPS:waveList1
	Make/O/N=(numwaves)  root:Packages:LaserUPS:waveList2
	Wave wList2 = root:Packages:LaserUPS:waveList2
	Variable i
	for (i = 0; i <numWaves ; i+= 1)
		wList1[i] = StringFromList(i,wList)
	endfor
	wList2 = 0
End
