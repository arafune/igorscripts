﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later
#pragma ModuleName=PARABOLAFIT

Menu "Analysis"
	Submenu "ARPES"
		"Free electron-like dispersion fit", FreeElectronDispersionFit()
	End
End

Function FreeElectronDispersionFit()
	String dfSave = GetDataFolder(1)
	NewDataFolder /O/S root:Packages
	NewDataFolder /O/S root:Packages:LaserUPS
	SetDataFolder dfSave
	//
	Variable /G root:Packages:LaserUPS:PARABOLA_E0
	Variable /G root:Packages:LaserUPS:PARABOLA_MASS
	Variable /G root:Packages:LaserUPS:PARABOLA_XOFFSET
	//
	String targetwindow = WinName(0,1)
	String /G root:Packages:LaserUPS:ParabolicFitWin = targetwindow
	GetWindow $targetwindow, wsize
	Variable w_width = 460
	Variable w_height = 220
	if (WinType("ParabolaFit")==7)
		DoWindow /F ParabolaFit
	else
		DrawPanel(w_width, w_height, targetwindow)
	endif
End


/// @param w_width
/// @param w_height
/// @param targetwindow
Static Function DrawPanel(Variable w_width, Variable w_height, String targetwindow)
	Variable /G root:Packages:LaserUPS:PARABOLA_E0
	Variable /G root:Packages:LaserUPS:PARABOLA_MASS
	Variable /G root:Packages:LaserUPS:PARABOLA_XOFFSET
	NVAR E0 = root:Packages:LaserUPS:PARABOLA_E0
	NVAR mass = root:Packages:LaserUPS:PARABOLA_MASS
	NVAR xoffset = root:Packages:LaserUPS:PARABOLA_XOFFSET
	//
	GetWindow $targetwindow, wsize
	NewPanel /N=ParabolaFit /W=(V_right, V_top, V_right+w_width, V_top+w_height)
	DoWindow /F/C ParabolaFit
	SetWindow ParabolaFit sizeLimit = {w_width, w_height, w_width, w_height}
	//
	SetDrawLayer UserBack
	SetDrawEnv fname= "Helvetica",fsize= 24
	DrawText 87,39,"Free electron dispersion fit"
	//
	SetVariable E_0, pos={15.00, 56.00}, size={124.0, 26.0}, title="E\\B0", fSize=16
	SetVariable E_0, limits={-Inf, Inf, 0.01}
	SetVariable E_0, value=E0
    SetVariable E_0, help={"The energy bottom of the parabolic band"}
	SetVariable mass_ratio, pos={148.00, 56.00}, size={168.00, 23.00}
	SetVariable mass_ratio, title="m*/m\\Bo", fSize=16
	SetVariable mass_ratio, limits={-Inf, Inf, 0.01}
	SetVariable mass_ratio, value=mass
	String hlpmsg
	hlpmsg = "Effective mass of electron (static electron unit)\n"
	hlpmsg += "If the angle is 'Axis', you may meet the strange value of the effective mass.\n"
	hlpmsg += "Recall you need to correct by the analyzer workfunction."
    SetVariable mass_ratio, help={hlpmsg}
	SetVariable X_offset, pos={148.0, 105.00}, size={168.0, 23.0}, title="X offset"
	SetVariable X_offset, fSize=16, limits={-Inf, Inf, 0.01}
	SetVariable X_offset, value=xoffset
	SetVariable X_offset, help={"Offset value of along the x-axis"}
	//
	PopupMenu popup_xaxis, pos={15.0, 108.0}, size={115.0, 20.00}, title="X-axis"
	PopupMenu popup_xaxis, font="Helvetica", fSize=16
	PopupMenu popup_xaxis, mode=1, popvalue="Angle", value=#"\"Angle;Momentum\""
    PopupMenu popup_xaxis, help={"Angle or Momentum. Note that \"Angle\" mode may not provide a correct effective mass"}
	//
	Button putcurvebutton, pos={26.0, 156.00}, size={80.0, 40.00}, title="Put curve"
    Button putcurvebutton, help={"Append the parabolic curve"}
	Button putcurvebutton, Proc=PARABOLAFIT#make_put_curve
	Button StoreButton, pos={131.0, 155.0}, size={80.0, 40.0}, title="Store"
	Button StoreButton, Proc=PARABOLAFIT#store_wave
    Button StoreButton, help={"Store the wave (the parameters are written in the note)"}
	Button Close, pos={352.0,154.0}, size={80.0, 40.0},title="Close"
	Button Close, Proc=PARABOLAFIT#closebutton
    Button Close, help={"Close this panel"}
End


Static Function make_put_curve(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String win_name = ba.win
 	SVAR targetwindow = root:Packages:LaserUPS:ParabolicFitWin
	//
	String formula1 = "((1 / root:Packages:LaserUPS:K_inv_AA)^2"
	formula1 += " / root:Packages:LaserUPS:PARABOLA_MASS)"
	formula1 += " * (x-root:Packages:LaserUPS:PARABOLA_XOFFSET)^2"
	formula1 += "+root:Packages:LaserUPS:PARABOLA_E0"
	String formula2 = "root:Packages:LaserUPS:PARABOLA_E0"
	formula2 += "/(1 - ((1 / root:Packages:LaserUPS:PARABOLA_MASS)"
	formula2 += "* (sin((x-root:Packages:LaserUPS:PARABOLA_XOFFSET)"
	formula2 += " *root:Packages:LaserUPS:degrees))^2))"
	//
	switch (ba.eventcode)
		case 2:
			SetWindow $targetwindow, hook=$""
			GetAxis /Q/W=$targetwindow bottom
			Make /N=400/O root:Packages:LaserUPS:parabolic
			Wave parabolic=root:Packages:LaserUPS:parabolic
			ControlInfo /W=ParabolaFit popup_xaxis
			strswitch (S_value)
				case "Angle":
					SetScale/I x, V_min, V_max, "Degrees", parabolic
					SetFormula root:Packages:LaserUPS:parabolic, formula2
				break
				case "Momentum":
					SetScale/I x, V_min, V_max, " Å\\S-1\\M", parabolic
					SetFormula root:Packages:LaserUPS:parabolic, formula1
				break
			endswitch
			SetScale d 0,0,"eV", parabolic
			String tracelist = TraceNameList(targetwindow, ";", 1)
			if (strlen (tracelist) == 0 || \
					(strlen(tracelist)>0 && WhichListItem("parabolic", tracelist)==-1))
				AppendToGraph /W=$targetwindow root:Packages:LaserUPS:parabolic
			endif
	endswitch
End



Static Function store_wave(ba): ButtonControl
	STRUCT WMButtonAction &ba
    //
	NVAR E0 = root:Packages:LaserUPS:PARABOLA_E0
	NVAR mass = root:Packages:LaserUPS:PARABOLA_MASS
	NVAR xoffset = root:Packages:LaserUPS:PARABOLA_XOFFSET
	String win_name = ba.win
	SVAR targetwindow =  root:Packages:LaserUPS:ParabolicFitWin
	String note_str
	note_str = "E0:" + num2str(E0)
	note_str += ";mass:" + num2str(mass)
	note_str += ";xoffset:" + num2str(xoffset) + "\n"
	String tracelist = ImageNameList(targetwindow, ";")
	String image_name = StringFromList(0, tracelist)
	String store_wave_name = image_name + "_parabolicfit"
	switch (ba.eventcode)
		case 2:
		    Duplicate /O root:Packages:LaserUPS:parabolic $store_wave_name
            note /K/NOCR $store_wave_name, note_str
            SetFormula $store_wave_name, ""
		break
	endswitch
End


Static Function closebutton(ba): ButtonControl
	STRUCT WMButtonAction &ba
	String win_name = ba.win
	SVAR targetwindow =  root:Packages:LaserUPS:ParabolicFitWin
	switch (ba.eventcode)
		case 2:
			DoWindow /K $win_name
		break
	endswitch
End
