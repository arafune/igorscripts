#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later

#include "SelectFiles"
#include "WaveSelection"

Menu "Load Waves"
	"Load .SP2 file", loadSP2("", "")
	"Load .SP2 files in a folder", Load_SP2_files()
End


Function Load_SP2_files()
	String dir_files, dirName, fileName
	Variable i, numFiles
	dir_files = SelectFilesIndir(".sp2", "????")
	numFiles = ItemsInList(dir_files)
	dirName = StringFromList(0, dir_files)
	for (i=1; i<numFiles; i+=1)
		fileName = dirName + StringFromList(i, dir_files)
		LoadSP2("", fileName)
	endfor
End

/// @param path_to_file
/// @param fileName
Function LoadSP2(String path_to_file, String fileName)
	Variable refNum
	Variable err
	Open /R/Z=2/P=$path_to_file refNum as fileName
	FStatus refNum
	//
	String buffer
	Make /O/T/N=(200, 2)/FREE  params
	Wave parms_w=params
	Variable i = 0
	String k, v
	Variable energy_ch
	Variable angle_ch
	String tmp_a, tmp_b, tmp_c
	Variable j, intensity
	j=0
	FReadLine refNum, buffer // Omit the first line.
	Variable section // 0: header 1: Value
	section=0
	do
		FReadLine refNum, buffer
		if (strlen(buffer) == 0) // End of fie
			break
		endif
		//
		switch (section)
			case 0:
				if (cmpstr(buffer[0], "#") == 0)
					SplitString /E="^(.*?)\s*=\s*(.*?)$" TrimString(buffer[1, strlen(buffer)]), k, v
					if (strlen(v)!=0)
						params[i][0] = k
						params[i][1] = v
						i = i+1
					endif
				else
					SplitString/E="^(\d+)+\s+(\d+)\s+(\d+)" TrimString(buffer), tmp_a, tmp_b, tmp_c
					energy_ch = str2num(tmp_a)
					angle_ch = str2num(tmp_b)
					String wave_name = ParseFilePath(3, S_fileName, ":", 0, 0)
					Make /O /N=(energy_ch*angle_ch) $wave_name
					Wave spectra=$wave_name
					section = 1
				endif
				break
			case 1:
				sscanf buffer, "%g", intensity
				spectra[j] = intensity
				j = j+1
				break
		endswitch
	while(1)
	String a_unit
	FindValue /TEXT="aUnit" params
	a_unit = params[V_Value][1]
	Redimension /N=(energy_ch, angle_ch) spectra
	String a_Range, E_Range
	FindValue /TEXT="aRange" params
	a_Range = params[V_Value][1]
	FindValue /TEXT="ERange" params
	e_Range = params[V_Value][1]
	Variable angle_min, angle_max
	angle_min = str2num(StringFromList(0, a_Range," "))
	angle_max = str2num(StringFromList(1, a_Range," "))
	Variable Ek_min, Ek_max
	Ek_min = str2num(StringFromList(0, E_Range, " "))
	Ek_max = str2num(StringFromList(1, E_Range, " "))
	//
	Variable angle_delta, Ek_delta
	angle_delta = (angle_max-angle_min)/angle_ch
	Ek_delta = (Ek_max - Ek_min)/energy_ch
	SetScale /P x, Ek_min + 0.5*Ek_delta, Ek_delta, "eV", spectra
	SetScale /P y, angle_min + 0.5*angle_delta, angle_delta, a_unit, spectra
	// note
	note /NOCR spectra "LoadFrom:"+ "/" + ReplaceString(":", S_path, "/") + S_fileName + "\n"
	String	note_variables
	note_variables = "Ek;Ep;integration_time;total_integration_time"
	for (i=0; i<ItemsInList(note_variables); i++)
		k = StringFromList(i, note_variables, ";")
		FindValue /Text=k params
		v = StringFromList(0, params[V_Value][1], " ")
		note /NOCR spectra, k + ":" + v + "\n"
	endfor
	MatrixTranspose spectra
End
