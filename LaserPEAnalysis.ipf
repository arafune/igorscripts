#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion =  6.3
#pragma Version = 2.00

Function conv2(p, src, dest)
	//  normally,
	//  src : gaussian function.
	//  dest : intrinsic spectrum
	Variable p
	Wave src, dest
	Variable deltax_src, deltax_dest
	// check deltax.  (Are they same length?)
	deltax_src=deltax(src)
	deltax_dest=deltax(dest)
	if (abs(deltax_src - deltax_dest)>1e-15)
		String AbortMessage
		AbortMessage = "deltax_src="+ num2str(deltax_src)
		AbortMessage += " and deltax_dest ="+num2str(deltax_dest)+num2char(13)
		AbortMessage = num2str(deltax_src - deltax_dest) + num2char(13)
		AbortMessage += "Both of deltax should be same!!"
		Abort AbortMessage
	endif
	Variable x0loc
	x0loc = x2pnt(src, 0.0)
	// The x2pnt function returns the integer point number on the wave whose X value is closest to x1.
	Variable tmp, m, N, shiftedsrc
	N = max(numpnts(src), numpnts(dest))
	tmp = 0
	for(m=0; m<N; m += 1)
		if (p+x0loc-m < 0 || p+x0loc-m >= numpnts(src))
			shiftedsrc= 0
		else
			shiftedsrc = src[p+x0loc-m]
		endif
		if (m < numpnts(dest))
			tmp = tmp + shiftedsrc*dest[m]
		endif
	endfor
	Return tmp*deltax_src
End


Function conv3(p, src, dest)
	//  src : lorentzian (or gaussian) function
	//  dest : intrinsic spectrum
	//
	//  different from conv2, this function does not check the delta x of the waves.
	//
	Variable p
	Wave src, dest
	Variable x0src
	Variable x0dest
	Variable tmp, m, N, shiftdest
	Variable deltaxdest
	x0src = x2pnt(src,0)
	deltaxdest = deltax(dest)
	N = max(numpnts(src), numpnts(dest)) -1
	tmp = 0
	for (m=0 ; m<=N; m+=1)
		if(p+x0src-m<0 || p+x0src-m >= numpnts(dest))
			shiftdest= dest[0]
		else
			shiftdest = dest[p+x0src-m]
		endif
		if (m < numpnts(src))
			tmp +=  shiftdest * src[m]
		endif
	endfor
	return tmp * deltaxdest
End


Function instrument(x, resolution)
	Variable x, resolution
	Variable resolution_meV, sigma
	resolution_meV = resolution* 10^(-3)
	sigma = resolution_meV/2/sqrt(2*Ln(2))
	return 1/(sqrt(2 * pi)*sigma) * exp(- x^2/2/sigma^2)
End


Constant kb_eV = 8.617333262145178E-5 // eV Units
Function FDVac(x, Evac, Ef, T)
	Variable x, Evac, Ef, T
	If (x < Evac)
		return 0
	else
		return 1/(exp((x-Ef)/kb_eV/T)+1)
	endif
End


Function Lorentzian(x, center, A, fwhm)
	// (normalized) Lorentzian function
	Variable center
	Variable A
	Variable fwhm
	Variable x
	If(A==0 || fwhm == 0 )
		return 0
	else
		return (A/Pi)*fwhm/2/((x-center)^2+(fwhm/2)^2)
	endif
End


Function gaussian(x, center, A, fwhm)
	Variable center
	Variable A
	Variable fwhm
	Variable x
	Variable sigma = fwhm/2/sqrt(2*Ln(2))
	If(A==0 || fwhm == 0)
		return 0
	else
		return A*Gauss(x, center, sigma)
	endif
End


Constant sqrtln2=0.832554611157698 // sqrt(ln(2))
Constant sqrtln2pi=0.469718639349826 // sqrt(ln(2)/pi)
Function VoigtFitting(w, xx): FitFunc
	Wave w; Variable xx
	// w[0] : vertical offset
	// w[1] : Area
	// w[2] : Center of the peak
	// w[3] : Gaussian width (FWHM)
	// w[4] : Lorentizl width (FWHM)
	Variable voigtX
	Variable voigtY 
	if (w[3] == 0)
		voigtX = 2*sqrtln2*(xx-w[2])/ 1
		voigtY = 10000  // Very large value
	else
		voigtX = 2*sqrtln2*(xx-w[2])/ w[3]
		voigtY = sqrtln2*w[4]/w[3]
	endif
	return w[0] + (w[1]/w[3]) * 2 * sqrtln2pi * VoigtFunc(voigtX, voigtY)
	// Note: the width of the Voigt peak is:
	// w[4]/2 + sqrt(w[4]^2/4 + w[3]^2)
End


Function position2dealytime(Variable position_mm, Variable center_position_mm)
	return 2 * (position_mm - center_position_mm) * 1000 * 3.335640951981521 * 1E-15 
End
